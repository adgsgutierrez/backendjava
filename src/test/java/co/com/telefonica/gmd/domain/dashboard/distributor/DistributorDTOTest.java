package co.com.telefonica.gmd.domain.dashboard.distributor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DistributorDTOTest {

    private DistributorDTO distributorDTO;

    @Before
    public  void setup(){
        distributorDTO = new DistributorDTO();
    }

    @Test
    public void testDistributorDTO(){
        Integer value = 1;
        distributorDTO.toString();
        distributorDTO.getIdDistribuidor();
        distributorDTO.getDistribuidor();
        distributorDTO.setIdDistribuidor(1);
        distributorDTO.setDistribuidor("String");

        Assert.assertEquals(value,distributorDTO.getIdDistribuidor());
        Assert.assertEquals("String",distributorDTO.getDistribuidor());
    }
}
