package co.com.telefonica.gmd.domain.user;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class UserTest {

    private LdapObjectUserDetail ldapObjectUserDetail;
    private UserDTO userDTO;
    private UsuariosEntity userEntity;
    private UserLdapAuthorization userLdapAuthorization;
    private UserLdapLogin userLdapLogin;
    private UserCreateDTO userCreateDTO;
    private UserResponsePermision userResponsePermision;

    @Before
    public void setup() {
        ldapObjectUserDetail = new LdapObjectUserDetail();
        userDTO = new UserDTO();
        userEntity = new UsuariosEntity();
        userLdapAuthorization = new UserLdapAuthorization();
        userLdapLogin = new UserLdapLogin();
        userCreateDTO = new UserCreateDTO();
        userResponsePermision = new UserResponsePermision();
    }

    @Test
    public void validationLdapObjectUserDetail() {
        String company = "company";
        String department = "department";
        String name = "name";
        ldapObjectUserDetail.setCompany(company);
        ldapObjectUserDetail.setDepartment(department);
        ldapObjectUserDetail.setName(name);

        Assert.assertEquals( company , ldapObjectUserDetail.getCompany() );
        Assert.assertEquals( department , ldapObjectUserDetail.getDepartment() );
        Assert.assertEquals( name , ldapObjectUserDetail.getName() );
        Assert.assertNotNull( ldapObjectUserDetail.toString() );
    }

    @Test
    public void validationUserDTO() {
        userDTO.setUser( "user" );
        userDTO.setPassword( "password" );

        Assert.assertEquals( "user" , userDTO.getUser());
        Assert.assertEquals( "password" , userDTO.getPassword());
        Assert.assertNotNull( userDTO.toString() );
    }

    @Test
    public void validationUserEntity() {
        String valueString = "Value String";
        Integer id = new Integer("1");

        userEntity.setEmail(valueString);
        userEntity.setUsuarioRed(valueString);
        userEntity.setEmail(valueString);
        userEntity.setNombres(valueString);
        userEntity.setEstado( Boolean.TRUE);
        userEntity.setFkIdPerfiles(id);
        userEntity.setIdUsuario(id);

        Assert.assertEquals( id , userEntity.getFkIdPerfiles() );
        Assert.assertEquals( id , userEntity.getIdUsuario() );
        Assert.assertEquals( valueString , userEntity.getEmail() );
        Assert.assertEquals( valueString , userEntity.getNombres() );
        Assert.assertEquals( valueString , userEntity.getUsuarioRed() );
        Assert.assertTrue( userEntity.getEstado() );

        Assert.assertNotNull( userEntity.toString() );
    }

    @Test
    public void validationUserLdapAuthorization(){
        String user = "user";
        String pwd = "pwd";
        String host = "host";
        userLdapAuthorization.setUser(user);
        userLdapAuthorization.setPassword(pwd);
        userLdapAuthorization.setHostDestiny(host);
        Assert.assertEquals( user , userLdapAuthorization.getUser() );
        Assert.assertEquals( pwd , userLdapAuthorization.getPassword());
        Assert.assertEquals( host , userLdapAuthorization.getHostDestiny());
        Assert.assertNotNull( userLdapAuthorization.toString() );
    }

    @Test
    public void validationUserLdapLogin(){
        String user = "user";
        String pwd = "pwd";
        userLdapLogin.setUsername( user );
        userLdapLogin.setPassword( pwd );

        Assert.assertEquals( user , userLdapLogin.getUsername() );
        Assert.assertEquals( pwd , userLdapLogin.getPassword());
        Assert.assertNotNull( userLdapLogin.toString() );
    }

    @Test
    public void UserResponsePermision(){
        userCreateDTO.toString();
        List<String> array =new ArrayList<>();
        userResponsePermision.getProfile();
        userResponsePermision.getPermisions();
        userResponsePermision.toString();

        Assert.assertEquals( "",userResponsePermision.getProfile() );
        Assert.assertEquals( array , userResponsePermision.getPermisions());
        Assert.assertNotNull( userLdapLogin.toString() );
    }


}
