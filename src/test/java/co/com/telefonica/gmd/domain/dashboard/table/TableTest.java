package co.com.telefonica.gmd.domain.dashboard.table;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TableTest {
    private TableDTO tableDTO;
    private TableListQueryDTO tableListQuery;

    @Before
    public void setup(){
        tableDTO = new TableDTO();
        tableListQuery = new TableListQueryDTO();
    }

    @Test
    public void testTableDTO(){
        Integer value = 1;
        tableDTO.getFecha();
        tableDTO.getDepartamento();
        tableDTO.getLocalidad();
        tableDTO.getDiagnostico();
        tableDTO.getDistribuidor();
        tableDTO.getNombreElemento();
        tableDTO.getElemento();
        tableDTO.getUsuario();
        tableDTO.getAfectados();
        tableDTO.getTipoPlanta();
        tableDTO.getEstado();
        tableDTO.setEstado(true);
        tableDTO.setFecha("String");
        tableDTO.setDepartamento("String");
        tableDTO.setLocalidad("String");
        tableDTO.setDiagnostico("String");
        tableDTO.setDistribuidor("String");
        tableDTO.setNombreElemento("String");
        tableDTO.setElemento("String");
        tableDTO.setUsuario("String");
        tableDTO.setAfectados(1);
        tableDTO.setTipoPlanta("String");
        tableDTO.toString();

        Assert.assertEquals("String", tableDTO.getFecha());
        Assert.assertEquals("String", tableDTO.getDepartamento());
        Assert.assertEquals("String",tableDTO.getLocalidad());
        Assert.assertEquals("String",tableDTO.getDiagnostico());
        Assert.assertEquals("String",tableDTO.getDistribuidor());
        Assert.assertEquals("String",tableDTO.getNombreElemento());
        Assert.assertEquals("String",tableDTO.getElemento());
        Assert.assertEquals("String",tableDTO.getUsuario());
        Assert.assertEquals(value,tableDTO.getAfectados());
        Assert.assertEquals("String",tableDTO.getTipoPlanta());
        Assert.assertTrue(tableDTO.getEstado());

    }

    @Test
    public void testTableListQuery(){

        Integer value = 1;
        tableListQuery.toString();
        tableListQuery.getId();
        tableListQuery.getFecha();
        tableListQuery.getDepartamento();
        tableListQuery.getLocalidad();
        tableListQuery.getDiagnostico();
        tableListQuery.getDistribuidor();
        tableListQuery.getNombreElemento();
        tableListQuery.getElemento();
        tableListQuery.getUsuario();
        tableListQuery.getAfectados();
        tableListQuery.getGravedad();
        tableListQuery.getTipoPlanta();
        tableListQuery.getEstado();
        tableListQuery.setId(1);
        tableListQuery.setFecha("String");
        tableListQuery.setDepartamento("String");
        tableListQuery.setLocalidad("String");
        tableListQuery.setDiagnostico("String");
        tableListQuery.setDistribuidor("String");
        tableListQuery.setNombreElemento("String");
        tableListQuery.setElemento("String");
        tableListQuery.setUsuario("String");
        tableListQuery.setAfectados(1);
        tableListQuery.setGravedad("String");
        tableListQuery.setTipoPlanta("String");
        tableListQuery.setEstado(true);

        Assert.assertEquals(value,tableListQuery.getId() );
        Assert.assertEquals("String",tableListQuery.getFecha());
        Assert.assertEquals("String",tableListQuery.getDepartamento());
        Assert.assertEquals("String",tableListQuery.getLocalidad());
        Assert.assertEquals("String",tableListQuery.getDiagnostico());
        Assert.assertEquals("String",tableListQuery.getDistribuidor());
        Assert.assertEquals("String",tableListQuery.getNombreElemento());
        Assert.assertEquals("String",tableListQuery.getElemento());
        Assert.assertEquals("String",tableListQuery.getUsuario());
        Assert.assertEquals(value,tableListQuery.getAfectados());
        Assert.assertEquals("String",tableListQuery.getGravedad());
        Assert.assertEquals("String",tableListQuery.getTipoPlanta());
        Assert.assertTrue(tableListQuery.getEstado());
    }
}
