package co.com.telefonica.gmd.domain.dashboard.boxesaffected;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BoxesAffectedDTOTest {

    private BoxesAffectedDTO boxesAffectedDTO;
    private BoxesAffectedEntity boxesAffectedEntity;

    @Before
    public void setup(){
        boxesAffectedDTO = new BoxesAffectedDTO();
        boxesAffectedEntity = new BoxesAffectedEntity();
    }

    @Test
    public void testBoxesAffectedDTO(){
        Integer value = 1;
        boxesAffectedDTO.toString();
        boxesAffectedDTO.getIdCA();
        boxesAffectedDTO.getFkIdAC();
        boxesAffectedDTO.getCajasAfectadas();
        boxesAffectedDTO.setIdCA(1);
        boxesAffectedDTO.setFkIdAC(1);
        boxesAffectedDTO.setCajasAfectadas("String");

        Assert.assertEquals(value,boxesAffectedDTO.getIdCA());
        Assert.assertEquals(value,boxesAffectedDTO.getFkIdAC());
        Assert.assertEquals("String",boxesAffectedDTO.getCajasAfectadas());
    }

    @Test
    public void testBoxesAffectedEntity(){
        Integer value = 1;
        boxesAffectedEntity.toString();
        boxesAffectedEntity.getIdCa();
        boxesAffectedEntity.getFkIdAc();
        boxesAffectedEntity.getCajasAfectadas();
        boxesAffectedEntity.setIdCa(1);
        boxesAffectedEntity.setFkIdAc(1);
        boxesAffectedEntity.setCajasAfectadas("String");

        Assert.assertEquals(value,boxesAffectedEntity.getIdCa());
        Assert.assertEquals(value,boxesAffectedEntity.getFkIdAc());
        Assert.assertEquals("String",boxesAffectedEntity.getCajasAfectadas());
    }
}
