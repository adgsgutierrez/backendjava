package co.com.telefonica.gmd.domain.admin.networktype;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigInteger;

@SpringBootTest
public class NetworkTypeTest {
    private NetworkTypeEntity networkTypeEntity;

    @Before
    public void setup(){
        networkTypeEntity = new NetworkTypeEntity();
    }

    @Test
    public void NetworkTypeEntity(){
        BigInteger id = new BigInteger("1");
        networkTypeEntity.getIdtipored();
        networkTypeEntity.getTipored();
        networkTypeEntity.getEstado();
        networkTypeEntity.toString();
        networkTypeEntity.setIdtipored(BigInteger.ONE);


        Assert.assertEquals(id,networkTypeEntity.getIdtipored());
        Assert.assertEquals(null,networkTypeEntity.getTipored());
        Assert.assertEquals(null,networkTypeEntity.getEstado());

    }
}
