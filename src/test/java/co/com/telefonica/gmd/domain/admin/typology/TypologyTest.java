package co.com.telefonica.gmd.domain.admin.typology;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigInteger;

@SpringBootTest
public class TypologyTest {
    private TypologyEntity typologyEntity;

    @Before
    public void setup(){
        typologyEntity = new TypologyEntity();
    }

    @Test
    public void TypologyEntity(){
        BigInteger id = new BigInteger("1");

        typologyEntity.getIdtipologia();
        typologyEntity.getTipologia();
        typologyEntity.getEstado();
        typologyEntity.toString();
        typologyEntity.setIdtipologia(BigInteger.ONE);

        Assert.assertEquals(id,typologyEntity.getIdtipologia());
        Assert.assertEquals(null,typologyEntity.getTipologia());
        Assert.assertEquals(null,typologyEntity.getEstado());


    }
}
