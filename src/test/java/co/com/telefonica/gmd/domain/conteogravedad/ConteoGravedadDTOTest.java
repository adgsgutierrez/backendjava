package co.com.telefonica.gmd.domain.conteogravedad;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ConteoGravedadDTOTest {

    private ConteoGravedadGeneralDTO conteoGravedadGeneralDTO;

    @Before
    public void setup(){
        conteoGravedadGeneralDTO = new ConteoGravedadGeneralDTO();
    }

    @Test
    public void testConteoGravedadGeneralDTO(){

        Integer value = 1;
        conteoGravedadGeneralDTO.getConteoPe();
        conteoGravedadGeneralDTO.getConteoPi();
        conteoGravedadGeneralDTO.getInactivos();
        conteoGravedadGeneralDTO.getActivos();
        conteoGravedadGeneralDTO.getMenor();
        conteoGravedadGeneralDTO.getMayor();
        conteoGravedadGeneralDTO.getCritica();
        conteoGravedadGeneralDTO.setConteoPe(1);
        conteoGravedadGeneralDTO.setConteoPi(1);
        conteoGravedadGeneralDTO.setInactivos(1);
        conteoGravedadGeneralDTO.setActivos(1);
        conteoGravedadGeneralDTO.setMenor(1);
        conteoGravedadGeneralDTO.setMayor(1);
        conteoGravedadGeneralDTO.setCritica(1);
        conteoGravedadGeneralDTO.toString();


        Assert.assertEquals(value,conteoGravedadGeneralDTO.getConteoPe());
        Assert.assertEquals(value,conteoGravedadGeneralDTO.getConteoPi());
        Assert.assertEquals(value,conteoGravedadGeneralDTO.getInactivos());
        Assert.assertEquals(value,conteoGravedadGeneralDTO.getActivos());
        Assert.assertEquals(value,conteoGravedadGeneralDTO.getMenor());
        Assert.assertEquals(value,conteoGravedadGeneralDTO.getMayor());
        Assert.assertEquals(value,conteoGravedadGeneralDTO.getCritica());


    }
}
