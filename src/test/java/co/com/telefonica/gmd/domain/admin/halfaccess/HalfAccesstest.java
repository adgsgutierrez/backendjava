package co.com.telefonica.gmd.domain.admin.halfaccess;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class HalfAccesstest {
    private HalfAccessEntity halfAccessEntity;

    @Before
    public void setup(){
        halfAccessEntity = new HalfAccessEntity();
    }

    @Test
    public void HalfAccessEntity(){
        Integer id = 1;
        halfAccessEntity.getIdma();
        halfAccessEntity.getMediosacceso();
        halfAccessEntity.getEstado();
        halfAccessEntity.toString();
        halfAccessEntity.setIdma(id);

        Assert.assertEquals(id,halfAccessEntity.getIdma());
        Assert.assertEquals(null,halfAccessEntity.getMediosacceso());
        Assert.assertEquals(null,halfAccessEntity.getEstado());

    }
}
