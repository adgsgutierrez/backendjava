package co.com.telefonica.gmd.domain.dashboard.department;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DepartmentTest {

    private DepartmentDTO departmentDTO;
    private DepartmentEntity departmentEntity;

    @Before
    public void setup(){
        departmentDTO = new DepartmentDTO();
        departmentEntity = new DepartmentEntity();
    }

    @Test
    public void testDepartmentDTO(){
        Integer value = 1;
        departmentDTO.toString();
        departmentDTO.getIdDpto();
        departmentDTO.getDepartamento();
        departmentDTO.setIdDpto(1);
        departmentDTO.setDepartamento("String");

        Assert.assertEquals(value,departmentDTO.getIdDpto());
        Assert.assertEquals("String",departmentDTO.getDepartamento());
    }

    @Test
    public void testDepartmentEntity(){
        Integer value = 1;
        departmentEntity.toString();
        departmentEntity.getIdDpto();
        departmentEntity.getDepartamento();
        departmentEntity.setIdDpto(1);
        departmentEntity.setDepartamento("String");

        Assert.assertEquals(value, departmentEntity.getIdDpto());
        Assert.assertEquals("String",departmentEntity.getDepartamento());
    }
}
