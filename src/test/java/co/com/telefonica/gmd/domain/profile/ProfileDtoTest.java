package co.com.telefonica.gmd.domain.profile;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProfileDtoTest {

    private ProfileFilterDTO profileFilterDTO;
    private ProfileEntity profileEntity;

    @Before
    public void setup(){

        profileFilterDTO = new ProfileFilterDTO();
        profileEntity = new ProfileEntity();
    }

    @Test
    public void testProfileFilterDTO(){
        Integer value = 1;
        profileFilterDTO.toString();
        profileFilterDTO.getGetIdUsuario();
        profileFilterDTO.getGetPerfil();
        profileFilterDTO.getGetNombre();
        profileFilterDTO.getGetEstado();
        profileFilterDTO.getGetEmail();
        profileFilterDTO.getGetUsuarioRed();
        profileFilterDTO.setGetIdUsuario(1);
        profileFilterDTO.setGetPerfil("String");
        profileFilterDTO.setGetNombre("String");
        profileFilterDTO.setGetEstado(true);
        profileFilterDTO.setGetEmail("String");
        profileFilterDTO.setGetUsuarioRed("String");

        Assert.assertEquals(value,profileFilterDTO.getGetIdUsuario());
        Assert.assertEquals("String",profileFilterDTO.getGetPerfil());
        Assert.assertEquals("String",profileFilterDTO.getGetNombre());
        Assert.assertEquals("String",profileFilterDTO.getGetEmail());
        Assert.assertEquals("String",profileFilterDTO.getGetUsuarioRed());
        Assert.assertTrue(profileFilterDTO.getGetEstado());
    }

    @Test
    public void testProfileEntity(){
        Integer value = 1;
        profileEntity.toString();
        profileEntity.getIdPerfiles();
        profileEntity.getPerfiles();
        profileEntity.setIdPerfiles(1);
        profileEntity.setPerfiles("String");

        Assert.assertEquals(value, profileEntity.getIdPerfiles());
        Assert.assertEquals("String",profileEntity.getPerfiles());
    }
}
