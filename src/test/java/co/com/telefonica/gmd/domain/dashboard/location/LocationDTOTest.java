package co.com.telefonica.gmd.domain.dashboard.location;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LocationDTOTest {

    private LocationDTO locationDTO;

    @Before
    public void setup(){
        locationDTO = new LocationDTO();
    }

    @Test
    public void testLocationDTO(){
        Integer value = 1;
        locationDTO.toString();
        locationDTO.getIdLocalidad();
        locationDTO.getLocalidad();
        locationDTO.getCodigoDane();
        locationDTO.setIdLocalidad(1);
        locationDTO.setLocalidad("String");
        locationDTO.setCodigoDane(1);

        Assert.assertEquals(value,locationDTO.getIdLocalidad());
        Assert.assertEquals("String",locationDTO.getLocalidad());
        Assert.assertEquals(value,locationDTO.getCodigoDane());
    }
}
