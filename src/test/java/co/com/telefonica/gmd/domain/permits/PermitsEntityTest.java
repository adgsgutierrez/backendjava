package co.com.telefonica.gmd.domain.permits;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PermitsEntityTest {

    private PermitsEntity permitsEntity;
    private PermitsDTO permitsDTO;

    @Before
    public void setup(){
        permitsEntity = new PermitsEntity();
        permitsDTO = new PermitsDTO();
    }

    @Test
    public void testPermitsEntity(){
        Integer value = 1;
        permitsEntity.toString();
        permitsEntity.getIdRelacionPerfilesPermisos();
        permitsEntity.getIdperfiles();
        permitsEntity.getIdpermisos();
        permitsEntity.getEstado();
        permitsEntity.setIdRelacionPerfilesPermisos(1);
        permitsEntity.setIdperfiles(1);
        permitsEntity.setIdpermisos(1);
        permitsEntity.setEstado(true);
        permitsDTO.toString();

        Assert.assertEquals(value,permitsEntity.getIdRelacionPerfilesPermisos());
        Assert.assertEquals(value,permitsEntity.getIdperfiles());
        Assert.assertEquals(value,permitsEntity.getIdpermisos());
        Assert.assertTrue(permitsEntity.getEstado());
    }
}
