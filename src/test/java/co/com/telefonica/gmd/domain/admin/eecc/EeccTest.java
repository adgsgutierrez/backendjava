package co.com.telefonica.gmd.domain.admin.eecc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class EeccTest {
    private EeccEntity eeccEntity;

    @Before
    public void setup(){
        eeccEntity = new EeccEntity();
    }

    @Test
    public void EeccEntity(){
        Integer id =1;

        eeccEntity.getIdeecc();
        eeccEntity.getProveedorContratista();
        eeccEntity.getEstado();
        eeccEntity.toString();
        eeccEntity.setIdeecc(id);

        Assert.assertEquals(id,eeccEntity.getIdeecc());
        Assert.assertEquals(null,eeccEntity.getProveedorContratista());
        Assert.assertEquals(null,eeccEntity.getEstado());

    }
}
