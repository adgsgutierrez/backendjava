package co.com.telefonica.gmd.domain.notification;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class NofiticationTest {

    private NotificationEntity notificationEntity;
    private NotificationDTO notificationDTO;

    @Before
    public void setup(){
        notificationEntity = new NotificationEntity();
        notificationDTO = new NotificationDTO();
    }

    @Test
    public void testNotificationEntity(){
        Integer id = new Integer("1");
        notificationEntity.getIdNotificacion();
        notificationEntity.getIdUsuario();
        notificationEntity.getNotification();
        notificationEntity.getTipoNotificacion();
        notificationEntity.getEstado();
        notificationEntity.getFecha();
        notificationEntity.getHora();
        notificationEntity.toString();
        notificationEntity.setIdNotificacion(1);

        Assert.assertEquals(id,notificationEntity.getIdNotificacion());
        Assert.assertEquals(null,notificationEntity.getIdUsuario());
        Assert.assertEquals(null,notificationEntity.getNotification());
        Assert.assertEquals(null,notificationEntity.getTipoNotificacion());
        Assert.assertEquals(null,notificationEntity.getEstado());
        Assert.assertEquals(null,notificationEntity.getFecha());
    }

    @Test
    public void testNotificationDTO(){
    }
}
