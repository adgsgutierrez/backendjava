package co.com.telefonica.gmd.domain.gravedad;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GravedadTest {

    private GravedadEntity gravedadEntity;
    private GravedadDTO gravedadDTO;

    @Before
    public void setup(){
        gravedadDTO = new GravedadDTO();
        gravedadEntity = new GravedadEntity();
    }

    @Test
    public void testGravedadDTO(){
        Integer id =1;
        gravedadDTO.getTipoGravedad();
        gravedadDTO.getHasta();
        gravedadDTO.getDesde();
        gravedadDTO.getIdGravedad();
        gravedadDTO.setTipoGravedad("String");
        gravedadDTO.setHasta(1);
        gravedadDTO.setDesde(1);
        gravedadDTO.setIdGravedad(1);
        gravedadDTO.toString();

        Assert.assertEquals("String",gravedadDTO.getTipoGravedad());
        Assert.assertEquals(id,gravedadDTO.getHasta());
        Assert.assertEquals(id,gravedadDTO.getDesde());
        Assert.assertEquals(id,gravedadDTO.getIdGravedad());

    }

    @Test
    public void testGravedadEntity(){
        Integer id =1;
        gravedadEntity.getHasta();
        gravedadEntity.getDesde();
        gravedadEntity.getIdGravedad();
        gravedadEntity.getTipoGravedad();
        gravedadEntity.setTipoGravedad("String");
        gravedadEntity.setHasta(1);
        gravedadEntity.setDesde(1);
        gravedadEntity.setIdGravedad(1);
        ;

        Assert.assertEquals("String",gravedadEntity.getTipoGravedad());
        Assert.assertEquals(id,gravedadEntity.getHasta());
        Assert.assertEquals(id,gravedadEntity.getDesde());
        Assert.assertEquals(id,gravedadEntity.getIdGravedad());
    }
}
