package co.com.telefonica.gmd.domain.internalplant;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class InternalPlantTest {
    private InternalPlantEntity internalPlantEntity;
    private InternalPlantCreateDTO internalPlantCreateDTO;

    @Before
    public void setup(){
        internalPlantEntity = new InternalPlantEntity();
        internalPlantCreateDTO = new InternalPlantCreateDTO();
    }

    @Test
    public void internalPlanEntity(){
        Integer id = new Integer("1");

        internalPlantEntity.toString();
        internalPlantEntity.getIdPI();
        internalPlantEntity.getFkIdUsuario();
        internalPlantEntity.getFkIdEecc();
        internalPlantEntity.getIdMa();
        internalPlantEntity.getFechaHoraRegistro();
        internalPlantEntity.getTecnicoReporta();
        internalPlantEntity.getCelularTecnico();
        internalPlantEntity.getDesFalla();
        internalPlantEntity.getRangoElementosAfectados();
        internalPlantEntity.getClientesAfectados();
        internalPlantEntity.getResponsableAtencionDanoMasivo();
        internalPlantEntity.getViabilidadTicket();
        internalPlantEntity.getDistribuidor();
        internalPlantEntity.getLocalidad();
        internalPlantEntity.getDepartamento();
        internalPlantCreateDTO.setIdPI(1);
        internalPlantCreateDTO.setDistribuidor("String");
        internalPlantCreateDTO.setLocalidad("String");
        internalPlantCreateDTO.setDepartamento("String");

        Assert.assertEquals(null,internalPlantEntity.getIdPI() );
        Assert.assertEquals(null,internalPlantEntity.getFkIdUsuario());
        Assert.assertEquals(null,internalPlantEntity.getFkIdEecc());
        Assert.assertEquals(null,internalPlantEntity.getIdMa());
        Assert.assertEquals(null, internalPlantEntity.getFechaHoraRegistro());
        Assert.assertEquals(null,internalPlantEntity.getTecnicoReporta());
        Assert.assertEquals(null,internalPlantEntity.getCelularTecnico());
        Assert.assertEquals(null,internalPlantEntity.getCelularTecnico());
        Assert.assertEquals(null,internalPlantEntity.getDesFalla());
        Assert.assertEquals(null,internalPlantEntity.getRangoElementosAfectados());
        Assert.assertEquals(null,internalPlantEntity.getClientesAfectados());
        Assert.assertEquals(null,internalPlantEntity.getResponsableAtencionDanoMasivo());
        Assert.assertEquals(null,internalPlantEntity.getViabilidadTicket());
        Assert.assertEquals("String",internalPlantCreateDTO.getDistribuidor());
        Assert.assertEquals("String", internalPlantCreateDTO.getLocalidad());
        Assert.assertEquals("String", internalPlantCreateDTO.getDepartamento());
        Assert.assertEquals(id, internalPlantCreateDTO.getIdPI());


    }

}
