package co.com.telefonica.gmd.domain.externalplant;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ExternalPlanTest {
    private ExternalPlantEntity externalPlantEntity;
    private ExternalPlantCreateDTO externalPlantCreateDTO;

    @Before
    public void setup(){
        externalPlantCreateDTO = new ExternalPlantCreateDTO();
        externalPlantEntity = new ExternalPlantEntity();
    }

    @Test
    public void externalPlanEntity(){
        Integer id = new Integer("1");

        externalPlantEntity.getFechaHoraRegistro();
        externalPlantEntity.getCelularTecnico();
        externalPlantEntity.getClientesAfectados();
        externalPlantEntity.getDesFalla();
        externalPlantEntity.getFkIdEecc();
        externalPlantEntity.getFkIdMa();
        externalPlantEntity.getFkIdTipologia();
        externalPlantEntity.getFkIdTipoRed();
        externalPlantEntity.getFkIdUsuario();
        externalPlantEntity.getIdPE();
        externalPlantEntity.getViabilidadTicket();
        externalPlantEntity.toString();
        externalPlantEntity.getTecnicoReporta();
        externalPlantEntity.getDireccion();
        externalPlantEntity.getBarrio();
        externalPlantEntity.getCajasAfectadas();
        externalPlantEntity.getArmarioCable();
        externalPlantEntity.getDistribuidor();
        externalPlantEntity.getLocalidad();
        externalPlantEntity.getDepartamento();
        externalPlantEntity.setIdPE(1);
        externalPlantCreateDTO.setDireccion("String");
        externalPlantCreateDTO.setBarrio("String");
        externalPlantCreateDTO.setCajasAfectadas("String");
        externalPlantCreateDTO.setArmarioCable("String");
        externalPlantCreateDTO.setDistribuidor("String");
        externalPlantCreateDTO.setLocalidad("String");
        externalPlantCreateDTO.setDepartamento("String");

        Assert.assertEquals( null, externalPlantEntity.getFechaHoraRegistro() );
        Assert.assertEquals( null,externalPlantEntity.getCelularTecnico() );
        Assert.assertEquals( null,externalPlantEntity.getClientesAfectados() );
        Assert.assertEquals( null,externalPlantEntity.getDesFalla() );
        Assert.assertEquals( null,externalPlantEntity.getFkIdEecc() );
        Assert.assertEquals( null,externalPlantEntity.getFkIdMa() );
        Assert.assertEquals( null,externalPlantEntity.getFkIdTipologia() );
        Assert.assertEquals( null,externalPlantEntity.getFkIdTipoRed() );
        Assert.assertEquals( null,externalPlantEntity.getFkIdUsuario() );
        Assert.assertEquals( id,externalPlantEntity.getIdPE() );
        Assert.assertEquals( null,externalPlantEntity.getViabilidadTicket() );
        Assert.assertEquals( null, externalPlantEntity.getTecnicoReporta() );
        Assert.assertEquals("String",externalPlantCreateDTO.getDireccion());
        Assert.assertEquals("String",externalPlantCreateDTO.getBarrio());
        Assert.assertEquals("String",externalPlantCreateDTO.getCajasAfectadas());
        Assert.assertEquals("String",externalPlantCreateDTO.getArmarioCable());
        Assert.assertEquals("String",externalPlantCreateDTO.getDistribuidor());
        Assert.assertEquals("String",externalPlantCreateDTO.getLocalidad());
        Assert.assertEquals("String",externalPlantCreateDTO.getDepartamento());



    }
}
