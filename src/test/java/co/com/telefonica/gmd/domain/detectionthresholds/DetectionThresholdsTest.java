package co.com.telefonica.gmd.domain.detectionthresholds;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DetectionThresholdsTest {

    private ElementsEntity elementsEntity;

    private ThresholdDTO thresholdDTO;

    private ThresholdsEntity thresholdsEntity;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        elementsEntity = new ElementsEntity();
        thresholdDTO = new ThresholdDTO();
        thresholdsEntity = new ThresholdsEntity();

    }

    @Test
    public void testElementsEntity(){
        Integer value = 1;
        elementsEntity.toString();
        elementsEntity.getIdElemento();
        elementsEntity.getElemento();
        elementsEntity.setIdElemento(1);
        elementsEntity.setElemento("String");

        Assert.assertEquals(value,elementsEntity.getIdElemento());
        Assert.assertEquals("String",elementsEntity.getElemento());
    }

    @Test
    public void testThresholdDTO(){

        Integer value = 1;
        thresholdDTO.toString();
        thresholdDTO.getIdElemento();
        thresholdDTO.getPorcConexion();
        thresholdDTO.getPorcDesconexion();
        thresholdDTO.getVolDesconexion();
        thresholdDTO.getVentanaTiempo();
        thresholdDTO.setIdElemento(1);

        Assert.assertEquals(value, thresholdDTO.getIdElemento());
    }

    @Test
    public void testThresholdsEntity(){
        Integer value = 1;
        thresholdsEntity.toString();
        thresholdsEntity.getIdUmbrales();
        thresholdsEntity.getFK_idElemento();
        thresholdsEntity.getPorcConexion();
        thresholdsEntity.getPorcDesconexion();
        thresholdsEntity.getVolDesconexion();
        thresholdsEntity.getVentanaTiempo();
        thresholdsEntity.setIdUmbrales(1);
        thresholdsEntity.setFK_idElemento(1);
        thresholdsEntity.setPorcConexion(1);
        thresholdsEntity.setPorcDesconexion(1);
        thresholdsEntity.setVolDesconexion(1);
        thresholdsEntity.setVentanaTiempo(1);

        Assert.assertEquals(value,thresholdsEntity.getIdUmbrales());
        Assert.assertEquals(value,thresholdsEntity.getFK_idElemento());
        Assert.assertEquals(value,thresholdsEntity.getPorcConexion());
        Assert.assertEquals(value,thresholdsEntity.getPorcDesconexion());
        Assert.assertEquals(value,thresholdsEntity.getVolDesconexion());
        Assert.assertEquals(value,thresholdsEntity.getVentanaTiempo());
    }
}
