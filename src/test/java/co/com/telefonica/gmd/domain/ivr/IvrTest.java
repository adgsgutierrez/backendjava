package co.com.telefonica.gmd.domain.ivr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigInteger;

@SpringBootTest
public class IvrTest {

    private IvrDetailEntity ivrDetailEntity;
    private IvrEntity ivrEntity;
    private java.lang.Object Object;

    @Before
    public void setup(){
        ivrDetailEntity = new IvrDetailEntity();
        ivrEntity = new IvrEntity();
    }

    @Test
    public void ivrDetailEntity(){
        BigInteger id = new BigInteger("1");
        Object obj = Object;
        ivrDetailEntity.equals(obj);
        ivrDetailEntity.hashCode();
        ivrDetailEntity.getIdIvr();
        ivrDetailEntity.getFkIdTipologia();
        ivrDetailEntity.getFkIdCargaIvr();
        ivrDetailEntity.getViabilidadTicket();
        ivrDetailEntity.getAbonado();
        ivrDetailEntity.getFechaFin();
        ivrDetailEntity.getHoraFin();
        ivrDetailEntity.getTipoDanio();
        ivrDetailEntity.getFlag1();
        ivrDetailEntity.getFlag2();
        ivrDetailEntity.setIdIvr(BigInteger.ONE);

        Assert.assertEquals(id,ivrDetailEntity.getIdIvr());
        Assert.assertEquals(null,ivrDetailEntity.getFkIdTipologia());
        Assert.assertEquals(null,ivrDetailEntity.getFkIdCargaIvr());
        Assert.assertEquals(null,ivrDetailEntity.getViabilidadTicket());
        Assert.assertEquals(null,ivrDetailEntity.getAbonado());
        Assert.assertEquals(null,ivrDetailEntity.getFechaFin());
        Assert.assertEquals(null,ivrDetailEntity.getHoraFin());
        Assert.assertEquals(null,ivrDetailEntity.getTipoDanio());
        Assert.assertEquals(null,ivrDetailEntity.getFlag1());
        Assert.assertEquals(null,ivrDetailEntity.getFlag2());

    }

    @Test
    public void ivrEntity(){
        ivrEntity.getFkIdUsuario();
        ivrEntity.getNombreArchivo();
        ivrEntity.getFechaCarga();
        ivrEntity.getClientesAfectados();
        ivrEntity.getEstado();

        Assert.assertEquals(null,ivrEntity.getFkIdUsuario());
        Assert.assertEquals(null,ivrEntity.getNombreArchivo());
        Assert.assertEquals(null,ivrEntity.getFechaCarga());
        Assert.assertEquals(null,ivrEntity.getClientesAfectados());
        Assert.assertEquals(null,ivrEntity.getEstado());

    }
}
