package co.com.telefonica.gmd.domain.dashboard.cablecloset;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CableClosetDTOTest {

    private CableClosetDTO cableClosetDTO;

    @Before
    public void setup(){
        cableClosetDTO = new CableClosetDTO();
    }

    @Test
    public void testCableClosetDTO(){
        Integer value = 1;
        cableClosetDTO.toString();
        cableClosetDTO.getIdAC();
        cableClosetDTO.getArmarioCable();
        cableClosetDTO.setIdAC(1);
        cableClosetDTO.setArmarioCable("String");

        Assert.assertEquals(value,cableClosetDTO.getIdAC());
        Assert.assertEquals("String",cableClosetDTO.getArmarioCable());
    }
}
