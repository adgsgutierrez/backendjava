package co.com.telefonica.gmd.application.usescase.admin.halfaccess;

import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.HashMap;


@SpringBootTest
public class HalfAccessListServiceTest {

    @Mock
    private IHalfAccessRepository halfAccessRepository;

    private Response response;

    private HalfAccessListService halfAccessListService;

    private Pageable pageable;
    private HashMap<String, String> hashMap;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();

        hashMap = new HashMap<>();
        hashMap.put("page" , "0");
        hashMap.put("items" , "25");
        pageable = new Pageable() {
            @Override
            public int getPageNumber() {
                return 0;
            }

            @Override
            public int getPageSize() {
                return 0;
            }

            @Override
            public long getOffset() {
                return 0;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public Pageable next() {
                return null;
            }

            @Override
            public Pageable previousOrFirst() {
                return null;
            }

            @Override
            public Pageable first() {
                return null;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }
        };

        //Mockito.when( halfAccessRepository.getListHalfAccess() ).thenReturn();

        halfAccessListService = new HalfAccessListService();

        ReflectionTestUtils.setField( halfAccessListService, "halfAccessRepository" , halfAccessRepository);
        ReflectionTestUtils.setField( halfAccessListService, "response" , response);

    }

    @Test
    public void getHalfAccessListService(){
        Response response = halfAccessListService.get(hashMap);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

}
