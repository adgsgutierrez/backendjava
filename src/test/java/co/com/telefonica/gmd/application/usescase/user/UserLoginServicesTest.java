package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserProfileRepository;
import co.com.telefonica.gmd.application.repositories.service.login.ILoginLdapService;
import co.com.telefonica.gmd.domain.user.UserDTO;
import co.com.telefonica.gmd.domain.user.UserLdapLoginResponse;
import co.com.telefonica.gmd.domain.user.UserProfileQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class UserLoginServicesTest {

    @Mock
    private ILoginLdapService ldapLogin;
    @Mock
    private IUserProfileRepository userRepository;
    private Response response;
    private IUser userService;
    private UserDTO userDTOTestInvalid;
    private UserDTO userDTOTestValid;
    private UserDTO userDTOTestValidDontDatabase;
    private UserProfileQuery userResponse;
    private static MockedStatic<FunctionsTransform> functTrans;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        functTrans = Mockito.mockStatic(FunctionsTransform.class);
        response = new Response();
        userDTOTestInvalid = new UserDTO();
        userDTOTestValid = new UserDTO();
        userDTOTestValidDontDatabase = new UserDTO();
        userDTOTestValid.setUser("UserName 1");
        userDTOTestValid.setPassword("Password 1");
        userDTOTestInvalid.setUser("UserName 2");
        userDTOTestInvalid.setPassword("Password 2");
        userDTOTestValidDontDatabase.setUser("UserName 3");
        userDTOTestValidDontDatabase.setPassword("Password 3");
        functTrans.when(
                () -> FunctionsTransform.decodeStringToBase64WithIteration("UserName 1"))
                .thenReturn("UserName 1");
        functTrans.when(
                () -> FunctionsTransform.decodeStringToBase64WithIteration("UserName 3"))
                .thenReturn("UserName 3");

        userService = new UserLoginService();
        userResponse = new UserProfileQuery() {
            @Override
            public String getNombres() {
                return "Nombres";
            }

            @Override
            public String getPerfiles() {
                return "perfiles";
            }

            @Override
            public String getPermisos() {
                return "permisos";
            }

            @Override
            public String getUsuarioRed() {
                return "usuario";
            }

            @Override
            public String getId() {
                return "ID";
            }
        };
        ArrayList<UserProfileQuery> listUserResponse = new ArrayList<>();
        listUserResponse.add(userResponse);
        Mockito.when( ldapLogin.login(userDTOTestInvalid) ).thenReturn(null);
        Mockito.when( ldapLogin.login(userDTOTestValid) ).thenReturn(new UserLdapLoginResponse());
        Mockito.when( ldapLogin.login(userDTOTestValidDontDatabase) ).thenReturn(new UserLdapLoginResponse());

        Mockito.when( userRepository.findUserWithProfile("UserName 1")).thenReturn(listUserResponse);
        Mockito.when( userRepository.findUserWithProfile("UserName 3")).thenThrow( NullPointerException.class);

        ReflectionTestUtils.setField( userService , "ldapLogin" , ldapLogin);
        ReflectionTestUtils.setField( userService , "userRepository" , userRepository);
        ReflectionTestUtils.setField( userService , "response" , response);
    }

    @Test
    public void userDontExistLdap(){
        Response responseService = userService.get(userDTOTestInvalid);
        Integer code = 301;
        String message = "El usuario no se encuentra en el sistema y/o las credenciales son incorrectas";
        Assert.assertEquals(code , responseService.getCode());
        Assert.assertEquals(message , responseService.getMessage());
    }
    @Test
    public void userSuccessFull(){
        Response responseService = userService.get(userDTOTestValid);
        Integer code = 200;
        String message = "ok";
        Assert.assertEquals(code , responseService.getCode());
        Assert.assertEquals(message , responseService.getMessage());
    }


    @After
    public void endTest(){
        functTrans.close();
    }
}
