package co.com.telefonica.gmd.application.usescase.profile;

import co.com.telefonica.gmd.application.repositories.database.profile.IProfileRepository;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class ProfileServicetest {

    @Mock
    private IProfileRepository profileRepository;

    private Response response;
    private ProfileService profileService;
    private Object obj;

    public static final Integer CODE = 200;
    public static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        ArrayList list = new ArrayList();

        Mockito.when(profileRepository.getProfile()).thenReturn(list);

        profileService = new ProfileService();

        ReflectionTestUtils.setField(profileService, "profileRepository", profileRepository);
        ReflectionTestUtils.setField(profileService, "response", response);
    }

    @Test
    public void testProfileService(){
        profileService.get(obj);
        Assert.assertEquals(CODE, response.getCode());
    }
}
