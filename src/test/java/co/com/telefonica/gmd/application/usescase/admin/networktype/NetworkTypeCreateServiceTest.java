package co.com.telefonica.gmd.application.usescase.admin.networktype;

import co.com.telefonica.gmd.application.repositories.database.admin.networktype.INetworkTypeRepository;
import co.com.telefonica.gmd.domain.admin.networktype.NetworkTypeEntity;
import co.com.telefonica.gmd.domain.admin.networktype.NetworktTypeDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class NetworkTypeCreateServiceTest {
    @Mock
    private INetworkTypeRepository networkTypeRepository;

    private Response response;
    private NetworktTypeDTO networktTypeDTO;
    private NetworkTypeEntity networkTypeEntity;

    private NetworkTypeCreateService networkTypeCreateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        response = new Response();

        networktTypeDTO = new NetworktTypeDTO();
        networktTypeDTO.setTipored("tipored");

        networkTypeEntity = new NetworkTypeEntity();
        networkTypeEntity.setTipored("tipored");

        Mockito.when( networkTypeRepository.save(networkTypeEntity) ).thenReturn(new NetworkTypeEntity());

        networkTypeCreateService = new NetworkTypeCreateService();
        ReflectionTestUtils.setField( networkTypeCreateService, "networkTypeRepository" , networkTypeRepository);
        ReflectionTestUtils.setField( networkTypeCreateService, "response" , response);
    }

    @Test
    public void getNetworkTypeCreateService(){
        Response response = networkTypeCreateService.get(networktTypeDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
