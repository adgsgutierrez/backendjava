package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IThresholdsRepository;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdDTO;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdsEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

@SpringBootTest
public class ElementUpdateServiceTest {

    @Mock
    private IThresholdsRepository thresoldRepository;

    private ThresholdDTO thresholdDTO;
    private IThresholds thresholds;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        thresholdDTO = new ThresholdDTO();
        thresholdDTO.setIdElemento(1234);
        ThresholdsEntity entity = new ThresholdsEntity();
        entity.setFK_idElemento(1234);
        entity.setIdUmbrales(1234);
        entity.setPorcConexion(1234);
        entity.setPorcDesconexion(1234);
        entity.setVentanaTiempo(1234);
        entity.setVolDesconexion(1234);
        thresholds = new ElementsUpdateService();
        Mockito.when( thresoldRepository.findById(1234) ).thenReturn(Optional.of(entity));
        ReflectionTestUtils.setField(thresholds , "thresoldRepository" , thresoldRepository);
        ReflectionTestUtils.setField(thresholds , "response" , new Response());
    }


    @Test
   public void testElementsUpdateService() {
        Integer code = 200;
        Response responseService = thresholds.get( new Object[]{thresholdDTO});
        Assert.assertEquals(code , responseService.getCode());
    }
}
