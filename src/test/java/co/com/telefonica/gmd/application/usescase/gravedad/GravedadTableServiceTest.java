package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.application.repositories.database.gravedad.IGravedadRepository;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class GravedadTableServiceTest {

    @Mock
    private IGravedadRepository gravedadRepository;

    private Response response;
    private GravedadTableService gravedadTableService;

    private Object obj;
    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();
        ArrayList list = new ArrayList();

        obj = new Object();

        Mockito.when(gravedadRepository.getTableGravity()).thenReturn(list);

        gravedadTableService = new GravedadTableService();

        ReflectionTestUtils.setField(gravedadTableService, "gravedadRepository", gravedadRepository);
        ReflectionTestUtils.setField(gravedadTableService, "response", response);
    }

    @Test
    public void testGetGravedadTable(){
        gravedadTableService.get(obj);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

}
