package co.com.telefonica.gmd.application.usescase.dashboard.location;

import co.com.telefonica.gmd.application.repositories.database.dashboard.location.ILocationRepository;
import co.com.telefonica.gmd.domain.dashboard.location.LocationListQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class LocationListServiceTest {

    @Mock
    private ILocationRepository locationRepository;

    private Response response;
    private LocationListService locationListService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        ArrayList list = new ArrayList();
        LocationListQuery locationListQuery = new LocationListQuery() {
            @Override
            public Integer getIdLocalidad() {
                return null;
            }

            @Override
            public String getLocalidad() {
                return null;
            }

            @Override
            public Integer getCodigoDane() {
                return null;
            }
        };

        Mockito.when(locationRepository.getLocationByDpto(1)).thenReturn(list);

        locationListService = new LocationListService();

        ReflectionTestUtils.setField(locationListService, "locationRepository", locationRepository);
        ReflectionTestUtils.setField(locationListService, "response", response);

    }

    @Test
    public void testLocationListService(){
        locationListService.get(1);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
