package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IThresholdsRepository;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdsEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class ThresholdsServiceTest {

    @Mock
    private IThresholdsRepository thresholdsRepository;

    private ThresholdsService thresholdsService;
    private Response response;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        Integer id = Integer.parseInt(String.valueOf(1));

        ArrayList<ThresholdsEntity> list = new ArrayList<>();
        Mockito.when(thresholdsRepository.getThresholds(id)).thenReturn(list);

        thresholdsService = new ThresholdsService();

        ReflectionTestUtils.setField(thresholdsService, "thresholdsRepository", thresholdsRepository);
        ReflectionTestUtils.setField(thresholdsService, "response", response);
    }

    @Test
    public void testThresholdsService() {
        Integer id = Integer.parseInt(String.valueOf(1));
        Object[] data = new Object[]{id};
        thresholdsService.get(data);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
