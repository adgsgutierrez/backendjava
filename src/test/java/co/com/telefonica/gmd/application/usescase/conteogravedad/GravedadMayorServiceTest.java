package co.com.telefonica.gmd.application.usescase.conteogravedad;

import co.com.telefonica.gmd.application.repositories.database.conteogravedad.IConteoGravedadRepository;
import co.com.telefonica.gmd.domain.conteogravedad.IConteoGravedadQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;

@SpringBootTest
public class GravedadMayorServiceTest {

    @Mock
    private IConteoGravedadRepository conteoGravedadRepository;

    private Response response;
    private IConteoGravedadQuery conteoGravedadQuery;

    private GravedadMayorService gravedadMayorService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        conteoGravedadQuery = new IConteoGravedadQuery() {
            @Override
            public BigInteger getConteoGravedad() {
                return null;
            }
        };

        Mockito.when( conteoGravedadRepository.getConteoMayor()).thenReturn(conteoGravedadQuery);

        gravedadMayorService = new GravedadMayorService();

        ReflectionTestUtils.setField(gravedadMayorService,"conteoGravedadRepository" , conteoGravedadRepository);
        ReflectionTestUtils.setField(gravedadMayorService, "response", response);
    }

    @Test
    public void testGetMayor(){
        gravedadMayorService.get();
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
