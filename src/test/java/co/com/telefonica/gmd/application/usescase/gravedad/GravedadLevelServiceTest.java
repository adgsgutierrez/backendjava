package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.application.repositories.database.gravedad.IGravedadRepository;
import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class GravedadLevelServiceTest {
    @Mock
    private IGravedadRepository gravedadRepository;

    private Response response;
    private GravedadLevelService gravedadService;
    private GravedadEntity gravedadEntity;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        gravedadEntity =  new GravedadEntity();
        Mockito.when(gravedadRepository.getGravedadWithRange(10)).thenReturn(gravedadEntity);

        gravedadService =  new GravedadLevelService();

        ReflectionTestUtils.setField(gravedadService, "gravedadRepository", gravedadRepository);
        ReflectionTestUtils.setField(gravedadService, "response", response);
    }

    @Test
    public void testGetGravedad(){
        gravedadService.get(10);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
