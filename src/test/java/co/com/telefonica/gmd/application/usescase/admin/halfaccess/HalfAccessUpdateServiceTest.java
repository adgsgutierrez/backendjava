package co.com.telefonica.gmd.application.usescase.admin.halfaccess;

import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;

@SpringBootTest
public class HalfAccessUpdateServiceTest {
    @Mock
    private IHalfAccessRepository halfAccessRepository;

    private Response response;
    private HalfAccessDTO halfAccessDTO;

    private HalfAccessUpdateService halfAccessUpdateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();

        halfAccessDTO = new HalfAccessDTO();
        halfAccessDTO.setIdma(BigInteger.ONE);
        halfAccessDTO.setEstado(true);

        halfAccessUpdateService = new HalfAccessUpdateService();
        ReflectionTestUtils.setField( halfAccessUpdateService, "halfAccessRepository" , halfAccessRepository);
        ReflectionTestUtils.setField( halfAccessUpdateService, "response" , response);
    }

    @Test
    public void getHalfAccessUpdateService(){
        halfAccessUpdateService.get(halfAccessDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
