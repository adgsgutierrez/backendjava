package co.com.telefonica.gmd.application.usescase.profile;

import co.com.telefonica.gmd.application.repositories.database.profile.IProfileFilterRepository;
import co.com.telefonica.gmd.domain.profile.ProfileFilterDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class ProfileFilterServiceTest {

    @Mock
    private IProfileFilterRepository profileFilterRepository;

    private Response response;
    private ProfileFilterService profileFilterService;

    public static final Integer CODE = 200;
    public static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Integer id = new Integer(1);
        Pageable page = PageRequest.of(0,25);
        ProfileFilterDTO profileFilterDTO = new ProfileFilterDTO();
        ArrayList<ProfileFilterDTO> list = new ArrayList<>();
        list.add(profileFilterDTO);
        Page<Iterable<ProfileFilterDTO>> result = new PageImpl(list);
        Mockito.when(profileFilterRepository.getFilterProfile(id,page)).thenReturn(result);

        profileFilterService = new ProfileFilterService();

        ReflectionTestUtils.setField(profileFilterService,"profileFilterRepository", profileFilterRepository);
        ReflectionTestUtils.setField(profileFilterService, "response",response);

    }

    @Test
    public void testProfileService(){
        Object[] args = new Object[]{ "1", "0" , "25"};
        profileFilterService.get(args);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( MESSAGE , response.getMessage());
    }
}
