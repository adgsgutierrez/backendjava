package co.com.telefonica.gmd.application.usescase.admin.typology;

import co.com.telefonica.gmd.application.repositories.database.admin.typology.ITypologyRepository;
import co.com.telefonica.gmd.domain.admin.typology.TypologyDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;

@SpringBootTest
public class TypologyUpdateServiceTest {

    @Mock
    private ITypologyRepository typologyRepository;

    private Response response;
    private TypologyDTO typologyDTO;

    private TypologyUpdateService typologyUpdateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        response = new Response();

        typologyDTO = new TypologyDTO();
        typologyDTO.setIdtipologia(BigInteger.ONE);
        typologyDTO.setEstado(true);

        typologyUpdateService = new TypologyUpdateService();

        ReflectionTestUtils.setField( typologyUpdateService, "typologyRepository" , typologyRepository);
        ReflectionTestUtils.setField( typologyUpdateService, "response" , response);
    }

    @Test
    public void getTypologyUpdateService(){
        typologyUpdateService.get(typologyDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
