package co.com.telefonica.gmd.application.usescase.dashboard.cablecloset;

import co.com.telefonica.gmd.application.repositories.database.dashboard.cablecloset.ICableClosetRepository;
import co.com.telefonica.gmd.domain.dashboard.cablecloset.CableClosetListQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class CableClosetListServiceTest {

    @Mock
    private ICableClosetRepository cableClosetRepository;

    private Response response;
    private CableClosetListService cableClosetListService;

    private static final Integer CODE = 200;
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        CableClosetListQuery cableClosetListQuery = new CableClosetListQuery() {
            @Override
            public Integer getIdAC() {
                return null;
            }

            @Override
            public String getArmarioCable() {
                return null;
            }
        };
        ArrayList list = new ArrayList();

        Mockito.when(cableClosetRepository.getCableClosetByDistributor(1)).thenReturn(list);

        cableClosetListService = new CableClosetListService();

        ReflectionTestUtils.setField(cableClosetListService, "cableClosetRepository", cableClosetRepository);
        ReflectionTestUtils.setField(cableClosetListService, "response", response);
    }

    @Test
    public void testCableClosetListService(){
        cableClosetListService.get(1);
        Assert.assertEquals(CODE, response.getCode());
    }
}
