package co.com.telefonica.gmd.application.usescase.admin.eecc;

import co.com.telefonica.gmd.application.repositories.database.admin.eecc.IEeccRepository;
import co.com.telefonica.gmd.domain.admin.eecc.EeccDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;

@SpringBootTest
public class EeccUpdateServiceTest {
    @Mock
    private IEeccRepository eeccRepository;

    private Response response;
    private EeccDTO eeccDTO;

    private EeccUpdateService eeccUpdateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();

        eeccDTO = new EeccDTO();
        eeccDTO.setIdeecc(BigInteger.ONE);
        eeccDTO.setEstado(true);

        eeccUpdateService = new EeccUpdateService();
        ReflectionTestUtils.setField( eeccUpdateService , "eeccRepository", eeccRepository);
        ReflectionTestUtils.setField( eeccUpdateService , "response" , response);

    }

    @Test
    public void getEeccUpdateService(){
        eeccUpdateService.get(eeccDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
