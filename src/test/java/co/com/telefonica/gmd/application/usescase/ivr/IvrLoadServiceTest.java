package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

@SpringBootTest
public class IvrLoadServiceTest {

    private MockMultipartFile mockFile;

    @Mock
    private IIvrRepository ivrRepository;
    @Mock
    private IIvrDetailRepository ivrDetailRepository;
    @Mock
    private ExecutorService serviceRuntimeMock;

    private Response response;
    private IvrLoadService ivrLoadService;

    public static final Integer CODE = 200;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockFile = new MockMultipartFile(
                "data",
                "filename.txt",
                "text/plain",
                "199335|53200029|10/01/2021|20:12:59|1|53|1|1".getBytes());

        IvrEntity ivrEntity = new IvrEntity();
        ivrEntity.setIdCargarIvr( new BigInteger("1") );
        response = new Response();
        ArrayList<IvrDetailEntity> entities = new ArrayList<>();

        Mockito.when( ivrRepository.save( ArgumentMatchers.any() ) ).thenReturn(ivrEntity);
        Mockito.when( ivrDetailRepository.saveAll(ArgumentMatchers.any())).thenReturn(entities);
        Mockito.when( serviceRuntimeMock.submit( ()->{} ) ).thenReturn(null);
        ivrLoadService = new IvrLoadService();
        ReflectionTestUtils.setField(ivrLoadService ,"ivrRepository" , ivrRepository);
        ReflectionTestUtils.setField(ivrLoadService ,"ivrDetailRepository" , ivrDetailRepository);
        ReflectionTestUtils.setField(ivrLoadService ,"executor" , serviceRuntimeMock);
        ReflectionTestUtils.setField(ivrLoadService ,"response" , response);
    }

    @Test
    public void testUseCaseLoad(){
        Object[] args = new Object[]{};
        Response responseService = ivrLoadService.get(args);
        Assert.assertEquals(CODE, responseService.getCode());
    }

    @Test
    public void testUseCaseRuntimeFunction(){
        Object[] args = new Object[]{ mockFile, "1" };
        ivrLoadService.runtimeFunction(args);
        Mockito.verify(ivrRepository , Mockito.times(1)).save(ArgumentMatchers.any());
        Mockito.verify(ivrDetailRepository , Mockito.times(1)).saveAll(ArgumentMatchers.any());
    }

}
