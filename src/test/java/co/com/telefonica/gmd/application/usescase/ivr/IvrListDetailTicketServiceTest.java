package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;

@SpringBootTest
public class IvrListDetailTicketServiceTest {

    @Mock
    private IIvrDetailRepository ivrDetailRepository;

    private Response response;
    private IvrListDetailTicketService ivrListDetailTicketService;

    public static final Integer CODE = 200;
    public static final String SMS = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Pageable page = PageRequest.of(0, 25);
        BigInteger ticket = new BigInteger("1");

        IvrDetailEntity ivrDetailEntity = new IvrDetailEntity();
        ArrayList<IvrDetailEntity> list  = new ArrayList<>();
        list.add(ivrDetailEntity);
        Page<Iterable<IvrDetailEntity>> result = new PageImpl(list);

        Mockito.when(ivrDetailRepository.listDetailFile(ticket,page)).thenReturn(result);

        ivrListDetailTicketService = new IvrListDetailTicketService();

        ReflectionTestUtils.setField(ivrListDetailTicketService,"ivrDetailRepository", ivrDetailRepository);
        ReflectionTestUtils.setField(ivrListDetailTicketService, "response", response);
    }

    @Test
    public void testIvrListDetailTicketService(){
        Object[] args = new Object[]{ "1" , "0" , "25"};
        ivrListDetailTicketService.get(args);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( SMS , response.getMessage());
    }
}
