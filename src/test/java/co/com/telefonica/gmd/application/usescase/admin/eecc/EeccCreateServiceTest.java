package co.com.telefonica.gmd.application.usescase.admin.eecc;

import co.com.telefonica.gmd.application.repositories.database.admin.eecc.IEeccRepository;
import co.com.telefonica.gmd.domain.admin.eecc.EeccDTO;
import co.com.telefonica.gmd.domain.admin.eecc.EeccEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class EeccCreateServiceTest {

    @Mock
    private IEeccRepository eeccRepository;

    private Response response;
    private EeccDTO eeccDTO;
    private EeccEntity eeccEntity;

    private EeccCreateService eeccCreateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();

        eeccDTO = new EeccDTO();
        eeccDTO.setProveedorContratista("proveedor");

        eeccEntity = new EeccEntity();
        eeccEntity.setProveedorContratista("proveedor");

        Mockito.when( eeccRepository.save(eeccEntity) ).thenReturn(new EeccEntity());

        eeccCreateService = new EeccCreateService();
        ReflectionTestUtils.setField( eeccCreateService , "eeccRepository", eeccRepository);
        ReflectionTestUtils.setField( eeccCreateService , "response" , response);

    }

    @Test
    public void getEeccServiceCreate(){
        eeccCreateService.get(eeccDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
