package co.com.telefonica.gmd.application.usescase.search;

import co.com.telefonica.gmd.application.repositories.database.search.ISearchRepository;
import co.com.telefonica.gmd.domain.search.ISearchQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;
import java.sql.Date;
import java.util.ArrayList;

@SpringBootTest
public class SearchServiceTest {

    @Mock
    private ISearchRepository searchRepository;

    private Response response;
    private SearchService searchService;

    public static final Integer CODE = 200;
    public static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Pageable page = PageRequest.of(0, 25);
        String abonado = new String("1");

        ArrayList<ISearchQuery> list = new ArrayList<>();
        list.add(new ISearchQuery() {
            @Override
            public Integer getIdCargaIVR() { return 34567; }

            @Override
            public String getNombreArchivo() { return "Nombre"; }

            @Override
            public Date getFechaCarga() { return Date.valueOf("2010-10-23"); }

            @Override
            public Integer getAfectados() { return 4523; }

            @Override
            public String getUsuario() { return "Usuario"; }

            @Override
            public Boolean getEstado() { return Boolean.TRUE; }
        });
        Page<Iterable<ISearchQuery>> result = new PageImpl(list);

        Mockito.when(searchRepository.getSearchAbonado(abonado,page)).thenReturn(result);

        searchService = new SearchService();

        ReflectionTestUtils.setField(searchService, "searchRepository", searchRepository);
        ReflectionTestUtils.setField(searchService, "response", response);
    }

    @Test
    public void testSearchService(){
        Object[] data = new Object[]{"1", "0", "25"};
        searchService.get(data);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
