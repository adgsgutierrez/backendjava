package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.application.repositories.database.gravedad.IGravedadRepository;
import co.com.telefonica.gmd.domain.gravedad.GravedadDTO;
import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

@SpringBootTest
public class GravedadUpdateServiceTest {

    @Mock
    private IGravedadRepository gravedadRepository;

    private Response response;
    private GravedadUpdateService gravedadUpdateService;
    private GravedadDTO gravedadDTO;
    private Object obj;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        gravedadDTO = new GravedadDTO();
        gravedadDTO.setIdGravedad(1);
        gravedadDTO.setDesde(1);
        gravedadDTO.setHasta(50);

        GravedadEntity gravedadEntity = new GravedadEntity();
        Optional<GravedadEntity> gravedad = Optional.of(gravedadEntity);

        Mockito.when(gravedadRepository.findById(gravedadDTO.getIdGravedad())).thenReturn(gravedad);

        gravedadUpdateService = new GravedadUpdateService();

        ReflectionTestUtils.setField(gravedadUpdateService, "gravedadRepository", gravedadRepository);
        ReflectionTestUtils.setField(gravedadUpdateService, "response", response);
    }

    @Test
    public void testGravedadUpdateService(){
        gravedadUpdateService.get(gravedadDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
