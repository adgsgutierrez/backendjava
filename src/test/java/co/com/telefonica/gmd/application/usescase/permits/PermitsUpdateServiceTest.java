package co.com.telefonica.gmd.application.usescase.permits;

import co.com.telefonica.gmd.application.repositories.database.permits.IPermitsRepository;
import co.com.telefonica.gmd.domain.permits.PermitsDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class PermitsUpdateServiceTest {

    @Mock
    private IPermitsRepository permitsRepository;

    private Response response;
    private PermitsUpdateService permitsUpdateService;
    private PermitsDTO permitsDTO;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Mockito.when(permitsRepository.getUpdatePermits(true,1,1)).thenReturn(1);

        permitsUpdateService = new PermitsUpdateService();

        ReflectionTestUtils.setField(permitsUpdateService, "permitsRepository", permitsRepository);
        ReflectionTestUtils.setField(permitsUpdateService, "response", response);

    }

    @Test
    public void testPermitsUpdateService(){
        permitsDTO = new PermitsDTO();
        permitsDTO.setIdPermisos(1);
        permitsDTO.setIdPerfiles(1);
        permitsDTO.setEstado(true);
        Object[] data = new Object[]{permitsDTO};
        permitsUpdateService.get(data);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
