package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.domain.ivr.SearchIdCargaIVR;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class IvrCloseFileOpenedServiceTest {

    @Mock
    private IIvrRepository repository;
    private Response response;
    private IIvrService service;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        SearchIdCargaIVR data = new SearchIdCargaIVR() {
            @Override
            public String getIdCargaIVR() {
                return "Data Exist";
            }
        };
        Mockito.when( repository.updateViabilityTicket(1, "false")).thenReturn(java.util.Optional.of(data));
        response = new Response();
        service = new IvrCloseFileOpenedService();

        ReflectionTestUtils.setField( service , "repository" , repository);
        ReflectionTestUtils.setField( service , "response" , response);

    }

    @Test
    public void itSuccess(){
        Response response = service.get( new Object[]{1});
        Integer code = 200;
        Assert.assertEquals(response.getCode(), code);
    }

}
