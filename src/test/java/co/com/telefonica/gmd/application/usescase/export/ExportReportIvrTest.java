package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class ExportReportIvrTest {

    private static MockedStatic<FunctionsTransform> functTrans;
    private String queryIvrString = "Query string";
    private Map<String, Object> paramsFilter = new HashMap<>();

    @Mock
    private Query query;
    @Mock
    private EntityManager entityManager;
    @Mock
    private ObjectToExcel export;

    private IExportOtherFile service;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        functTrans = Mockito.mockStatic(FunctionsTransform.class);
        service = new ExportReportIvr();
        List<Object[]> results = new ArrayList<>();
        results.add( new Object[]{1,2,3,4});
        ArrayList<ArrayList<Object>> dataExport = new ArrayList<>();
        Mockito.when( query.getResultList() ).thenReturn(results);
        Mockito.when( export.getWorkbook() ).thenReturn( new XSSFWorkbook());
        functTrans.when(
                () -> FunctionsTransform.getQueryBuilderString(
                        queryIvrString,
                        entityManager,
                        paramsFilter
                )
        ).thenReturn(query);
        functTrans.when(
                () -> FunctionsTransform.getDataResult(
                        new Integer[]{0,1,2,3},
                        results
                )
        ).thenReturn(dataExport);

        ReflectionTestUtils.setField(service , "export" , export);
        ReflectionTestUtils.setField(service , "entityManager" , entityManager);
        ReflectionTestUtils.setField(service , "queryIvr" , queryIvrString);
    }

    @Test
    public void itSucess(){
        Assert.assertNotNull( service.getWorkbook(paramsFilter) );
    }

    @After
    public void endTest(){
        functTrans.close();
    }
}
