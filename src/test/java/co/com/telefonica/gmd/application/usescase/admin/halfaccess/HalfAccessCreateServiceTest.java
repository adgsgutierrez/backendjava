package co.com.telefonica.gmd.application.usescase.admin.halfaccess;

import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessDTO;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class HalfAccessCreateServiceTest {
    @Mock
    private IHalfAccessRepository halfAccessRepository;

    private Response response;
    private HalfAccessDTO halfAccessDTO;
    private HalfAccessEntity halfAccessEntity;

    private HalfAccessCreateService halfAccessCreateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();

        halfAccessDTO = new HalfAccessDTO();
        halfAccessDTO.setMediosacceso("medioacceso");

        halfAccessEntity = new HalfAccessEntity();
        halfAccessEntity.setMediosacceso("medioacceso");

        Mockito.when( halfAccessRepository.save(halfAccessEntity) ).thenReturn(new HalfAccessEntity());

        halfAccessCreateService = new HalfAccessCreateService();
        ReflectionTestUtils.setField( halfAccessCreateService, "halfAccessRepository" , halfAccessRepository);
        ReflectionTestUtils.setField( halfAccessCreateService, "response" , response);
    }

    @Test
    public void getHalfAccessCreateService(){
        Response response = halfAccessCreateService.get(halfAccessDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
