package co.com.telefonica.gmd.application.usescase.dashboard.detailtypeplant;

import co.com.telefonica.gmd.application.repositories.database.dashboard.detailtypeplant.IDetailTypePlantExternalRepository;
import co.com.telefonica.gmd.application.repositories.database.dashboard.detailtypeplant.IDetailTypePlantInternalRepository;
import co.com.telefonica.gmd.domain.externalplant.IExternalPlantDetailQuery;
import co.com.telefonica.gmd.domain.internalplant.IInternalPlantaDetailQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.util.ArrayList;

@SpringBootTest
public class DetailTypePlantServiceTest {

    @Mock
    private IDetailTypePlantInternalRepository detailTypePlantInternalRepository;

    @Mock
    private IDetailTypePlantExternalRepository detailTypePlantExternalRepository;

    private Response response;
    private DetailTypePlantService detailTypePlantService;
    private IExternalPlantDetailQuery externalPlantDetailQuery;
    private IInternalPlantaDetailQuery internalPlantaDetailQuery;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";
    private Object List;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        ArrayList list = new ArrayList();
        externalPlantDetailQuery = new IExternalPlantDetailQuery() {
            @Override
            public Integer getIdPE() {
                return null;
            }

            @Override
            public String getDepartamento() {
                return null;
            }

            @Override
            public String getLocalidad() {
                return null;
            }

            @Override
            public String getDistribuidor() {
                return null;
            }

            @Override
            public String getArmarioCable() {
                return null;
            }

            @Override
            public String getCajasAfectadas() {
                return null;
            }

            @Override
            public String getProveedorContratista() {
                return null;
            }

            @Override
            public String getMediosAcceso() {
                return null;
            }

            @Override
            public String getTipologia() {
                return null;
            }

            @Override
            public String getTipoRed() {
                return null;
            }

            @Override
            public Timestamp getFechaHoraRegistro() {
                return null;
            }

            @Override
            public String getTecnicoReporta() {
                return null;
            }

            @Override
            public String getCelularTecnico() {
                return null;
            }

            @Override
            public String getDesFalla() {
                return null;
            }

            @Override
            public String getClientesAfectados() {
                return null;
            }

            @Override
            public Integer getViabilidadTicket() {
                return null;
            }

            @Override
            public Boolean getEstado() {
                return null;
            }

            @Override
            public String getBarrio() {
                return null;
            }

            @Override
            public String getDireccion() {
                return null;
            }
        };
        internalPlantaDetailQuery = new IInternalPlantaDetailQuery() {
            @Override
            public Integer getIdPI() {
                return null;
            }

            @Override
            public String getDepartamento() {
                return null;
            }

            @Override
            public String getLocalidad() {
                return null;
            }

            @Override
            public String getDistribuidor() {
                return null;
            }

            @Override
            public String getProveedorContratista() {
                return null;
            }

            @Override
            public String getMediosAcceso() {
                return null;
            }

            @Override
            public Timestamp getFechaHoraRegistro() {
                return null;
            }

            @Override
            public String getTecnicoReporta() {
                return null;
            }

            @Override
            public String getCelularTecnico() {
                return null;
            }

            @Override
            public String getDesFalla() {
                return null;
            }

            @Override
            public String getClientesAfectados() {
                return null;
            }

            @Override
            public Integer getViabilidadTicket() {
                return null;
            }

            @Override
            public Boolean getEstado() {
                return null;
            }

            @Override
            public String getRangoElementoAfectados() {
                return null;
            }
        };

        Mockito.when(detailTypePlantExternalRepository.getDetailExternalPlant(1)).thenReturn(list);
        Mockito.when(detailTypePlantInternalRepository.getDetailInternalPlant(1)).thenReturn(list);

        detailTypePlantService = new DetailTypePlantService();

        ReflectionTestUtils.setField(detailTypePlantService, "detailTypePlantExternalRepository",detailTypePlantExternalRepository);
        ReflectionTestUtils.setField(detailTypePlantService,"detailTypePlantInternalRepository", detailTypePlantInternalRepository);
        ReflectionTestUtils.setField(detailTypePlantService,"response", response);

    }

    @Test
    public void testDetailTypePlantServicePI(){
        detailTypePlantService.get(1,"PI");
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testDetailTypePlantServicePE(){
        detailTypePlantService.get(1,"PE");
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
