package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserRepository;
import co.com.telefonica.gmd.domain.user.UserCreateDTO;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class UserCreateServicesTest {

    @Mock
    private IUserRepository userRepository;
    private UserCreateDTO userCreate;
    private UserCreateService userCreateService;

    private Response response;
    public static final Integer CODE = 200;
    public static final String SMS = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        // Mock user create
        userCreate = new UserCreateDTO();
        userCreate.setPerfil( new Integer("1"));
        userCreate.setUsuarioRed("jhondoe");
        userCreate.setNombres("jhon doe");
        userCreate.setEstado( Boolean.TRUE );
        userCreate.setEmail( "jhondoe@test.com" );

        UsuariosEntity userEntity = new UsuariosEntity();
        userEntity.setUsuarioRed("jhondoe");
        userEntity.setEmail("jhondoe@test.com");
        userEntity.setEstado(Boolean.TRUE);
        userEntity.setNombres("jhon doe");
        userEntity.setFkIdPerfiles(new Integer("1"));

        response = new Response();

        Mockito.when( userRepository.save(userEntity) ).thenReturn(userEntity);

        userCreateService = new UserCreateService();
        ReflectionTestUtils.setField( userCreateService , "userRepository" , userRepository );
        ReflectionTestUtils.setField( userCreateService , "response" , response );
    }

    @Test
    public void useCaseCreate(){
        Response response = userCreateService.get(userCreate);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( SMS , response.getMessage());
    }
}
