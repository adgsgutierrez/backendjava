package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;

@SpringBootTest
public class IvrListDetailFileServiceTest {

    @Mock
    private IIvrDetailRepository ivrDetailRepository;

    private Response response;

    private IvrListDetailFileService ivrListDetailFileService;

    public static final Integer CODE = 200;
    public static final String SMS = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        ivrListDetailFileService = new IvrListDetailFileService();
        response = new Response();

        Pageable page = PageRequest.of(0, 25);
        BigInteger indexFile = new BigInteger("1");
        IvrDetailEntity ivrDetailEntity = new IvrDetailEntity();
        ArrayList<IvrDetailEntity> list  = new ArrayList<>();
        list.add(ivrDetailEntity);
        Page<Iterable<IvrDetailEntity>> result = new PageImpl(list);
        Mockito.when( ivrDetailRepository.listDetailFile(indexFile , page) ).thenReturn(result);

        ReflectionTestUtils.setField(ivrListDetailFileService , "ivrDetailRepository" , ivrDetailRepository);
        ReflectionTestUtils.setField(ivrListDetailFileService , "response" , response);
    }

    @Test
    public void testUseCaseListDetail(){
        Object[] args = new Object[]{ "1" , "0" , "25"};
        Response response = ivrListDetailFileService.get(args);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( SMS , response.getMessage());
    }
}
