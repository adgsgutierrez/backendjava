package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserRepository;
import co.com.telefonica.gmd.domain.user.UserCreateDTO;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import co.com.telefonica.gmd.platform.beans.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

@SpringBootTest
public class UserUpdateServiceTest {
    @Mock
    private IUserRepository userRepository;

    private Response response;
    private UserCreateDTO userCreateDTO;

    private UserUpdateService userUpdateService;

    public static final Integer CODE = 200;
    public static final String SMS = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(SMS);

        userCreateDTO = new UserCreateDTO();
        userCreateDTO.setIdUsuario(1);
        userCreateDTO.setPerfil(1);
        userCreateDTO.setNombres("Nombre");
        userCreateDTO.setEstado(false);
        userCreateDTO.setEmail("email");
        userCreateDTO.setUsuarioRed("usuario");

        UsuariosEntity usuarios = new UsuariosEntity();

        Optional<UsuariosEntity> usuariosEntity = Optional.of(usuarios);

        Mockito.when(userRepository.findById(userCreateDTO.getIdUsuario()) ).thenReturn(usuariosEntity);

        userUpdateService = new UserUpdateService();
        ReflectionTestUtils.setField(userUpdateService, "userRepository" , userRepository);
        ReflectionTestUtils.setField(userUpdateService, "response", response);

    }

    @Test
    public void updateUser(){
        userUpdateService.get(userCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(SMS, response.getMessage());
    }
}
