package co.com.telefonica.gmd.application.usescase.detectionthresholds;


import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IElementsRepository;
import co.com.telefonica.gmd.domain.detectionthresholds.ElementsEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class ElementServiceTest {

    @Mock
    private IElementsRepository elementsRepository;

    private Response response;
    private ElementsService elementsService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        ArrayList<ElementsEntity> list = new ArrayList<>();

        Mockito.when(elementsRepository.getElements()).thenReturn(list);

        elementsService = new ElementsService();

        ReflectionTestUtils.setField(elementsService, "elementsRepository", elementsRepository);
        ReflectionTestUtils.setField(elementsService, "response", response);

    }

    @Test
    public void testGetElementsService(){
        Object data = new Object();
        elementsService.get(data);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
