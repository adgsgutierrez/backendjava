package co.com.telefonica.gmd.application.repositories.service.login;

import co.com.telefonica.gmd.domain.user.*;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "ldap.authorization=/api/v1/authorization",
        "ldap.login=/api/v1/authorization/ldap",
        "ldap.userAuthorization=us3rL0g1n4uth",
        "ldap.keyAuthorization=p4ssw0rdL0g1n4uth",
        "ldap.group=ToolBox",
        "ldap.host=https://apilab.telefonica.co:20291"
})
@Slf4j
public class LoginLdapServiceTest {

    @Mock
    private RestTemplate restTemplate;
    @Value("${ldap.userAuthorization}")
    private String usLoginLdap;
    @Value("${ldap.keyAuthorization}")
    private String pwLoginLdap;
    @Value("${ldap.authorization}")
    private String pathAuthorizationLdap;
    @Value("${ldap.login}")
    private String pathLoginLdap;
    @Value("${ldap.host}")
    private String hostServiceLdap;
    @Value("${ldap.group}")
    private String groupLdap;

    private HttpHeaders headerAuth;
    private HttpHeaders headerLogin;
    private String urlAuthorization;
    private String urlLogin;

    private UserDTO user;
    private LoginLdapService loginLdapService = new LoginLdapService();;

    private UserLdapAuthorization usLdapAuth;
    private UserLdapLogin userLdap;
    private ResponseEntity<UserLdapAuthorizationResponse> reqEntityAuthorization;
    private ResponseEntity<UserLdapLoginResponse> reqEntityLogin;

    private UserLdapAuthorizationResponse userLdapResponse;
    private UserLdapLoginResponse userLoginResponse;

    private LdapObject ldapObject;

    private static MockedStatic<FunctionsTransform> utilities;

    @Before
    public void setup(){

        MockitoAnnotations.initMocks(this);
        utilities = Mockito.mockStatic(FunctionsTransform.class);
        user = new UserDTO();
        user.setUser("UserName");
        user.setPassword("Password");
        urlAuthorization = hostServiceLdap.concat(pathAuthorizationLdap);
        urlLogin = hostServiceLdap.concat(pathLoginLdap);
        String userEncript = "userEncript";
        String passwordEncript = "passwordEncript";
        String hostEncript = "hostEncript";
        utilities.when( () -> FunctionsTransform.getStringNowWithFormat("yyyyMMdd") ).thenReturn( "20210526");
        // Usuario
        utilities.when( () -> FunctionsTransform.generateKeysDynamic(usLoginLdap, "$","20210526" )).thenReturn( userEncript );
        utilities.when( () -> FunctionsTransform.encodeStringToBase64WithIteration(userEncript)).thenReturn( userEncript );
        //Password
        utilities.when( () -> FunctionsTransform.generateKeysDynamic(pwLoginLdap, "$","20210526" )).thenReturn( passwordEncript );
        utilities.when( () -> FunctionsTransform.encodeStringToBase64WithIteration(passwordEncript)).thenReturn( passwordEncript );
        //host
        utilities.when( () -> FunctionsTransform.generateKeysDynamic(pathLoginLdap, "","" )).thenReturn( hostEncript );
        utilities.when( () -> FunctionsTransform.encodeStringToBase64WithIteration(hostEncript)).thenReturn( hostEncript );

        userLdapResponse = new UserLdapAuthorizationResponse();
        userLdapResponse.setCode(200);
        userLdapResponse.setMessage("Ok");
        userLdapResponse.setData("token");

        reqEntityAuthorization = new ResponseEntity<UserLdapAuthorizationResponse>(
                userLdapResponse,
                headerAuth,
                HttpStatus.OK);

        ldapObject = new LdapObject();
        ldapObject.setGroups(new String[]{"group 1", "group2" , groupLdap});

        userLoginResponse = new UserLdapLoginResponse();
        userLoginResponse.setCode(200);
        userLoginResponse.setMessage("Message");
        userLoginResponse.setData(ldapObject);

        reqEntityLogin = new ResponseEntity<UserLdapLoginResponse>(
                userLoginResponse,
                headerAuth,
                HttpStatus.OK
        );

        Mockito.when(
                restTemplate.exchange(
                        ArgumentMatchers.eq(urlAuthorization),
                        ArgumentMatchers.any(HttpMethod.class),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.<Class<UserLdapAuthorizationResponse>>any()))
                .thenReturn(reqEntityAuthorization);

        Mockito.when(
                restTemplate.exchange(
                        ArgumentMatchers.eq(urlLogin),
                        ArgumentMatchers.any(HttpMethod.class),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.<Class<UserLdapLoginResponse>>any()))
                .thenReturn(reqEntityLogin);

        ReflectionTestUtils.setField(loginLdapService , "usLoginLdap" , usLoginLdap);
        ReflectionTestUtils.setField(loginLdapService , "pwLoginLdap" , pwLoginLdap);
        ReflectionTestUtils.setField(loginLdapService , "pathAuthorizationLdap" , pathAuthorizationLdap);
        ReflectionTestUtils.setField(loginLdapService , "pathLoginLdap" , pathLoginLdap);
        ReflectionTestUtils.setField(loginLdapService , "hostServiceLdap" , hostServiceLdap);
        ReflectionTestUtils.setField(loginLdapService , "groupLdap" , groupLdap);
        ReflectionTestUtils.setField(loginLdapService , "restTemplate" , restTemplate);
    }

    @Test
    public void validateLoginSuccess() {
        UserLdapLoginResponse responseService = loginLdapService.login(user);
        Assert.assertEquals( responseService.getData().getGroups() , ldapObject.getGroups() );
    }

    @Test
    public void validateNotExistGroup(){
        ldapObject.setGroups(new String[]{"group 1", "group2"});
        userLoginResponse.setCode(200);
        userLoginResponse.setMessage("Message");
        userLoginResponse.setData(ldapObject);
        reqEntityLogin = new ResponseEntity<UserLdapLoginResponse>(
                userLoginResponse,
                headerAuth,
                HttpStatus.OK
        );

        Mockito.when(
                restTemplate.exchange(
                        ArgumentMatchers.eq(urlLogin),
                        ArgumentMatchers.any(HttpMethod.class),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.<Class<UserLdapLoginResponse>>any()))
                .thenReturn(reqEntityLogin);

        ReflectionTestUtils.setField(loginLdapService , "restTemplate" , restTemplate);

        UserLdapLoginResponse responseService = loginLdapService.login(user);
        Assert.assertNotNull(responseService );
    }

    @Test
    public void validateNotSuccessData(){
        ldapObject.setGroups(new String[]{"group 1", "group2"});
        userLoginResponse.setCode(200);
        userLoginResponse.setMessage("Message");
        userLoginResponse.setData(ldapObject);
        reqEntityLogin = new ResponseEntity<UserLdapLoginResponse>(
                userLoginResponse,
                headerAuth,
                HttpStatus.NOT_FOUND
        );

        Mockito.when(
                restTemplate.exchange(
                        ArgumentMatchers.eq(urlLogin),
                        ArgumentMatchers.any(HttpMethod.class),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.<Class<UserLdapLoginResponse>>any()))
                .thenReturn(reqEntityLogin);

        ReflectionTestUtils.setField(loginLdapService , "restTemplate" , restTemplate);

        UserLdapLoginResponse responseService = loginLdapService.login(user);
        Assert.assertNull(responseService );
    }

    @Test
    public void validateNotSuccessGroup(){
        ldapObject.setGroups(new String[]{"group 1", "group2"});
        userLoginResponse.setCode(300);
        userLoginResponse.setMessage("Message");
        userLoginResponse.setData(ldapObject);
        reqEntityLogin = new ResponseEntity<UserLdapLoginResponse>(
                userLoginResponse,
                headerAuth,
                HttpStatus.OK
        );

        Mockito.when(
                restTemplate.exchange(
                        ArgumentMatchers.eq(urlLogin),
                        ArgumentMatchers.any(HttpMethod.class),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.<Class<UserLdapLoginResponse>>any()))
                .thenReturn(reqEntityLogin);

        ReflectionTestUtils.setField(loginLdapService , "restTemplate" , restTemplate);

        UserLdapLoginResponse responseService = loginLdapService.login(user);
        Assert.assertNull(responseService );
    }

    @Test
    public void generateErnoClient(){
        ldapObject.setGroups(new String[]{"group 1", "group2"});

        Mockito.when(
                restTemplate.exchange(
                        ArgumentMatchers.eq(urlLogin),
                        ArgumentMatchers.any(HttpMethod.class),
                        ArgumentMatchers.any(),
                        ArgumentMatchers.<Class<UserLdapLoginResponse>>any()))
                .thenThrow(HttpClientErrorException.class);

        ReflectionTestUtils.setField(loginLdapService , "restTemplate" , restTemplate);

        UserLdapLoginResponse responseService = loginLdapService.login(user);
        Assert.assertNull(responseService );
    }

    @After
    public void endTest(){
        utilities.close();
    }
}
