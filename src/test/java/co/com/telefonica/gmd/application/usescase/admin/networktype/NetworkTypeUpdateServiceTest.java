package co.com.telefonica.gmd.application.usescase.admin.networktype;

import co.com.telefonica.gmd.application.repositories.database.admin.networktype.INetworkTypeRepository;
import co.com.telefonica.gmd.domain.admin.networktype.NetworktTypeDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;

@SpringBootTest
public class NetworkTypeUpdateServiceTest {

    @Mock
    private INetworkTypeRepository networkTypeRepository;

    private Response response;
    private NetworktTypeDTO networktTypeDTO;

    private NetworkTypeUpdateService networkTypeUpdateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        response = new Response();

        networktTypeDTO = new NetworktTypeDTO();
        networktTypeDTO.setIdtipored(BigInteger.ONE);
        networktTypeDTO.setEstado(true);

        networkTypeUpdateService = new NetworkTypeUpdateService();

        ReflectionTestUtils.setField( networkTypeUpdateService, "networkTypeRepository" , networkTypeRepository);
        ReflectionTestUtils.setField( networkTypeUpdateService, "response" , response);
    }

    @Test
    public void getNetworkTypeUpdateService(){
        networkTypeUpdateService.get(networktTypeDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
