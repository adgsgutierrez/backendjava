package co.com.telefonica.gmd.application.usescase.dashboard.department;

import co.com.telefonica.gmd.application.repositories.database.dashboard.department.IDepartmentRepository;
import co.com.telefonica.gmd.domain.dashboard.department.DepartmentEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;

@SpringBootTest
public class DepartmentListServiceTest {

    @Mock
    private IDepartmentRepository departmentRepository;

    private Response response;
    private DepartmentListService departmentListService;
    private HashMap<String,String> hashMap;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        hashMap = new HashMap<>();
        hashMap.put("page" , "0");
        hashMap.put("items" , "25");
        response = new Response();
        ArrayList list = new ArrayList();
        DepartmentEntity departmentEntity = new DepartmentEntity();

        Mockito.when(departmentRepository.getListDepartment(hashMap)).thenReturn(list);

        departmentListService = new DepartmentListService();

        ReflectionTestUtils.setField(departmentListService, "departmentRepository", departmentRepository);
        ReflectionTestUtils.setField(departmentListService, "response", response);
    }

    @Test
    public void testDepartmentListService(){
        departmentListService.get(hashMap);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
