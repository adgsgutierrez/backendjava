package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserDetailRepository;
import co.com.telefonica.gmd.application.repositories.database.user.IUserRepository;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class UserFilterServiceTest {

    @Mock
    private IUserDetailRepository userDetailRepository;
    @Mock
    private IUserRepository userRepository;
    private Response response;
    private Object[] filter;
    private UserFilterService userFilterService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();
        Pageable page = PageRequest.of(0,25);
        Mockito.when(
                userRepository.getlist(page)
        ).thenReturn(Page.empty());
        Mockito.when(
                userDetailRepository.getFilterProfileUser(1 , page)
        ).thenReturn(Page.empty());

        userFilterService = new UserFilterService();
        ReflectionTestUtils.setField(userFilterService , "userDetailRepository" , userDetailRepository );
        ReflectionTestUtils.setField(userFilterService , "userRepository" , userRepository );
        ReflectionTestUtils.setField(userFilterService , "response" , response );
    }

    @Test
    public void itIdUserAllQuery(){
        filter = new Object[]{0, "0" , "25"};
        Assert.assertNotNull( userFilterService.get(filter));
    }

    @Test
    public void itIdUserFilterProfile(){
        filter = new Object[]{1, "0" , "25"};
        Assert.assertNotNull( userFilterService.get(filter));
    }
}
