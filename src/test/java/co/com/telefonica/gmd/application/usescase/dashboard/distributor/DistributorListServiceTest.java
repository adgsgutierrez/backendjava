package co.com.telefonica.gmd.application.usescase.dashboard.distributor;

import co.com.telefonica.gmd.application.repositories.database.dashboard.distributor.IDistributorRepository;
import co.com.telefonica.gmd.domain.dashboard.distributor.DistributorListQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class DistributorListServiceTest {

    @Mock
    private IDistributorRepository distributorRepository;

    private Response response;
    private DistributorListService distributorListService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        Integer id = new Integer(1);
        response = new Response();
        ArrayList list = new ArrayList();
        DistributorListQuery distributorListQuery = new DistributorListQuery() {
            @Override
            public Integer getIdDistribuidor() {
                return null;
            }

            @Override
            public String getDistribuidor() {
                return null;
            }
        };

        Mockito.when(distributorRepository.getDistributorByLocation(1)).thenReturn(list);

        distributorListService = new DistributorListService();

        ReflectionTestUtils.setField(distributorListService, "distributorRepository", distributorRepository);
        ReflectionTestUtils.setField(distributorListService, "response", response);
    }

    @Test
    public void testDistributorListService(){
        distributorListService.get(1);
        Assert.assertEquals(CODE,response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
