package co.com.telefonica.gmd.application.usescase.dashboard.boxesaffected;

import co.com.telefonica.gmd.domain.dashboard.boxesaffected.BoxesAffectedEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.*;
import java.util.*;

@SpringBootTest
public class BoxesAfectedListTest {

    @Mock
    private EntityManager entityManager;

    @Mock
    private Query queryMock;

    private String query = "Esta es una Query";
    private Map<String , Object> params;
    private List<Object> resultsResponse = new ArrayList<>();
    private Response response;
    private IBoxesAffected service;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        params = new HashMap<>();
        params.put("value" , "key");
        params.put("data" , 12345);
        service = new BoxesAfectedList();
        response = new Response();
        Mockito.when(queryMock.getResultList()).thenReturn(resultsResponse);

        Mockito.when(entityManager.createNativeQuery(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.<Class<BoxesAffectedEntity>>any()
        )).thenReturn(queryMock);

        ReflectionTestUtils.setField(service , "entityManager" , entityManager);
        ReflectionTestUtils.setField(service , "queryBoxes" , query);
        ReflectionTestUtils.setField(service , "response" , response);
    }

    @Test
    public void itSuccessGetBoxes(){
        this.service.getBoxesAffected(params);
    }
}
