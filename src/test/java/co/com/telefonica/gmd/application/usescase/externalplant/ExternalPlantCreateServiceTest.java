package co.com.telefonica.gmd.application.usescase.externalplant;

import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantCreateDTO;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
public class ExternalPlantCreateServiceTest {

    @Mock
    private IExternalPlantRepository externalPlantRepository;

    private Response response;


    private ExternalPlantCreateDTO externalPlantCreateDTO;
    private ExternalPlantEntity externalPlantEntity;
    private ExternalPlantCreateService externalPlantCreateService;
    private SimpleDateFormat format;
    private Timestamp timestamp;
    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";
    private static final Integer CODE_ERROR = 300;
    private static final String MESSAGE_ERROR = "Error";

    @Before
    public void setup() throws ParseException {
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        format =  new SimpleDateFormat("YYYY-MM-DD hh:mm:ss");

        externalPlantCreateDTO =  new ExternalPlantCreateDTO();
        externalPlantCreateDTO.setIdUsuario(1);
        externalPlantCreateDTO.setIdTipologia(1);
        externalPlantCreateDTO.setIdMa(1);
        externalPlantCreateDTO.setIdEecc(1);
        externalPlantCreateDTO.setIdTipoRed(1);
        externalPlantCreateDTO.setFechaHoraRegistro("2020-01-01 07:07:07");
        externalPlantCreateDTO.setFechaInicialRegistro( new Date() );
       // externalPlantCreateDTO.setFechaInicialDano("2020-01-01 07:07:07");
       // externalPlantCreateDTO.setFechaRequerida("2020-01-01 07:07:07");
        externalPlantCreateDTO.setTecnicoReporta("Jhon doe");
        externalPlantCreateDTO.setCelularTecnico("0000000000");
        externalPlantCreateDTO.setDesFalla("prueba");
        externalPlantCreateDTO.setClientesAfectados(1234);
        externalPlantCreateDTO.setViabilidadTicket(1);

        externalPlantEntity =  new ExternalPlantEntity();
        externalPlantEntity.setFkIdUsuario(externalPlantCreateDTO.getIdUsuario());
        externalPlantEntity.setFkIdTipologia(externalPlantCreateDTO.getIdTipologia());
        externalPlantEntity.setFkIdMa(externalPlantCreateDTO.getIdMa());
        externalPlantEntity.setFechaHoraInicial( new Timestamp( externalPlantCreateDTO.getFechaInicialRegistro().getTime()));
        externalPlantEntity.setFkIdEecc(externalPlantCreateDTO.getIdEecc());
        externalPlantEntity.setFkIdTipoRed(externalPlantCreateDTO.getIdTipoRed());
        externalPlantEntity.setFechaHoraRegistro(
                new Timestamp(format.parse(externalPlantCreateDTO.getFechaHoraRegistro()).getTime()));
       // externalPlantEntity.setFechaInicialDano(
          //      new Timestamp(format.parse(externalPlantCreateDTO.getFechaInicialDano()).getTime()));
       // externalPlantEntity.setFechaRequerida(
         //       new Timestamp(format.parse(externalPlantCreateDTO.getFechaRequerida()).getTime()));
        externalPlantEntity.setTecnicoReporta(externalPlantCreateDTO.getTecnicoReporta());
        externalPlantEntity.setCelularTecnico(externalPlantCreateDTO.getCelularTecnico());
        externalPlantEntity.setDesFalla(externalPlantCreateDTO.getDesFalla());
        externalPlantEntity.setClientesAfectados(externalPlantCreateDTO.getClientesAfectados());
        externalPlantEntity.setViabilidadTicket(externalPlantCreateDTO.getViabilidadTicket());

        Mockito.when(externalPlantRepository.save(externalPlantEntity)).thenReturn(new ExternalPlantEntity());
        externalPlantCreateService =  new ExternalPlantCreateService();

        ReflectionTestUtils.setField(externalPlantCreateService, "externalPlantRepository", externalPlantRepository);
        ReflectionTestUtils.setField(externalPlantCreateService, "response", response);

    }

    @Test
    public void testUseCaseGet(){
        externalPlantCreateService.get(externalPlantCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

   /* @Test()
    public void testUseCaseGetCatch(){
        externalPlantCreateDTO.setFechaInicialDano("texto de prueba");
        externalPlantCreateService.get(externalPlantCreateDTO);
        Assert.assertEquals(CODE_ERROR, response.getCode());
        Assert.assertEquals(MESSAGE_ERROR, response.getMessage());
    }*/
}
