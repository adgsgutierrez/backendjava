package co.com.telefonica.gmd.application.usescase.permits;

import co.com.telefonica.gmd.application.repositories.database.permits.IPermitsRepository;
import co.com.telefonica.gmd.domain.permits.IPermitsQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;

@SpringBootTest
public class PermitsServiceTest {
    @Mock
    private IPermitsRepository permitsRepository;

    private Response response;
    private PermitsService permitsService;

    public static final Integer CODE = 200;
    public static final String MESSAGE = "Ok";
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();

        Pageable page = PageRequest.of(0, 25);
        Integer indexFile = new Integer(1);
        IPermitsQuery permitsQuery = new IPermitsQuery() {
            @Override
            public Integer getIdPerfil() {
                return null;
            }

            @Override
            public Integer getIdPermiso() {
                return null;
            }

            @Override
            public String getPermisos() {
                return null;
            }

            @Override
            public Boolean getEstado() {
                return null;
            }
        };
        ArrayList<IPermitsQuery> list  = new ArrayList<>();
        list.add(permitsQuery);
        Page<Iterable<IPermitsQuery>> result = new PageImpl(list);
        Mockito.when(permitsRepository.getPermitsId(indexFile,page)).thenReturn(result);

        permitsService = new PermitsService();

        ReflectionTestUtils.setField(permitsService, "permitsRepository", permitsRepository);
        ReflectionTestUtils.setField(permitsService, "response", response);
    }

    @Test
    public void testPermitsService(){
        Object[] args = new Object[]{ "1" , "0" , "25"};
        permitsService.get(args);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( MESSAGE , response.getMessage());
    }
}
