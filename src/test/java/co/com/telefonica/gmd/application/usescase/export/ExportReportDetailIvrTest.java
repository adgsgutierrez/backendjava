package co.com.telefonica.gmd.application.usescase.export;


import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class ExportReportDetailIvrTest {

    @Mock
    private Query query;
    @Mock
    private EntityManager entityManager;
    @Mock
    private ObjectToExcel objectToExcel;

    private static MockedStatic<FunctionsTransform> functTrans;

    private String queryMockByFilter;
    private IExportOtherFile service;
    private Map<String, Object> paramsFilter;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        functTrans = Mockito.mockStatic(FunctionsTransform.class);
        paramsFilter = new HashMap<>();
        paramsFilter.put("ViabilidadTicket" , 12345);
        queryMockByFilter = "Esta es una query";
        List<Object[]> resultsByQuery = new ArrayList<>();
        ArrayList< ArrayList <Object> > dataExport = new ArrayList<>();
        resultsByQuery.add( new Object[]{12354, 31456789 , "2021-09-09" , "16:34" , 1 , 14 , 1, 1 } );


        Mockito.when( entityManager.createNativeQuery(queryMockByFilter)).thenReturn(query);
        Mockito.when( query.getResultList() ).thenReturn(resultsByQuery);
        functTrans.when( () ->  FunctionsTransform.getDataResult( new Integer[]{3,4,5,6,1,7,8,9} , resultsByQuery) )
                .thenReturn(dataExport);
        Mockito.when( objectToExcel.getWorkbook() ).thenReturn( new XSSFWorkbook() );

        service = new ExportReportDetailIvr();

        ReflectionTestUtils.setField(service , "export" , objectToExcel);
        ReflectionTestUtils.setField(service , "entityManager" , entityManager);
        ReflectionTestUtils.setField(service , "queryFilterIvr" , queryMockByFilter);
    }

    @Test
    public void itSuccess(){
        Assert.assertNotNull(service.getWorkbook(paramsFilter) );
    }

    @After
    public void endTest(){
        functTrans.close();
    }
}
