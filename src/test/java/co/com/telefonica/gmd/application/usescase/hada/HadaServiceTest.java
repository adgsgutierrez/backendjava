package co.com.telefonica.gmd.application.usescase.hada;

import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.hada.HadaRepository;
import co.com.telefonica.gmd.application.repositories.database.internalplant.IInternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.hada.IResponseQueryHada;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.platform.beans.ResponseHada;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@SpringBootTest
public class HadaServiceTest {

    @Mock
    private HadaRepository hadaRepository;
    @Mock
    private IIvrDetailRepository ivrDetailRepository;
    @Mock
    private IExternalPlantRepository externalPlantRepository;
    @Mock
    private IInternalPlantRepository internalPlantRepository;

    private IHadaService hadaService = new HadaService();

    private String abonado = "3214567";

    @SneakyThrows
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        IvrEntity ivrEntity = new IvrEntity();
        ivrEntity.setEstado(Boolean.TRUE);
        ivrEntity.setFechaCarga( new Date() );
        IvrDetailEntity ivrDetailEntity = new IvrDetailEntity();
        ivrDetailEntity.setFechaFin( new Date() );
        ivrDetailEntity.setViabilidadTicket(1234);
        ExternalPlantEntity external = new ExternalPlantEntity();
        IResponseQueryHada responseQueryHada = new IResponseQueryHada() {
            @Override
            public String getDescription() {
                return "Get Description";
            }

            @Override
            public String getLocation() {
                return "location";
            }

            @Override
            public String getNetType() {
                return "NetType";
            }

            @Override
            public String getNetName() {
                return "GetNAme";
            }

            @Override
            public String getNetAddress() {
                return "Address";
            }

            @Override
            public Integer getAffectedCustomer() {
                return 34256;
            }

            @Override
            public String getSeverity() {
                return "Severity";
            }

            @Override
            public String getEventId() {
                return "Event Id";
            }

            @Override
            public String getAdditionalInfo() {
                return "Additional Information";
            }

            @Override
            public String getSummary() {
                return "Summary";
            }

            @Override
            public String getAgent() {
                return "Agent";
            }

            @Override
            public String getNetState() {
                return "Estate";
            }

            @Override
            public String getMassiva() {
                return "Massiva";
            }

            @Override
            public String getClas() {
                return "Class";
            }
        };
        long now = System.currentTimeMillis();
        InternalPlantEntity internalPlantEntity = new InternalPlantEntity();
        ivrDetailEntity.setHoraFin(new Time(now));
        ivrDetailEntity.setFlag1(1);
        ivrDetailEntity.setFlag2(1);
        ivrDetailEntity.setFkIdTipologia( new BigInteger("1") );
        ivrDetailEntity.setFkIdCargaIvr( new BigInteger("1234"));

        Mockito.when( hadaRepository.getFileDamage(abonado)).thenReturn(ivrEntity);
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        java.sql.Time timeValue = new java.sql.Time(formatter.parse("18:00:00").getTime());
        Object[] line = new Object[]{
                "6015108948",
                266365,13,
                format.parse("2021-12-23"),
                timeValue,
                1,1,5437351,5522,1};
        Mockito.when( hadaRepository.getDamageDetail(abonado)).thenReturn( new Object[]{line});
        Mockito.when( externalPlantRepository.getByViabilityTicket(1234)).thenReturn(external);
        Mockito.when( internalPlantRepository.getByViabilityTicket(1234)).thenReturn(internalPlantEntity);
        Mockito.when( hadaRepository.getPE(1234)).thenReturn(responseQueryHada);
        Mockito.when( hadaRepository.getPI(1234)).thenReturn(responseQueryHada);
        Mockito.when( hadaRepository.countValuesFilter(abonado)).thenReturn(1234L);

        ReflectionTestUtils.setField(hadaService , "hadaRepository" , hadaRepository);
        ReflectionTestUtils.setField(hadaService , "ivrDetailRepository" , ivrDetailRepository);
        ReflectionTestUtils.setField(hadaService , "externalPlantRepository" , externalPlantRepository);
        ReflectionTestUtils.setField(hadaService , "internalPlantRepository" , internalPlantRepository);
    }

    @Test
    public void itSuccess() {
        ResponseHada responseService = hadaService.get(abonado);
        Assert.assertNotNull(responseService);
    }

}
