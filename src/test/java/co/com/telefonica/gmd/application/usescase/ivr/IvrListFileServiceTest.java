package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.domain.ivr.SearchFileQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

@SpringBootTest
public class IvrListFileServiceTest {

    @Mock
    private IIvrRepository ivrRepository;

    private Response response;

    private IvrListFileService ivrListFileService;

    public static final Integer CODE = 200;
    public static final String SMS = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();
        Pageable page = PageRequest.of(0,25);
        ArrayList<SearchFileQuery> result = new ArrayList<>();
        result.add(new SearchFileQuery() {
            @Override
            public BigInteger getIdCargarIvr() {
                return new BigInteger("123456");
            }

            @Override
            public String getUsuario() {
                return "usuario";
            }

            @Override
            public String getNombreArchivo() {
                return "nombre";
            }

            @Override
            public Date getFechaCarga() {
                return new Date();
            }

            @Override
            public Integer getClientesAfectados() {
                return 12345;
            }

            @Override
            public Boolean getEstado() {
                return Boolean.TRUE;
            }
        });
        Page<Iterable<SearchFileQuery>> listResponse = new PageImpl(result);
        Mockito.when( ivrRepository.findAll(page) ).thenReturn(listResponse);

        ivrListFileService = new IvrListFileService();
        ReflectionTestUtils.setField( ivrListFileService , "response" ,  response);
        ReflectionTestUtils.setField( ivrListFileService , "ivrRepository" ,  ivrRepository);
    }

    @Test
    public void testUseCaseList(){
        Object[] args = new Object[]{ "0" , "25"};
        Response response = ivrListFileService.get(args);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( SMS , response.getMessage());
        Assert.assertNotNull(response.getData());
    }
}
