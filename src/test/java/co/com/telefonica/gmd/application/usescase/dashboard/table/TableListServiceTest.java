package co.com.telefonica.gmd.application.usescase.dashboard.table;

import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;

@SpringBootTest
public class TableListServiceTest {

    @Mock
    private EntityManager entityManager;
    @Mock
    private Query query;
    @Mock
    private Query queryCount;

    private HashMap<String, Object> list;
    private List tableResult;
    private String queryFilterString;
    private String queryCountFilterString;
    private static MockedStatic<FunctionsTransform> functTrans;
    private ITable tableService;
    private Response response;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        functTrans = Mockito.mockStatic(FunctionsTransform.class);
        tableService = new TableListService();
        list = new HashMap<>();
        list.put("page" , 0 );
        list.put("items" , 25 );
        list.put("data" , "value" );
        Map<String , Object> newList = new HashMap<>();
        newList.put("\"V2_DM\".\"V_TablaDashboard\".\"data\"" , "value" );
        queryFilterString = "Query Filter";
        queryCountFilterString = "Query Count Filter";
        response = new Response();
        tableResult = new ArrayList<Object[]>();
        tableResult.add( new Object[]{
                1, new Date(), "Cundinamarca", "Ciudad Bolivar", "Diagnostico 1",
                "Distribuidor 1", "Nombre Elemento 1", "Elemento 1", "Usuario 1",
                12345, "Gravedad", "Tipo Planta", true
        });
        Mockito.when( query.getResultList() ).thenReturn(tableResult);
        ArrayList arrayList = new ArrayList<BigInteger>();
        arrayList.add(new BigInteger("1234") );
        Mockito.when( queryCount.getResultList() ).thenReturn(arrayList);
        functTrans.when(
                () -> FunctionsTransform.getQueryBuilderString(
                        queryFilterString.concat(" WHERE "), entityManager, newList, Boolean.FALSE))
                .thenReturn(query);
        functTrans.when(
                () -> FunctionsTransform.getQueryBuilderString(
                        queryCountFilterString.concat(" WHERE "), entityManager, newList, Boolean.FALSE))
                .thenReturn(queryCount);

        ReflectionTestUtils.setField(tableService , "entityManager" , entityManager );
        ReflectionTestUtils.setField(tableService , "queryFilter" , queryFilterString );
        ReflectionTestUtils.setField(tableService , "queryCountFilter" , queryCountFilterString );
        ReflectionTestUtils.setField(tableService , "response" , response );
    }

    @Test
    public void itSuccess(){
        Response responseService = tableService.get(list);
        Integer code = 200;
        Assert.assertEquals(code , responseService.getCode());
    }

    @After
    public void endTest(){
        functTrans.close();
    }
}
