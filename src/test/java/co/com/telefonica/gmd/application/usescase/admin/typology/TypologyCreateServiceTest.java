package co.com.telefonica.gmd.application.usescase.admin.typology;

import co.com.telefonica.gmd.application.repositories.database.admin.typology.ITypologyRepository;
import co.com.telefonica.gmd.domain.admin.typology.TypologyDTO;
import co.com.telefonica.gmd.domain.admin.typology.TypologyEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class TypologyCreateServiceTest {
    @Mock
    private ITypologyRepository typologyRepository;

    private Response response;
    private TypologyDTO typologyDTO;
    private TypologyEntity typologyEntity;

    private TypologyCreateService typologyCreateService;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Ok";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        response = new Response();

        typologyDTO = new TypologyDTO();
        typologyDTO.setTipologia("tipologia");

        typologyEntity = new TypologyEntity();
        typologyEntity.setTipologia("tipologia");

        Mockito.when( typologyRepository.save(typologyEntity) ).thenReturn(new TypologyEntity());

        typologyCreateService = new TypologyCreateService();

        ReflectionTestUtils.setField( typologyCreateService, "typologyRepository" , typologyRepository);
        ReflectionTestUtils.setField( typologyCreateService, "response" , response);
    }

    @Test
    public void getTypologyCreateService(){
        Response response = typologyCreateService.get(typologyDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
