package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserDetailRepository;
import co.com.telefonica.gmd.domain.user.UserAllQeury;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class UserDetailServiceTest {

    @Mock
    private IUserDetailRepository detailRepository;

    private Response response;
    private UserDetailService userDetailService;

    public static final Integer CODE = 200;
    public static final String SMS = "Ok";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        ArrayList<UserAllQeury> list = new ArrayList<>();

        Mockito.when(detailRepository.getDetailUser(1)).thenReturn(list);

        userDetailService = new UserDetailService();

        ReflectionTestUtils.setField(userDetailService, "detailRepository", detailRepository);
        ReflectionTestUtils.setField(userDetailService, "response", response);
    }

    @Test
    public void testUserDetailService(){
        userDetailService.get(1);
        Assert.assertEquals(CODE,response.getCode());
        Assert.assertEquals(SMS, response.getMessage());
    }
}
