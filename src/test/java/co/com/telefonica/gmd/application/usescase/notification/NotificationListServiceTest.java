package co.com.telefonica.gmd.application.usescase.notification;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.application.repositories.database.notification.INotificationRepository;
import co.com.telefonica.gmd.domain.notification.NotificationEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;

@SpringBootTest
public class NotificationListServiceTest {
    @Mock
    private INotificationRepository notificationRepository;
    @Mock
    private IIvrDetailRepository ivrDetailRepository;

    private Response response;

    private NotificationListService notificationListService;

    public static final Integer CODE = 200;
    public static final String SMS = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(SMS);

        Pageable page = PageRequest.of(0, 25);
        Integer id = new Integer(1);
        NotificationEntity notificationEntity = new NotificationEntity();
        ArrayList<NotificationEntity> list = new ArrayList<>();
        list.add(notificationEntity);
        Page<NotificationEntity> result = new PageImpl(list);
        Mockito.when(notificationRepository.getList(id, page)).thenReturn(result);

        notificationListService = new NotificationListService();
        ReflectionTestUtils.setField(notificationListService, "notificationRepository", notificationRepository);
        ReflectionTestUtils.setField(notificationListService, "ivrDetailRepository", ivrDetailRepository);
        ReflectionTestUtils.setField(notificationListService, "response", response);
    }

    @Test
    public void getUpdateListNotification(){
        Object[] args = new Object[]{"0", "25", "1" };
        notificationListService.get(args);
        Assert.assertEquals( CODE , response.getCode());
        Assert.assertEquals( SMS , response.getMessage());
    }
}
