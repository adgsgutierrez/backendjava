package co.com.telefonica.gmd.application.usescase.internalplant;

import co.com.telefonica.gmd.application.repositories.database.internalplant.IInternalPlantRepository;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantCreateDTO;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.platform.beans.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
public class InternalPlantCreateServiceTest {

    @Mock
    private IInternalPlantRepository internalPlantRepository;

    private Response response;


    private InternalPlantCreateDTO internalPlantCreateDTO;
    private InternalPlantEntity internalPlantEntity;
    private InternalPlantCreateService internalPlantCreateService;
    private SimpleDateFormat format;
    private Timestamp timestamp;
    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";
    private static final Integer CODE_ERROR = 300;
    private static final String MESSAGE_ERROR = "Error";

    @Before
    public void setup() throws ParseException {
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        format =  new SimpleDateFormat("YYYY-MM-DD hh:mm:ss");

        internalPlantCreateDTO =  new InternalPlantCreateDTO();
        internalPlantCreateDTO.setIdUsuario(1);
        internalPlantCreateDTO.setIdMa(1);
        internalPlantCreateDTO.setIdEecc(1);
        internalPlantCreateDTO.setFechaHoraRegistro("2020-01-01 07:07:07");
        internalPlantCreateDTO.setTecnicoReporta("Jhon doe");
        internalPlantCreateDTO.setCelularTecnico("0000000000");
        internalPlantCreateDTO.setDesFalla("prueba");
        internalPlantCreateDTO.setClientesAfectados(1234);
        internalPlantCreateDTO.setViabilidadTicket(1234);
        internalPlantCreateDTO.setFechaInicialRegistro( new Date() );

        internalPlantEntity =  new InternalPlantEntity();
        internalPlantEntity.setFkIdUsuario(internalPlantCreateDTO.getIdUsuario());
        internalPlantEntity.setFkIdEecc(internalPlantCreateDTO.getIdEecc());
        internalPlantEntity.setFechaHoraInicial( new Timestamp(internalPlantCreateDTO.getFechaInicialRegistro().getTime()));
        internalPlantEntity.setFechaHoraRegistro(
                new Timestamp(format.parse(internalPlantCreateDTO.getFechaHoraRegistro()).getTime()));
         internalPlantEntity.setTecnicoReporta(internalPlantCreateDTO.getTecnicoReporta());
        internalPlantEntity.setCelularTecnico(internalPlantCreateDTO.getCelularTecnico());
        internalPlantEntity.setDesFalla(internalPlantCreateDTO.getDesFalla());
        internalPlantEntity.setClientesAfectados(internalPlantCreateDTO.getClientesAfectados());
        internalPlantEntity.setViabilidadTicket(internalPlantCreateDTO.getViabilidadTicket());

        Mockito.when(internalPlantRepository.save(internalPlantEntity)).thenReturn(new InternalPlantEntity());
        internalPlantCreateService =  new InternalPlantCreateService();

        ReflectionTestUtils.setField(internalPlantCreateService, "internalPlantRepository", internalPlantRepository);
        ReflectionTestUtils.setField(internalPlantCreateService, "response", response);

    }

    @Test
    public void testUseCaseGet(){
        internalPlantCreateService.get(internalPlantCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

   /* @Test()
    public void testUseCaseGetCatch(){
        internalPlantCreateDTO.setFechaHoraRegistro("texto de prueba");
        internalPlantCreateService.get(internalPlantCreateDTO);
        Assert.assertEquals(CODE_ERROR, response.getCode());
        Assert.assertEquals(MESSAGE_ERROR, response.getMessage());
    }*/
}
