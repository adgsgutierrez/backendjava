package co.com.telefonica.gmd;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.boot.SpringApplication;

public class GestorDanosMasivosApplicationTest {

    private static MockedStatic<SpringApplication> utilities;

    @Before
    public void setup(){
        utilities = Mockito.mockStatic(SpringApplication.class);
        utilities.when( () -> SpringApplication.run(GestorDanosMasivosApplication.class, new String[]{}) ).thenReturn( null);
    }

    @Test
    public void startApplication(){
        GestorDanosMasivosApplication.main(new String[]{});
    }

    @After
    public void endTest(){
        utilities.close();
    }
}
