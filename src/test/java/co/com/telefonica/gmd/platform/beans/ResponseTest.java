package co.com.telefonica.gmd.platform.beans;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class ResponseTest {

    private Response response;
    private ResponseHada responseHada;

    @Before
    public void validateResponse(){
        response = new Response();
        responseHada = new ResponseHada();
    }

    @Test
    public void validateBeanResponse(){
        String data = "this data response";
        String sms = "this message response";
        Integer code = 200;
        response.setData( data );
        response.setMessage( sms );
        response.setCode(code);
        Assert.assertEquals(code , response.getCode());
        Assert.assertEquals(data , response.getData());
        Assert.assertEquals(sms , response.getMessage());
        Assert.assertNotNull( response.toString() );
    }

    @Test
    public void testResponseHada(){
        Date date = new Date();
        Integer id = 1;
        responseHada.toString();
        responseHada.setEventId("String");
        responseHada.setState("String");
        responseHada.setStateChange(date);
        responseHada.setFirstOcurrence(date);
        responseHada.setLastOcurrence(date);
        responseHada.setHourOcurrence("String");
        responseHada.setFlag1(1);
        responseHada.setFlag2(1);
        responseHada.setAffectedCustomer(1);
        responseHada.getCodeError();
        responseHada.getDescriptionMessage();
        responseHada.getEventId();
        responseHada.getState();
        responseHada.getStateChange();
        responseHada.getFirstOcurrence();
        responseHada.getLastOcurrence();
        responseHada.getHourOcurrence();
        responseHada.getFlag1();
        responseHada.getFlag2();
        responseHada.getAffectedCustomer();

        Assert.assertEquals("String",responseHada.getEventId());
        Assert.assertEquals("String", responseHada.getState());
        Assert.assertEquals(date,responseHada.getStateChange());
        Assert.assertEquals(date,responseHada.getFirstOcurrence());
        Assert.assertEquals(date, responseHada.getLastOcurrence());
        Assert.assertEquals("String",responseHada.getHourOcurrence());
        Assert.assertEquals(id,responseHada.getFlag1());
        Assert.assertEquals(id,responseHada.getFlag2());
        Assert.assertEquals(id,responseHada.getAffectedCustomer());

    }


}
