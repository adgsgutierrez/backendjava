package co.com.telefonica.gmd.platform.config;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@SpringBootTest
public class HandlersGDMTest {


    private HandlersGDM handlersGDM;

    @Before
    public void setup(){
        handlersGDM = new HandlersGDM();
    }

    @Test
    public void testCorsMapping(){
        handlersGDM.addCorsMappings(new CorsRegistry());
        Assert.assertNotNull(handlersGDM);
    }
}
