package co.com.telefonica.gmd.platform.config;

import io.jsonwebtoken.lang.Assert;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CaseNamingStrategyTest {

    private CaseNamingStrategy caseNamingStrategy= new CaseNamingStrategy();;
    @Mock
    private Identifier identifier;
    @Mock
    private JdbcEnvironment context;

    @Before
    public void setup(){ }

    @Test
    public void testCaseCallToPhysical(){
        Identifier catalog  = caseNamingStrategy.toPhysicalCatalogName(identifier, context);
        Assert.isNull( catalog);
        Identifier schema = caseNamingStrategy.toPhysicalSchemaName(identifier, context);
        Assert.isNull( schema );
        Identifier name = caseNamingStrategy.toPhysicalTableName(identifier, context);
        Assert.isNull( name );
        Identifier sequence = caseNamingStrategy.toPhysicalSequenceName(identifier, context);
        Assert.isNull( sequence );
        Identifier column = caseNamingStrategy.toPhysicalColumnName(identifier, context);
        Assert.isNull( column );

    }

    @Test
    public void testConvertSnakeCase(){
        Identifier identity = new Identifier("qatable" , true);
        Identifier column = caseNamingStrategy.toPhysicalColumnName(identity, context);
        Assert.notNull( column );
    }
}


