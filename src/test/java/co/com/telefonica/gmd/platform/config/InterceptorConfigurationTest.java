package co.com.telefonica.gmd.platform.config;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

@SpringBootTest
public class InterceptorConfigurationTest {

    @Mock
    private ApplicationContext applicationContext;
    @Mock
    private InterceptorRegistry registry;
    @Mock
    private InterceptorRegistration registration;

    private InterceptorConfiguration interceptorConfiguration;


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        Mockito.when(registry.addInterceptor(ArgumentMatchers.any())).thenReturn(registration);

        interceptorConfiguration = new InterceptorConfiguration();

        ReflectionTestUtils.setField(interceptorConfiguration, "applicationContext", applicationContext);
    }

    @Test
    public void testInterceptorConfiguration(){
        interceptorConfiguration.addInterceptors(registry);
        Assert.assertNotNull(interceptorConfiguration);
    }
}
