package co.com.telefonica.gmd.platform.utils.export;

import org.apache.poi.xssf.usermodel.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;

@SpringBootTest
public class ObjectToExcelTest {

    @Mock
    private XSSFWorkbook workbook;
    @Mock
    private XSSFSheet sheet;
    @Mock
    private XSSFRow row;
    @Mock
    private XSSFCell cell;
    @Mock
    private XSSFCellStyle style;
    @Mock
    private XSSFFont font;

    private ObjectToExcel objectToExcel;

    private ArrayList<String> titles;
    private ArrayList<ArrayList<Object>> matrixValuesSheet;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    private void setupInit(){
        Mockito.when(row.createCell(Mockito.anyInt())).thenReturn(cell);
        Mockito.when(sheet.createRow(Mockito.anyInt())).thenReturn(row);
        Mockito.when(workbook.createCellStyle()).thenReturn(style);
        Mockito.when(workbook.createFont()).thenReturn(font);
        objectToExcel = new ObjectToExcel();
        ReflectionTestUtils.setField(objectToExcel , "workbook" , workbook);
        ReflectionTestUtils.setField(objectToExcel , "sheet" , sheet);

        titles = new ArrayList<>();
        titles.add("title A");
        titles.add("title B");

        matrixValuesSheet = new ArrayList<>();
        ArrayList<Object> line = new ArrayList<>();
        line.add( Integer.valueOf("123") );
        line.add( Boolean.TRUE );
        line.add("Texto");
        matrixValuesSheet.add(line);
    }

    @Test
    public void itSetTitleDocumentExport(){
        setupInit();
        objectToExcel.setTitleDocumentExport(titles);
    }

    @Test
    public void itSetDataDocumentExport(){
        setupInit();
        objectToExcel.setDataDocumentExport(matrixValuesSheet);
    }

}
