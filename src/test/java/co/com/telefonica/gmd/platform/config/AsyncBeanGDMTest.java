package co.com.telefonica.gmd.platform.config;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.Executor;


@SpringBootTest
public class AsyncBeanGDMTest {

    private final Integer corePoolSize = 200;
    private final Integer maxPoolSize = 1;

    private AsyncBeanGDM asyncBeanGDM;

    @Mock
    private ThreadPoolTaskExecutor executorMock;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        asyncBeanGDM = new AsyncBeanGDM();
        ReflectionTestUtils.setField(asyncBeanGDM  , "corePoolSize" , corePoolSize);
        ReflectionTestUtils.setField(asyncBeanGDM  , "maxPoolSize" , maxPoolSize);
        ReflectionTestUtils.setField(asyncBeanGDM  , "executor" , executorMock);
    }

    @Test
    public void isExecutorNotNull(){
        Executor executor = asyncBeanGDM.taskExecutor();
        Assert.assertNotNull(executor);
    }
}
