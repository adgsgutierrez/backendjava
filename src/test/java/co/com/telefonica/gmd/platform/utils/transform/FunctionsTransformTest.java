package co.com.telefonica.gmd.platform.utils.transform;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FunctionsTransformTest {


    @Test
    public void testGenerateKeysDynamic() {
        String cadena1 = "hellow";
        String cadena2 = " ";
        String cadena3 = "world";
        String resultExpected = cadena1 + cadena2 + cadena3;
        String result = FunctionsTransform.generateKeysDynamic(cadena1,cadena2,cadena3);
        Assert.assertEquals(resultExpected , result);
    }

    @Test
    public void testEncodeDecodeEncription(){
        String encription = "data encripciones";
        String data = FunctionsTransform.encodeStringToBase64WithIteration(encription);
        String decode = FunctionsTransform.decodeStringToBase64WithIteration(data);
        Assert.assertEquals(encription , decode);
    }

    @Test
    public void testFormatDate(){
        String date = FunctionsTransform.getStringNowWithFormat("YYYY/mm/dd");
        Integer size = date.split("/").length;
        Assert.assertEquals( 3 , size.intValue());
    }

    @Test
    public void testJsonObject(){
        String objectString = "{ 'name':'John', 'age':30 }";
        JSONObject jsonObject = FunctionsTransform.stringToJsonObject(objectString);
        Assert.assertNotNull(jsonObject);
    }

}
