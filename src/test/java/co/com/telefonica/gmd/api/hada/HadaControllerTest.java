package co.com.telefonica.gmd.api.hada;

import co.com.telefonica.gmd.application.usescase.hada.IHadaService;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class HadaControllerTest {

    @Mock
    private IHadaService hadaService;

    private Response response;
    private HadaController hadaController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

       hadaController = new HadaController();

        ReflectionTestUtils.setField(hadaController, "iHadaService", hadaService);

    }

    @Test
    public void testGetReportHada(){
        hadaController.getReportHada("12");
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
