package co.com.telefonica.gmd.api.dashboard.cablecloset;

import co.com.telefonica.gmd.api.dashboard.boxesaffected.BoxesAffectedController;
import co.com.telefonica.gmd.application.usescase.dashboard.boxesaffected.IBoxesAffected;
import co.com.telefonica.gmd.application.usescase.dashboard.cablecloset.ICableCloset;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;

@SpringBootTest
public class CableClosetControllerTest {

    @Mock
    private ICableCloset cableCloset;

    private Response response;
    private CableClosetController cableClosetController;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Mockito.when(cableCloset.get(1)).thenReturn(response);

        cableClosetController = new CableClosetController();

        ReflectionTestUtils.setField(cableClosetController,"cableClosetService", cableCloset);
    }

    @Test
    public void testcableClosetController(){
        cableClosetController.getCableClosetByDistributor(1);
        Assert.assertEquals(null, response.getCode());
        Assert.assertEquals(null, response.getMessage());
    }
}
