package co.com.telefonica.gmd.api.notification;

import co.com.telefonica.gmd.application.usescase.notification.INotification;
import co.com.telefonica.gmd.domain.notification.NotificationDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class NotificationControllerTest {

    @Mock
    private INotification notificationService;

    private Response response;
    private NotificationController notificationController;
    private NotificationDTO notificationDTO;

    private String mockPage = "0";
    private String mockLimit = "25";
    private Integer mockUser = 1;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        notificationDTO = new NotificationDTO();
        notificationDTO.toString();
        notificationDTO.setIdUsuario(1);
        notificationDTO.setNotificacion("String");
        notificationDTO.setTipoNotificacion("String");
        notificationDTO.setEstado(false);
        Object[] mockData , mockData2;

        mockData = new Object[]{ mockPage, mockLimit, mockUser};
        Mockito.when(notificationService.get(mockData) ).thenReturn(response);

        mockData2 = new Object[]{ notificationDTO };
        Mockito.when(notificationService.get(mockData2) ).thenReturn(response);

        notificationController = new NotificationController();
        ReflectionTestUtils.setField( notificationController, "notificationListService" , notificationService);
        ReflectionTestUtils.setField( notificationController, "notificationCreateService", notificationService);

    }

    @Test
    public void testControllerAll(){
        notificationController.getNotifications(mockPage, mockLimit, mockUser);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
