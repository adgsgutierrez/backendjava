package co.com.telefonica.gmd.api.profile;

import co.com.telefonica.gmd.application.usescase.profile.IProfielFilter;
import co.com.telefonica.gmd.application.usescase.profile.IProfile;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class ProfileControllerTest {

    @Mock
    private IProfielFilter profielFilter;

    @Mock
    private IProfile profile;
    private Integer mockIndexFile = 1;
    private String mockPage = "0";
    private String mockLimit = "25";

    private Response response;
    private ProfileController profileController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        Object[] mockData;

        mockData = new Object[]{mockIndexFile,mockLimit,mockPage};
        Mockito.when(profielFilter.get(mockData)).thenReturn(response);

        profileController = new ProfileController();

        ReflectionTestUtils.setField(profileController, "profielFilter", profielFilter);
        ReflectionTestUtils.setField(profileController,"profile", profile);
    }

    @Test
    public void testProfileController(){
        profileController.filterProfile(mockIndexFile,mockLimit,mockPage);
        Assert.assertEquals(CODE,response.getCode());
    }

    @Test
    public void testGetProfile(){
        profileController.getProfile();
        Assert.assertEquals(CODE,response.getCode());
    }
}
