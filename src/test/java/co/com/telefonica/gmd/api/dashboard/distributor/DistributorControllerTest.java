package co.com.telefonica.gmd.api.dashboard.distributor;

import co.com.telefonica.gmd.application.usescase.dashboard.distributor.IDistributor;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class DistributorControllerTest {
    @Mock
    private IDistributor distributor;

    private Response response;
    private DistributorController distributorController;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Mockito.when(distributor.get(1)).thenReturn(response);

        distributorController = new DistributorController();

        ReflectionTestUtils.setField(distributorController, "distributorListServices",distributor);
    }

    @Test
    public void testDistributorController(){
        distributorController.getDistributorByLocation(1);
        Assert.assertEquals(null,response.getCode());
        Assert.assertEquals(null,response.getMessage());
    }
}
