package co.com.telefonica.gmd.api.user;


import co.com.telefonica.gmd.application.usescase.user.IUser;
import co.com.telefonica.gmd.application.usescase.user.IUserDetail;
import co.com.telefonica.gmd.application.usescase.user.IUserFilter;
import co.com.telefonica.gmd.domain.user.UserCreateDTO;
import co.com.telefonica.gmd.domain.user.UserDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.HashMap;

@SpringBootTest
public class UserControllerTest {

    @Mock
    private IUser userLoginService;

    @Mock
    private IUserDetail userDetail;

    @Mock
    private IUserFilter userFilter;

    private Response response;
    private UserDTO user;
    private UserCreateDTO userCreateDTO;
    private UserController userController;
    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        // Mock Response
        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);
        // Mock usuario
        user = new UserDTO();
        user.setUser("usuario");
        user.setPassword("password");
        // Mock Hash map
        HashMap<String , String> list = new HashMap<>();
        list.put("page" , "0");
        list.put("items" , "25");
        // Mock User DTO
        userCreateDTO = new UserCreateDTO();
        userCreateDTO.setPerfil(new Integer("1"));
        userCreateDTO.setEmail("ejemplo@ejemplo.com");
        userCreateDTO.setEstado( Boolean.TRUE);
        userCreateDTO.setNombres("Jhon Doe");
        userCreateDTO.setUsuarioRed("jdoe");
        // Mock use case login
        Mockito.when( userLoginService.get(user) ).thenReturn(response);
        Mockito.when( userLoginService.get(list) ).thenReturn(response);
        Mockito.when( userLoginService.get(userCreateDTO) ).thenReturn(response);
        userController = new UserController();

        ReflectionTestUtils.setField(userController , "userService" , userLoginService );
        ReflectionTestUtils.setField(userController , "useCreateService" , userLoginService );
        ReflectionTestUtils.setField(userController , "userUpdateService", userLoginService);
        ReflectionTestUtils.setField(userController, "userDetail", userDetail);
        ReflectionTestUtils.setField(userController, "userFilter", userFilter);
    }

    @Test
    public void testUseCaseLogin(){
        Response response = userController.login(user);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }

    @Test
    public void testUseCaseCreateUser(){
        Response response = userController.createUser(userCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }

    @Test
    public void testUserControllerUpdate(){
        userController.updateUser(userCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }

    @Test
    public void testGetDetailUser(){
        userController.getDetailUser(1);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }

    @Test
    public void testGetFilterProfileUser(){
        userController.getFilterProfileUser(1,"0","25");
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }
}
