package co.com.telefonica.gmd.api.admin.halfaccess;

import co.com.telefonica.gmd.application.usescase.admin.halfaccess.IHalfAccess;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.HashMap;

@SpringBootTest
public class HalfAccessControllerTest {

    @Mock
    private IHalfAccess halfAccessService;

    private Response response;
    private HalfAccessDTO halfAccessDTO;
    private HalfAccessController halfAccessController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        halfAccessDTO = new HalfAccessDTO();
        halfAccessDTO.setMediosacceso("medioacceso");
        halfAccessDTO.toString();

        halfAccessDTO = new HalfAccessDTO();
        halfAccessDTO.setIdma(BigInteger.ONE);
        halfAccessDTO.setEstado(true);

        HashMap<String , String> list = new HashMap<>();
        list.put("page" , "0");
        list.put("items" , "25");

        Mockito.when( halfAccessService.get(halfAccessDTO) ).thenReturn(response);
        Mockito.when( halfAccessService.get(list) ).thenReturn(response);

        halfAccessController = new HalfAccessController();

        ReflectionTestUtils.setField( halfAccessController, "halfAccessCreate", halfAccessService);
        ReflectionTestUtils.setField( halfAccessController , "halfAccessList", halfAccessService);
        ReflectionTestUtils.setField( halfAccessController, "halfAccessUpdate", halfAccessService);
    }

    @Test
    public void testHalfAccessGetAllHalfAccess(){
        halfAccessController.getAllHalfAccess();
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testHalfAccessTypeCreateHalfAccess(){
        halfAccessController.createHalfAccess(halfAccessDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testHalfAccessTypeUpdateCondition(){
        halfAccessController.updateHalfAccess(halfAccessDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
