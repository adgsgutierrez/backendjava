package co.com.telefonica.gmd.api.ivr;

import co.com.telefonica.gmd.application.usescase.ivr.IIvrService;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class IvrControllerTest {

    @Mock
    private IIvrService ivrService;
    private MockMultipartFile mockFile;
    private String mockUser = "1";
    private String mockIndexFile = "1";
    private String mockPage = "0";
    private String mockLimit = "25";
    private String mockNameColumn = "nameColumnFilter";
    private String mockValueColumn = "valueColumnFilter";

    private IvrController ivrController;
    private Response response;

    private static final Integer CODE = 200;
    private static final String SMS = "Message";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        ivrController = new IvrController();
        response = new Response();
        response.setCode(CODE);
        response.setMessage(SMS);
        mockFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some data".getBytes());

        Object[] mockData0 , mockData1 , mockData2 , mockData3;

        mockData0 = new Object[]{ mockFile , mockUser };
        Mockito.when( ivrService.get(mockData0) ).thenReturn(response);

        mockData1 = new Object[]{ mockPage , mockLimit , mockNameColumn , mockValueColumn };
        Mockito.when( ivrService.get(mockData1) ).thenReturn(response);

        mockData2 = new Object[]{ mockIndexFile , mockPage , mockLimit , mockNameColumn , mockValueColumn};
        Mockito.when( ivrService.get(mockData2) ).thenReturn(response);

        mockData3 = new Object[]{1};
        Mockito.when( ivrService.get(mockData3) ).thenReturn(response);

        ReflectionTestUtils.setField( ivrController , "ivrListDetailService" , ivrService );
        ReflectionTestUtils.setField( ivrController , "ivrService" , ivrService );
        ReflectionTestUtils.setField( ivrController , "ivrListService" , ivrService );
        ReflectionTestUtils.setField( ivrController , "ivrListDetailTicket", ivrService );
        ReflectionTestUtils.setField( ivrController , "ivrCloseFileOpened", ivrService );
    }

    @Test
    public void testControllerLoad(){
        Response responseController = ivrController.load(mockFile , mockUser);
        Assert.assertEquals(CODE , responseController.getCode());
        Assert.assertEquals(SMS , responseController.getMessage());
    }

    @Test
    public void testReadAllIvr(){
        Response responseController = ivrController.readAllIvr(mockPage,mockLimit,mockNameColumn , mockValueColumn);
        Assert.assertEquals(CODE , responseController.getCode());
        Assert.assertEquals(SMS , responseController.getMessage());
    }

    @Test
    public void testDetail(){
        ivrController.detail(mockIndexFile , mockPage , mockLimit , mockNameColumn , mockValueColumn);
        Assert.assertEquals(CODE , response.getCode());
        Assert.assertEquals(SMS , response.getMessage());
    }

    @Test
    public void testDetailTicket(){
        ivrController.detailTicket("134",mockLimit,mockPage);
        Assert.assertEquals(CODE , response.getCode());
        Assert.assertEquals(SMS , response.getMessage());
    }

    @Test
    public void testCloseFile(){
        ivrController.closeFileOpened(1);
        Assert.assertEquals(CODE , response.getCode());
        Assert.assertEquals(SMS , response.getMessage());
    }
}
