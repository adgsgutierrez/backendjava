package co.com.telefonica.gmd.api.conteogravedad;

import co.com.telefonica.gmd.application.usescase.conteogravedad.IGravedad;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class ConteoGravedadControllerTest {

    @Mock
    private IGravedad gravedadService;

    private Response response;
    private ConteoGravedadController conteoGravedadController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        Mockito.when( gravedadService.get() ).thenReturn(response);

        conteoGravedadController = new ConteoGravedadController();

        ReflectionTestUtils.setField(conteoGravedadController,"gravedadGeneral",gravedadService);
    }

    @Test
    public void testConteoGravedad(){
        conteoGravedadController.getAll();
        Assert.assertEquals(CODE , response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

}
