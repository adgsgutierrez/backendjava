package co.com.telefonica.gmd.api.admin.networktype;

import co.com.telefonica.gmd.application.usescase.admin.networktype.INetworkType;
import co.com.telefonica.gmd.domain.admin.networktype.NetworktTypeDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.HashMap;

@SpringBootTest
public class NetworkTypeControllerTest {

    @Mock
    private INetworkType networkTypeService;

    private Response response;
    private NetworktTypeDTO networktTypeDTO;
    private NetworkTypeController networkTypeController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        networktTypeDTO = new NetworktTypeDTO();
        networktTypeDTO.setTipored("tipored");
        networktTypeDTO.toString();

        networktTypeDTO = new NetworktTypeDTO();
        networktTypeDTO.setIdtipored(BigInteger.ONE);
        networktTypeDTO.setEstado(true);

        HashMap<String , String> list = new HashMap<>();
        list.put("page" , "0");
        list.put("items" , "25");

        Mockito.when( networkTypeService.get(networktTypeDTO) ).thenReturn(response);
        Mockito.when( networkTypeService.get(list) ).thenReturn(response);

        networkTypeController = new NetworkTypeController();

        ReflectionTestUtils.setField( networkTypeController, "networkTypeCreate" , networkTypeService);
        ReflectionTestUtils.setField( networkTypeController, "networkTypeList" , networkTypeService);
        ReflectionTestUtils.setField( networkTypeController, "networkTypeUpdate" , networkTypeService);
    }

    @Test
    public void testNetworkTypeGetAllNetworkType(){
        networkTypeController.getAllNetworkType();
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testNetworkTypeCreateNetworkType(){
        networkTypeController.createNetworkType(networktTypeDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testNetworkTypeUpdateCondition(){
        networkTypeController.updateNetworkType(networktTypeDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

}
