package co.com.telefonica.gmd.api.search;

import co.com.telefonica.gmd.application.usescase.search.ISearch;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class SearchControllerTest {

    @Mock
    private ISearch search;

    private SearchController searchController;
    private Response response;

    private String mockIndexFile = "1";
    private String mockPage = "0";
    private String mockLimit = "25";
    private Object[] mockData;


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        mockData =  new Object[]{mockIndexFile, mockLimit, mockPage};

        Mockito.when(search.get(mockData)).thenReturn(response);

        searchController = new SearchController();

        ReflectionTestUtils.setField(searchController, "search",search);

    }

    @Test
    public void testGetSearch(){
        searchController.getSearch(mockIndexFile,mockPage, mockLimit);
        Assert.assertEquals(null , response.getCode());
        Assert.assertEquals(null , response.getMessage());
    }
}
