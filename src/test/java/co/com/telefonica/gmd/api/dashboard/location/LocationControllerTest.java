package co.com.telefonica.gmd.api.dashboard.location;

import co.com.telefonica.gmd.application.usescase.dashboard.location.ILocation;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class LocationControllerTest {
    @Mock
    private ILocation location;

    private Response response;
    private LocationController locationController;


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();

        Mockito.when(location.get(1)).thenReturn(response);

        locationController = new LocationController();

        ReflectionTestUtils.setField(locationController, "locationListService", location);

    }

    @Test
    public void testLocationController(){
        locationController.getLocationByDpto(1);
        Assert.assertEquals(null,response.getMessage());
        Assert.assertEquals(null,response.getCode());
    }
}
