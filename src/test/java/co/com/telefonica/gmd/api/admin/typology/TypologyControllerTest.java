package co.com.telefonica.gmd.api.admin.typology;

import co.com.telefonica.gmd.application.usescase.admin.typology.ITypology;
import co.com.telefonica.gmd.domain.admin.typology.TypologyDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.HashMap;

@SpringBootTest
public class TypologyControllerTest {
    @Mock
    private ITypology typologyService;

    private Response response;
    private TypologyDTO typologyDTO;
    private TypologyController typologyController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        typologyDTO = new TypologyDTO();
        typologyDTO.setTipologia("tipologia");
        typologyDTO.toString();

        typologyDTO = new TypologyDTO();
        typologyDTO.setIdtipologia(BigInteger.ONE);
        typologyDTO.setEstado(true);

        HashMap<String , String> list = new HashMap<>();
        list.put("page" , "0");
        list.put("items" , "25");

        Mockito.when( typologyService.get(typologyDTO) ).thenReturn(response);
        Mockito.when( typologyService.get(list) ).thenReturn(response);

        typologyController = new TypologyController();

        ReflectionTestUtils.setField( typologyController , "typologyCreate" , typologyService);
        ReflectionTestUtils.setField( typologyController, "typologyList" , typologyService);
        ReflectionTestUtils.setField( typologyController, "typologyUpdate" , typologyService);

    }

    @Test
    public void testTypoligyGetAllTypoligy(){
        typologyController.getAllTypology();
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testTypoligyCreateTypoligy(){
        typologyController.createTypology(typologyDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testTypoligyUpdateCondition(){
        typologyController.updateTypology(typologyDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
