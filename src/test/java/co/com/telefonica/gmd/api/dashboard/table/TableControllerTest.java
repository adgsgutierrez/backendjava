package co.com.telefonica.gmd.api.dashboard.table;

import co.com.telefonica.gmd.application.usescase.dashboard.table.ITable;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;

@SpringBootTest
public class TableControllerTest {
    @Mock
    private ITable tableService;

    private TableController tableController;
    private Response response;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "Success";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();
        response.setMessage(MESSAGE);
        response.setCode(CODE);

        HashMap<String , Object> list = new HashMap<>();

        Mockito.when(tableService.get(list)).thenReturn(response);

        tableController = new TableController();

        ReflectionTestUtils.setField(tableController, "tableListService", tableService);
    }

    @Test
    public void testGetAllTable(){
        HashMap<String , Object> list = new HashMap<>();
        tableController.getAllTable(list);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
