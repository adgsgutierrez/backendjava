package co.com.telefonica.gmd.api.admin.eecc;

import co.com.telefonica.gmd.application.usescase.admin.eecc.IEecc;
import co.com.telefonica.gmd.domain.admin.eecc.EeccDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.HashMap;

@SpringBootTest
public class EeccControllerTest {

    @Mock
    private IEecc eeccService;

    private Response response;
    private EeccDTO eeccDTO;
    private EeccController eeccController;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        eeccDTO = new EeccDTO();
        eeccDTO.setProveedorContratista("proveedorcontratista");
        eeccDTO.toString();

        eeccDTO = new EeccDTO();
        eeccDTO.setIdeecc(BigInteger.ONE);
        eeccDTO.setEstado(false);

        HashMap<String , String> list = new HashMap<>();
        list.put("page" , "0");
        list.put("items" , "25");

        Mockito.when( eeccService.get(eeccDTO) ).thenReturn(response);
        Mockito.when( eeccService.get(list) ).thenReturn(response);

        eeccController = new EeccController();

        ReflectionTestUtils.setField( eeccController , "eeccCreate" , eeccService);
        ReflectionTestUtils.setField( eeccController , "eeccList" , eeccService);
        ReflectionTestUtils.setField( eeccController, "eeccUpdate", eeccService);

    }

    @Test
    public void testEeccGetAllEecc(){
        eeccController.getAllEecc();
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testEeccCreateEecc(){
        eeccController.createEecc(eeccDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }

    @Test
    public void testUpdateEecc() {
        eeccController.updateEecc(eeccDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
