package co.com.telefonica.gmd.api.dashboard.department;

import co.com.telefonica.gmd.application.usescase.dashboard.department.IDepartment;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class DepartmentControllerTest {

    @Mock
    private IDepartment department;

    private Response response;
    private DepartmentController departmentController;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        Object obj = new Object[]{};

        Mockito.when(department.get(obj)).thenReturn(response);

        departmentController = new DepartmentController();

        ReflectionTestUtils.setField(departmentController, "departmentListService",department);
    }

    @Test
    public void testDepartmentController(){
        departmentController.getAllDepartment();
        Assert.assertEquals(null,response.getCode());
        Assert.assertEquals(null,response.getMessage());
    }
}
