package co.com.telefonica.gmd.api.externalplant;

import co.com.telefonica.gmd.application.usescase.externalplant.IExternalPlant;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantCreateDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class ExternalPlantControllerTest {

    @Mock
    private IExternalPlant externalPlantService;

    private Response response;
    private ExternalPlantCreateDTO externalPlantCreateDTO;
    private ExternalPlantController externalPlantController;
    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        externalPlantCreateDTO = new ExternalPlantCreateDTO();
        externalPlantCreateDTO.setCelularTecnico("0000000000");
        externalPlantCreateDTO.setClientesAfectados(1234);
        externalPlantCreateDTO.setDesFalla("prueba");
        externalPlantCreateDTO.setFechaHoraRegistro("2020-01-01 07:07:07");
       // externalPlantCreateDTO.setFechaRequerida("2020-01-01 07:07:07");
       // externalPlantCreateDTO.setFechaInicialDano("2020-01-01 07:07:07");
        externalPlantCreateDTO.setIdEecc(1);
        externalPlantCreateDTO.setIdMa(1);
        externalPlantCreateDTO.setIdTipologia(1);
        externalPlantCreateDTO.setIdTipoRed(1);
        externalPlantCreateDTO.setIdTipoRed(1);
        externalPlantCreateDTO.setIdUsuario(1);
        externalPlantCreateDTO.setViabilidadTicket(11);
        externalPlantCreateDTO.toString();

        Mockito.when( externalPlantService.get(externalPlantCreateDTO)).thenReturn(response);
        externalPlantController =  new ExternalPlantController();

        ReflectionTestUtils.setField(externalPlantController, "externalPlantService", externalPlantService);
    }

    @Test
    public void testUseCaseCreate(){
        Response response = externalPlantController.createExternalPlant(externalPlantCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
