package co.com.telefonica.gmd.api.permits;

import co.com.telefonica.gmd.application.usescase.permits.IPermitsService;
import co.com.telefonica.gmd.domain.permits.PermitsDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class PermitsControllerTest {

    @Mock
    private IPermitsService permitsService;

    private Integer mockIndexFile = 1;
    private String mockPage = "0";
    private String mockLimit = "25";

    private PermitsController permitsController;
    private Response response;
    private PermitsDTO permitsDTO;

    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);
        permitsDTO = new PermitsDTO();

        Object[] mockData;
        Object[] data = new Object[]{permitsDTO};

        mockData = new Object[]{mockIndexFile, mockLimit, mockPage};
        Mockito.when(permitsService.get(mockData)).thenReturn(response);
        Mockito.when(permitsService.get(data)).thenReturn(response);

        permitsController = new PermitsController();

        ReflectionTestUtils.setField(permitsController, "permitsService", permitsService);
        ReflectionTestUtils.setField(permitsController, "permitsUpdateService", permitsService);

    }

    @Test
    public void testPermits(){
        permitsController.getAllPermits(mockIndexFile,mockLimit,mockPage);
        Assert.assertEquals(CODE , response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }

    @Test
    public void testUpdatePermits(){
        permitsController.updatePermits(permitsDTO);
        Assert.assertEquals(CODE , response.getCode());
        Assert.assertEquals(MESSAGE , response.getMessage());
    }
}
