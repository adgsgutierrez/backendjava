package co.com.telefonica.gmd.api.detectionthresholds;

import co.com.telefonica.gmd.application.usescase.detectionthresholds.IElements;
import co.com.telefonica.gmd.application.usescase.detectionthresholds.IThresholds;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;

@SpringBootTest
public class ElementsControllerTest {

    @Mock
    private IElements elements;

    @Mock
    private IThresholds thresholds;

    private Response response;
    private ElementsController elementsController;
    private ThresholdDTO thresholdDTO;

    private static final Integer CODE = 200;

    @Before
    public void setup(){

        MockitoAnnotations.initMocks(this);

        response = new Response();
        thresholdDTO = new ThresholdDTO();
        Object[] data = new Object[1];
        HashMap<String, String> list = new HashMap<>();

        Mockito.when(thresholds.get(data)).thenReturn(response);
        Mockito.when(elements.get(list)).thenReturn(response);
        Mockito.when(thresholds.get(data)).thenReturn(response);

        elementsController = new ElementsController();

        ReflectionTestUtils.setField(elementsController, "elements", elements);
        ReflectionTestUtils.setField(elementsController, "thresholds", thresholds);
        ReflectionTestUtils.setField(elementsController, "thresholdsUpdate", thresholds);

    }

    @Test
    public void testGetElements(){
        elementsController.getElements();
        Assert.assertEquals(null, response.getCode());
    }

    @Test
    public void testGetElementID(){
        elementsController.getElementID(1);
        Assert.assertEquals(null, response.getCode());
    }

    @Test
    public void testUpdateElement(){
        elementsController.updateElement(thresholdDTO);
        Assert.assertEquals(null, response.getCode());
    }
}
