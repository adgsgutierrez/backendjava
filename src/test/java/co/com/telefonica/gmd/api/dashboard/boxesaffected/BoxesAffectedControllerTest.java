package co.com.telefonica.gmd.api.dashboard.boxesaffected;

import co.com.telefonica.gmd.application.usescase.dashboard.boxesaffected.IBoxesAffected;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;

@SpringBootTest
public class BoxesAffectedControllerTest {

    @Mock
    private IBoxesAffected boxesAffected;

    private Response response;
    private BoxesAffectedController boxesAffectedController;
    private HashMap<String, Object> paramsInput;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        paramsInput = new HashMap<>();

        Mockito.when(boxesAffected.getBoxesAffected(paramsInput)).thenReturn(response);

        boxesAffectedController = new BoxesAffectedController();

        ReflectionTestUtils.setField(boxesAffectedController, "boxesAffected", boxesAffected);
    }

    @Test
    public void test(){
        HashMap<String,Object> data = new HashMap<>();
        data.put("test",2);
        boxesAffectedController.getBoxesAffected(data);
        Assert.assertEquals(null, response.getCode());
        Assert.assertEquals(null, response.getMessage());
    }

}
