package co.com.telefonica.gmd.api.dashboard.detailtypeplant;

import co.com.telefonica.gmd.application.usescase.dashboard.detailtypeplant.IDetailTypePlant;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class DetailTypePlantControllerTest {

    @Mock
    private IDetailTypePlant detailTypePlant;

    private Response response;
    private DetailTypePlantController detailTypePlantController;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        response = new Response();
        Mockito.when(detailTypePlant.get(1,"PI")).thenReturn(response);

        detailTypePlantController = new DetailTypePlantController();

        ReflectionTestUtils.setField(detailTypePlantController, "detailTypePlant", detailTypePlant);
    }

    @Test
    public void testDetailTypeController(){
        detailTypePlantController.getDetailNewDamage(1,"PE");
        Assert.assertEquals(null,response.getCode());
        Assert.assertEquals(null, response.getMessage());
    }
}
