package co.com.telefonica.gmd.api.internalplant;

import co.com.telefonica.gmd.application.usescase.internalplant.IInternalPlant;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantCreateDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
public class InternalPlantControllerTest {

    @Mock
    private IInternalPlant internalPlantService;

    private Response response;
    private InternalPlantCreateDTO internalPlantCreateDTO;
    private InternalPlantController internalPlantController;
    private static final Integer CODE = 200;
    private static final String MESSAGE = "OK";

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        response = new Response();
        response.setCode(CODE);
        response.setMessage(MESSAGE);

        internalPlantCreateDTO = new InternalPlantCreateDTO();
        internalPlantCreateDTO.setCelularTecnico("0000000000");
        internalPlantCreateDTO.setClientesAfectados(2939);
        internalPlantCreateDTO.setDesFalla("prueba");
        internalPlantCreateDTO.setRangoElementosAfectados("prueba");
        internalPlantCreateDTO.setFechaHoraRegistro("2020-10-15 07:07:07");
        internalPlantCreateDTO.setIdEecc(1);
        internalPlantCreateDTO.setIdMa(1);
        internalPlantCreateDTO.setIdUsuario(1);
        internalPlantCreateDTO.setResponsableAtencionDanoMasivo("prueba");
        internalPlantCreateDTO.setTecnicoReporta("prueba");
        internalPlantCreateDTO.setViabilidadTicket(1234);
        internalPlantCreateDTO.toString();

        Mockito.when( internalPlantService.get(internalPlantCreateDTO)).thenReturn(response);
        internalPlantController =  new InternalPlantController();

        ReflectionTestUtils.setField(internalPlantController, "internalPlantService", internalPlantService);
    }

    @Test
    public void testUseCaseCreate(){
        Response response = internalPlantController.createInternalPlant(internalPlantCreateDTO);
        Assert.assertEquals(CODE, response.getCode());
        Assert.assertEquals(MESSAGE, response.getMessage());
    }
}
