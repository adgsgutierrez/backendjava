package co.com.telefonica.gmd.application.usescase.admin.typology;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.admin.typology.ITypologyRepository;
import co.com.telefonica.gmd.domain.admin.typology.TypologyDTO;
import co.com.telefonica.gmd.domain.admin.typology.TypologyEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("TypologyCreate")
@Slf4j
public class TypologyCreateService implements ITypology{

    @Autowired
    private ITypologyRepository typologyRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        TypologyDTO typologyDTO = (TypologyDTO) data;
        TypologyEntity typologyEntity = new TypologyEntity();
        typologyEntity.setTipologia(typologyDTO.getTipologia());
        typologyEntity.setEstado(typologyDTO.getEstado());
        response.setData(
                (typologyRepository.save(typologyEntity) !=null)
        );
        return response;
    }
}
