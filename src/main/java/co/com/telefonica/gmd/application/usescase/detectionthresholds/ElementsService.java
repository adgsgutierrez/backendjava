package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IElementsRepository;
import co.com.telefonica.gmd.domain.detectionthresholds.ElementsEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("Elements")
@Slf4j
public class ElementsService implements IElements{

    @Autowired
    private IElementsRepository elementsRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        List<ElementsEntity> list = elementsRepository.getElements();
        response.setData(list);
        return response;
    }
}
