package co.com.telefonica.gmd.application.repositories.database.dashboard.cablecloset;

import co.com.telefonica.gmd.domain.dashboard.cablecloset.CableClosetListQuery;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ICableClosetRepository  extends CrudRepository<InternalPlantEntity, String> {

    @Query(value = "SELECT AC.\"IdAC\", AC.\"ArmarioCable\" " +
            "FROM \"V2_DM\".\"ARMARIO_CABLE\" as AC " +
            "INNER JOIN \"V2_DM\".\"DISTRIBUIDOR\" as Dis ON AC.\"FK_IdDistribuidor\" = Dis.\"IdDistribuidor\" " +
            "WHERE Dis.\"IdDistribuidor\" =:distributor ",
            nativeQuery = true)
    ArrayList<CableClosetListQuery> getCableClosetByDistributor(@Param("distributor")Integer distributor);
}
