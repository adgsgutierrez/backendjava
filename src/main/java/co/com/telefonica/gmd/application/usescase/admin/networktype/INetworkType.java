package co.com.telefonica.gmd.application.usescase.admin.networktype;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.platform.beans.Response;

public interface INetworkType {
    public Response get(Object data);
}
