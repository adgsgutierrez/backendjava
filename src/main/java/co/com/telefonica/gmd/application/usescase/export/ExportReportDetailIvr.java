package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("ExportReportDetailIvr")
@Slf4j
public class ExportReportDetailIvr implements IExportOtherFile {

    @Autowired
    private ObjectToExcel export;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${query.view.detailIvr}")
    private String queryFilterIvr;

    @Override
    public List<String> getWorkbook(Map<String, Object> paramsFilter) {
        Query query = entityManager.createNativeQuery(queryFilterIvr);
        Integer ticket = Integer.valueOf( String.valueOf( paramsFilter.get("ViabilidadTicket")));
        query.setParameter("viability" , ticket );
        List<Object[]> results = query.getResultList();
        ArrayList<String> dataExport = FunctionsTransform.getDataResultString(
                new Integer[]{3,4,5,6,2,7,8,9} , results
        );
        return dataExport;
    }

}
