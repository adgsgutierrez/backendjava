package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserRepository;
import co.com.telefonica.gmd.domain.user.UserCreateDTO;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("UserUpdate")
@Slf4j
public class UserUpdateService implements IUser {
    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        UserCreateDTO userDTO = (UserCreateDTO) data;
        Optional<UsuariosEntity> usuariosEntity = userRepository.findById(userDTO.getIdUsuario());
        if (usuariosEntity.isPresent()) {
            if (userDTO.getPerfil() != null) {
                usuariosEntity.get().setFkIdPerfiles(userDTO.getPerfil());
            }
            if (userDTO.getEstado() != null) {
                usuariosEntity.get().setEstado(userDTO.getEstado());
            }
            if (userDTO.getNombres() != null) {
                usuariosEntity.get().setNombres(userDTO.getNombres());
            }
            if (userDTO.getUsuarioRed() != null) {
                usuariosEntity.get().setUsuarioRed(userDTO.getUsuarioRed());
            }
            if (userDTO.getEmail() != null) {
                usuariosEntity.get().setEmail(userDTO.getEmail());
            }
            userRepository.getUpdateUser(
                    usuariosEntity.get().getIdUsuario(),
                    usuariosEntity.get().getFkIdPerfiles(),
                    usuariosEntity.get().getNombres(),
                    usuariosEntity.get().getEstado(),
                    usuariosEntity.get().getEmail(),
                    usuariosEntity.get().getUsuarioRed()
            );
        } else {
            response.setCode(300);
            response.setMessage("No se encontró usuario para actualizar");
        }
        return response;
    }
}
