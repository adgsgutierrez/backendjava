package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.platform.beans.Response;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
public interface IGravedad {

    Response get(Integer value);

}
