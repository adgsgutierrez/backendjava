package co.com.telefonica.gmd.application.usescase.permits;

import co.com.telefonica.gmd.application.repositories.database.permits.IPermitsRepository;
import co.com.telefonica.gmd.domain.permits.IPermitsQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service("Permits")
public class PermitsService implements IPermitsService{

    @Autowired
    private IPermitsRepository permitsRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] data) {
        Integer perfil = Integer.parseInt(String.valueOf(data[0]));
        Integer pageNumber = Integer.parseInt((String) data[1]);
        Integer limitNumber = Integer.parseInt((String) data[2]);
        Pageable page = PageRequest.of(pageNumber, limitNumber);
        response.setCode(200);
        response.setMessage("Ok");
        Page<Iterable<IPermitsQuery>> listPermits = permitsRepository.getPermitsId(perfil,page);
        response.setData(listPermits);
        return response;
    }
}
