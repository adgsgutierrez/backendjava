package co.com.telefonica.gmd.application.usescase.dashboard.department;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IDepartment {

    public Response get(Object data);
}
