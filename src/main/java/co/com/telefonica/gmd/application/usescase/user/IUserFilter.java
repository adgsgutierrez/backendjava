package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IUserFilter {

    Response get(Object[] data);
}
