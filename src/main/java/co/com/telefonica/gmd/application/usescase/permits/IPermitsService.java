package co.com.telefonica.gmd.application.usescase.permits;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IPermitsService {

    public Response get(Object[] data);
}
