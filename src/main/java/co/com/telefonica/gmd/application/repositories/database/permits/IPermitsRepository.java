package co.com.telefonica.gmd.application.repositories.database.permits;

import co.com.telefonica.gmd.domain.permits.IPermitsQuery;
import co.com.telefonica.gmd.domain.permits.PermitsEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IPermitsRepository extends CrudRepository<PermitsEntity, Integer> {

    @Query( value = "SELECT RPP.\"FK_IdPerfiles\" as \"IdPerfil\", RPP.\"FK_IdPermisos\" as \"IdPermiso\",Per.\"Permisos\", RPP.\"Estado\" " +
            "FROM \"V2_DM\".\"RelacionPerfilesPermisos\" as RPP " +
            "INNER JOIN \"V2_DM\".\"PERMISOS\" as Per ON RPP.\"FK_IdPermisos\" = Per.\"IdPermisos\" " +
            "WHERE RPP.\"FK_IdPerfiles\" = :perfil",
    nativeQuery = true)

    Page<Iterable<IPermitsQuery>> getPermitsId(@Param("perfil") Integer perfil, Pageable pageable);

    @Query( value = "UPDATE \"V2_DM\".\"RelacionPerfilesPermisos\" " +
            "SET \"Estado\" = :estado " +
            "WHERE \"FK_IdPerfiles\" = :idPerfiles and \"FK_IdPermisos\" = :idPermisos RETURNING \"FK_IdPerfiles\"", nativeQuery = true)
    Integer getUpdatePermits(
            @Param("estado") Boolean estado,
            @Param("idPerfiles") Integer idPerfiles,
            @Param("idPermisos") Integer idPermisos
            );
}
