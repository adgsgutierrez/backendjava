package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IThresholds {

    Response get(Object[] data);
}
