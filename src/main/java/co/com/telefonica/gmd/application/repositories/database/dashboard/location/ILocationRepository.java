package co.com.telefonica.gmd.application.repositories.database.dashboard.location;

import co.com.telefonica.gmd.domain.dashboard.location.LocationListQuery;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ILocationRepository extends CrudRepository<InternalPlantEntity, String> {

    @Query(value = "SELECT Loc.\"IdLocalidad\" , Loc.\"Localidad\", Loc.\"CodigoDANE\" " +
            "FROM \"V2_DM\".\"LOCALIDAD\" as Loc " +
            "INNER JOIN \"V2_DM\".\"DEPARTAMENTO\" as Dep ON loc.\"FK_IdDpto\" = Dep.\"IdDpto\" " +
            "WHERE Dep.\"IdDpto\" =:dpto ",
            nativeQuery = true)
    ArrayList<LocationListQuery> getLocationByDpto(@Param("dpto") Integer dpto);
}
