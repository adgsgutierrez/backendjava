package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IGravedaTable {

    Response get(Object data);
}
