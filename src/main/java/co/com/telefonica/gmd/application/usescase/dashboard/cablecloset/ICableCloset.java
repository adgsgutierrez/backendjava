package co.com.telefonica.gmd.application.usescase.dashboard.cablecloset;

import co.com.telefonica.gmd.platform.beans.Response;

public interface ICableCloset {

    public Response get(Integer distributor);
}
