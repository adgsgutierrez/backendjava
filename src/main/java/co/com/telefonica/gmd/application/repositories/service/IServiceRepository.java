package co.com.telefonica.gmd.application.repositories.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Mayo 2021
 * Author: Everis GDM
 * All rights reserved
 */
public interface IServiceRepository {

    public ResponseEntity<?> get (String url, HttpHeaders headers , Class<?> classResponse);

    public ResponseEntity<?> post (String url, Object body , HttpHeaders headers , Class<?> classResponse );

}
