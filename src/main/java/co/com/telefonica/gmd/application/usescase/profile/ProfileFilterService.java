package co.com.telefonica.gmd.application.usescase.profile;

import co.com.telefonica.gmd.application.repositories.database.profile.IProfileFilterRepository;
import co.com.telefonica.gmd.domain.profile.ProfileFilterDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service("ProfileFilter")
public class ProfileFilterService implements IProfielFilter{

    @Autowired
    private IProfileFilterRepository profileFilterRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] data) {
        response.setCode(200);
        response.setMessage("Ok");
        Integer id = Integer.parseInt(String.valueOf( data[0]));
        Integer pageNumber = Integer.parseInt((String) data[1]);
        Integer limitNumber = Integer.parseInt((String) data[2]);
        Pageable page = PageRequest.of(pageNumber, limitNumber);
        Page<Iterable<ProfileFilterDTO>> profileFilterQuery = profileFilterRepository.getFilterProfile(id ,page);
        response.setData(profileFilterQuery);
        return response;
    }
}
