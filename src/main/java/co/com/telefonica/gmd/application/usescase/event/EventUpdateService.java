package co.com.telefonica.gmd.application.usescase.event;

import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.internalplant.IInternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.domain.event.EventUpdateRequest;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.domain.ivr.SearchIdCargaIVR;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

@Service("EventUpdate")
@Slf4j
public class EventUpdateService implements IEvent{

    @Autowired
    private Response response;

    @Autowired
    private IExternalPlantRepository serviceExternalRepository;

    @Autowired
    private IInternalPlantRepository serviceInternalRepository;

    @Autowired
    private IIvrRepository ivrRepository;

    private String logdata;

    @Override
    public Response getOperation(Object request) {
        EventUpdateRequest event = (EventUpdateRequest) request;
        this.logdata = "";
        // Buscando ticket Planta Externa
        Optional<ArrayList<ExternalPlantEntity>> entityExternal = serviceExternalRepository.getByViabilityTicket( Integer.parseInt(event.getViabilidadTicket()) , Integer.parseInt(event.getIdUsuario()) );
        if(entityExternal.isPresent()) {
            for ( ExternalPlantEntity externalPlantEntity : entityExternal.get()){
                serviceExternalRepository.updateViabilityTicket( externalPlantEntity.getIdPE() , event.getEstadoTicket());
            }
            this.logdata = "Actualizado(s) ["+entityExternal.get().size()+"] registros de planta externa ** ";
        } else {
            this.logdata = "No se encontró viabilidad externa ** ";
        }
        // Buscando tickets Planta Interna
        Optional<ArrayList<InternalPlantEntity>> entityInternal = serviceInternalRepository.getByViabilityTicket(Integer.parseInt(event.getViabilidadTicket()) , Integer.parseInt(event.getIdUsuario()) );
        if(entityInternal.isPresent()) {
            for ( InternalPlantEntity internalPlantEntity : entityInternal.get()) {
                serviceInternalRepository.updateViabilityTicket( internalPlantEntity.getIdPI() , event.getEstadoTicket());
            }
            this.logdata += "Actualizado(s) ["+entityInternal.get().size()+"] registros de planta interna ** ";
        } else {
            this.logdata += "No se encontró viabilidad interna ** ";
        }
        // Buscando Archivos asociados a ese ticket
        Optional<ArrayList<SearchIdCargaIVR>> ivrEntities = ivrRepository.getListDamageByViability( Integer.parseInt(event.getViabilidadTicket()) );
        if (entityInternal.isPresent()) {
            for ( SearchIdCargaIVR ivrEntity : ivrEntities.get()) {
                try {
                    ivrRepository.updateViabilityTicket(Integer.parseInt(String.valueOf(ivrEntity.getIdCargaIVR())), event.getEstadoTicket().toString());
                } catch (JpaSystemException e){
                    log.info("Error "+ e.getMessage());
                }
            }
            this.logdata += "Actualizado(s) ["+ivrEntities.get().size()+"] registros de ficheros en IVR";
        } else {
            this.logdata += "No se encontró viabilidad interna";
        }
        response.setCode(200);
        response.setMessage(this.logdata);
        response.setData("");
        return response;
    }
}
