package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.application.repositories.database.gravedad.IGravedadRepository;
import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("GravedadTable")
@Slf4j
public class GravedadTableService implements IGravedaTable{


    @Autowired
    private IGravedadRepository gravedadRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        List<GravedadEntity> list = gravedadRepository.getTableGravity();
        response.setData(list);
        return response;
    }

}
