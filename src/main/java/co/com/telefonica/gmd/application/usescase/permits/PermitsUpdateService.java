package co.com.telefonica.gmd.application.usescase.permits;

import co.com.telefonica.gmd.application.repositories.database.permits.IPermitsRepository;
import co.com.telefonica.gmd.domain.permits.IPermitsQuery;
import co.com.telefonica.gmd.domain.permits.PermitsDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service("PermitsUpdate")
public class PermitsUpdateService implements IPermitsService{

    @Autowired
    private IPermitsRepository permitsRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] data) {
        PermitsDTO permitsQuery = (PermitsDTO) data[0];
        response.setCode(200);
        response.setMessage("Ok");
        response.setData(permitsRepository.getUpdatePermits(permitsQuery.getEstado(), permitsQuery.getIdPerfiles(), permitsQuery.getIdPermisos() ));
        return response;
    }
}
