package co.com.telefonica.gmd.application.usescase.admin.eecc;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.admin.eecc.IEeccRepository;
import co.com.telefonica.gmd.domain.admin.eecc.EeccDTO;
import co.com.telefonica.gmd.domain.admin.eecc.EeccEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("EeccCreate")
@Slf4j
public class EeccCreateService implements IEecc{

    @Autowired
    private IEeccRepository eeccRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        EeccDTO eeccDTO = (EeccDTO) data;
        EeccEntity eeccEntity = new EeccEntity();
        eeccEntity.setProveedorContratista(eeccDTO.getProveedorContratista());
        eeccEntity.setEstado(eeccDTO.getEstado());
        response.setData(
                (eeccRepository.save(eeccEntity) != null)
        );
        return response;
    }
}
