package co.com.telefonica.gmd.application.repositories.database.dashboard.distributor;

import co.com.telefonica.gmd.domain.dashboard.distributor.DistributorListQuery;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IDistributorRepository extends CrudRepository<InternalPlantEntity, String> {

    @Query(value = "SELECT Dis.\"IdDistribuidor\" ,Dis.\"Distribuidor\"\n" +
            "FROM \"V2_DM\".\"DISTRIBUIDOR\" as Dis\n" +
            "INNER JOIN \"V2_DM\".\"LOCALIDAD\" as Loc ON Dis.\"FK_IdLocalidad\" = Loc.\"IdLocalidad\"\n" +
            "WHERE Loc.\"IdLocalidad\" =:location ",
            nativeQuery = true)
    ArrayList<DistributorListQuery> getDistributorByLocation(@Param("location")Integer location);
}
