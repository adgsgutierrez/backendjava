package co.com.telefonica.gmd.application.repositories.database.conteogravedad;

import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface IConteov2Repository extends CrudRepository<GravedadEntity, BigInteger> {

    @Query(value = "SELECT \n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Severity\"='Mayor' ) as \"Mayor\",\n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Severity\"='Menor' ) as \"Menor\",\n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Severity\"='Crítica' ) as \"Critica\",\n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Tipo de Planta\"='PE' ) as \"TotalesPE\",\n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Tipo de Planta\"='PI' ) as \"TotalesPI\",\n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Estado\"='true' ) as \"Activos\",\n" +
            "\t ( Select COUNT(*) FROM \"V2_DM\".\"V_TablaDashboard\" Where \"V2_DM\".\"V_TablaDashboard\".\"Estado\"='false' ) as \"Inactivos\"" , nativeQuery = true)
    public IGetData getInformation();


    public interface IGetData {
        Integer getCritica();
        Integer getMayor();
        Integer getMenor();
        Integer getActivos();
        Integer getInactivos();
        Integer getTotalesPE();
        Integer getTotalesPI();
    }
}


