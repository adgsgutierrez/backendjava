package co.com.telefonica.gmd.application.usescase.dashboard.detailtypeplant;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IDetailTypePlant {

    Response get(Integer id, String type);
}
