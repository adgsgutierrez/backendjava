package co.com.telefonica.gmd.application.repositories.database.user;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import co.com.telefonica.gmd.domain.user.UserProfileQuery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserProfileRepository extends CrudRepository<UsuariosEntity, String> {

    @Query( value = "SELECT \n" +
            "\"V2_DM\".\"VM_PerfilPermisosUsuario\".\"Nombres\" as \"nombres\",\n" +
            "\"V2_DM\".\"VM_PerfilPermisosUsuario\".\"Perfiles\" as \"perfiles\",\n" +
            "\"V2_DM\".\"VM_PerfilPermisosUsuario\".\"Permisos\" as \"permisos\",\n" +
            "\"V2_DM\".\"VM_PerfilPermisosUsuario\".\"UsuarioRed\" as \"usuarioRed\",\n" +
            "\"V2_DM\".\"USUARIOS\".\"IdUsuario\" as \"id\"\n" +
            "FROM \"V2_DM\".\"VM_PerfilPermisosUsuario\" \n" +
            "JOIN \"V2_DM\".\"USUARIOS\" ON \"V2_DM\".\"VM_PerfilPermisosUsuario\".\"UsuarioRed\" = \"V2_DM\".\"USUARIOS\".\"UsuarioRed\"  WHERE \"V2_DM\".\"VM_PerfilPermisosUsuario\".\"UsuarioRed\" = :username",
            nativeQuery = true
    )
    Iterable<UserProfileQuery> findUserWithProfile(@Param("username") String username);

}
