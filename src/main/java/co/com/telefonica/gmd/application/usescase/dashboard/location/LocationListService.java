package co.com.telefonica.gmd.application.usescase.dashboard.location;

import co.com.telefonica.gmd.application.repositories.database.dashboard.location.ILocationRepository;
import co.com.telefonica.gmd.domain.dashboard.location.LocationListQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("LocationList")
public class LocationListService implements ILocation{

    @Autowired
    private ILocationRepository locationRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Integer dpto) {
        response.setCode(200);
        response.setMessage("Success");
        List<LocationListQuery> locationEntity = locationRepository.getLocationByDpto(dpto);
        response.setData(locationEntity);
        return response;
    }
}
