package co.com.telefonica.gmd.application.usescase.event;

import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("EventUpdatePE")
@Slf4j
public class EventUpdatePEService implements IEvent {

    @Autowired
    private IExternalPlantRepository serviceInternalRepository;

    @Override
    public Response getOperation(Object request) {
        Response response = new Response();
        response.setCode(200);
        response.setData(serviceInternalRepository.updateViabilityTicket( (Integer) request, Boolean.FALSE));
        return response;
    }
}
