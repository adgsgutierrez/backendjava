package co.com.telefonica.gmd.application.repositories.service.login;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.service.ServiceRepository;
import co.com.telefonica.gmd.domain.user.*;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import java.util.Arrays;

@Service
@Slf4j
public class LoginLdapService extends ServiceRepository implements ILoginLdapService {

    @Value("${ldap.userAuthorization}")
    private String usLoginLdap;
    @Value("${ldap.keyAuthorization}")
    private String pwLoginLdap;
    @Value("${ldap.authorization}")
    private String pathAuthorizationLdap;
    @Value("${ldap.login}")
    private String pathLoginLdap;
    @Value("${ldap.host}")
    private String hostServiceLdap;
    @Value("${ldap.group}")
    private String groupLdap;

    private static final String CONECTOR_KEY ="$";

    public UserLdapLoginResponse login(UserDTO user) {
        try {

            // Send Request to Authorization
            UserLdapLogin userLdap = new UserLdapLogin();
            userLdap.setUsername(user.getUser());
            userLdap.setPassword(user.getPassword());

            String urlLogin = hostServiceLdap.concat(pathLoginLdap);
            HttpHeaders header = new HttpHeaders();
            header.setContentType(MediaType.APPLICATION_JSON_UTF8);
            ResponseEntity<UserLdapLoginResponse> result =
                    (ResponseEntity<UserLdapLoginResponse>) this.post(urlLogin, userLdap, header, UserLdapLoginResponse.class);

            log.info("Result " + result.getBody().toString());
            if (result.getStatusCode() == HttpStatus.OK) {
                if (result.getBody().getCode() == 200) {
                   // if (Arrays.asList(result.getBody().getData().getGroups()).contains(groupLdap)) {
                        return result.getBody();
                  //  }
                  //  return null;
                }
                return null;
            }
        } catch (HttpClientErrorException  e){
            log.error("Error in service " + e.getMessage());
        }
        return null;
    }
}
