package co.com.telefonica.gmd.application.usescase.admin.halfaccess;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessDTO;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("HalfAccessCreate")
@Slf4j
public class HalfAccessCreateService implements IHalfAccess {

    @Autowired
    private IHalfAccessRepository halfAccessRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        HalfAccessDTO halfAccessDTO = (HalfAccessDTO) data;
        HalfAccessEntity halfAccessEntity = new HalfAccessEntity();
        halfAccessEntity.setMediosacceso(halfAccessDTO.getMediosacceso());
        halfAccessEntity.setEstado(halfAccessDTO.getEstado());
        response.setData(
                (halfAccessRepository.save(halfAccessEntity) != null)
        );
        return response;
    }
}
