package co.com.telefonica.gmd.application.usescase.admin.networktype;

import co.com.telefonica.gmd.application.repositories.database.admin.networktype.INetworkTypeRepository;
import co.com.telefonica.gmd.domain.admin.networktype.NetworktTypeDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("NetworkTypeUpdate")
@Slf4j
public class NetworkTypeUpdateService implements INetworkType{

    @Autowired
    private INetworkTypeRepository networkTypeRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        NetworktTypeDTO networktTypeDTO = (NetworktTypeDTO) data;
        response.setData(
                (networkTypeRepository.getUpdateNetworkType(networktTypeDTO.getIdtipored(), networktTypeDTO.getEstado()) !=null)
        );
        return response;
    }
}
