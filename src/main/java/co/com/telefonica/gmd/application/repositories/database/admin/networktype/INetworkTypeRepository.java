package co.com.telefonica.gmd.application.repositories.database.admin.networktype;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.admin.networktype.NetworkTypeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;

@Repository
public interface INetworkTypeRepository extends CrudRepository<NetworkTypeEntity, BigInteger> {

    @Query ( value = "SELECT \"IdTipoRed\", \"TipoRed\", \"Estado\" " +
            "FROM \"V2_DM\".\"TIPO_RED\" ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"TIPO_RED\" ",
            nativeQuery = true)
    ArrayList<NetworkTypeEntity> getListNetworkType();

    @Query ( value = "UPDATE \"V2_DM\".\"TIPO_RED\" SET \"Estado\" = :estado WHERE \"IdTipoRed\" = :idtipored RETURNING \"IdTipoRed\"",
            nativeQuery = true)

    BigInteger getUpdateNetworkType(@Param("idtipored") BigInteger idtipored, @Param("estado") Boolean estado);
}
