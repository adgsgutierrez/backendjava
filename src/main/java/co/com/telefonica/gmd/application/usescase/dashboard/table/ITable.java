package co.com.telefonica.gmd.application.usescase.dashboard.table;

import co.com.telefonica.gmd.platform.beans.Response;

public interface ITable {

    public Response get(Object data);
}
