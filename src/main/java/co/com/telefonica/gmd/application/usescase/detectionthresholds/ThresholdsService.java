package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IThresholdsRepository;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdsEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("Thresholds")
@Slf4j
public class ThresholdsService implements IThresholds{

    @Autowired
    private IThresholdsRepository thresholdsRepository;

    @Autowired
    private Response response;


    @Override
    public Response get(Object[] data) {
        Integer idElements = Integer.parseInt(String.valueOf(data[0]));
        response.setCode(200);
        response.setMessage("Ok");
        List<ThresholdsEntity> list = thresholdsRepository.getThresholds(idElements);
        response.setData(list);
        return response;
    }
}
