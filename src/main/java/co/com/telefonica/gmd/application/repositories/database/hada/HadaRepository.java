package co.com.telefonica.gmd.application.repositories.database.hada;

import co.com.telefonica.gmd.domain.hada.IResponseQueryHada;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface HadaRepository extends CrudRepository<IvrEntity, BigInteger> {

    /**
     * Consulta si un numero se encuentra en la lista para el IVR
     * */
    @Query( value = "SELECT Count(*) FROM \"V2_DM\".\"IVR\" WHERE \"V2_DM\".\"IVR\".\"Abonado\" = :abonado",
            nativeQuery = true )
    long countValuesFilter(@Param("abonado") String abonado);

    /**
     * Consulta el fichero de ivr asociado al caso
     * */
    @Query( value = "SELECT \"V2_DM\".\"CARGA_IVR\".* " +
            "FROM \"V2_DM\".\"IVR\" " +
            "INNER JOIN \"V2_DM\".\"CARGA_IVR\" ON \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" = \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" " +
            "WHERE \"V2_DM\".\"CARGA_IVR\".\"Estado\" = 'true' AND \"V2_DM\".\"IVR\".\"Abonado\" = :abonado ORDER BY \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" DESC LIMIT 1",
            nativeQuery = true )
    IvrEntity getFileDamage(@Param("abonado") String abonado);

    // PI
    @Query(value = "SELECT PI.\"ViabilidadTicket\" as \"EventId\", " +
            "(Select \"V2_DM\".\"GRAVEDAD\".\"TipoGravedad\" as \"Severity\" " +
            "FROM \"V2_DM\".\"GRAVEDAD\" WHERE PI.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\"), " +
            "'Distribuidor' AS \"NetType\", " +
            "PI.\"Distribuidor\" AS \"NetName\", " +
            "PI.\"Distribuidor\" AS \"NetAddress\", " +
            "'1' as \"NetState\",\n" +
            "'PI' as \"Clas\", " +
            "PI.\"TecnicoReporta\" as \"AdditionalInfo\", " +
            "coalesce(PI.\"Departamento\"||''||'/'||''||PI.\"Localidad\") as \"Location\", " +
            "PI.\"ClientesAfectados\" as \"AffectedCustomer\", " +
            "PI.\"DesFalla\" as \"Description\", " +
            "PI.\"DesFalla\" as \"Summary\", " +
            "U.\"Nombres\" as \"Agent\" " +
            "FROM \"V2_DM\".\"PLANTA_INTERNA\" as PI " +
            "INNER JOIN \"V2_DM\".\"USUARIOS\" as U ON PI.\"FK_IdUsuario\" = U.\"IdUsuario\" " +
            "WHERE PI.\"ViabilidadTicket\" = :ticket ORDER BY 1 DESC limit 1",
            nativeQuery = true)

    IResponseQueryHada getPI(@Param("ticket") Integer ticket);

    // PE
    @Query(value = "SELECT PE.\"ViabilidadTicket\" as \"EventId\", " +
            "(Select \"V2_DM\".\"GRAVEDAD\".\"TipoGravedad\" as \"Severity\" " +
            "FROM \"V2_DM\".\"GRAVEDAD\" WHERE PE.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\"), " +
            "CASE " +
            "WHEN PE.\"ArmarioCable\" = ' ' THEN 'Distribuidor' " +
            "WHEN PE.\"CajasAfectadas\" = ' ' THEN 'Distribuidor/ArmarioCable' " +
            "ELSE 'Distribuidor/ArmarioCable/CajasAfectadas' " +
            "END AS \"NetType\", " +
            "CASE " +
            "WHEN PE.\"ArmarioCable\" = '' THEN PE.\"Distribuidor\" " +
            "WHEN PE.\"CajasAfectadas\" = '' THEN CONCAT( PE.\"Distribuidor\",PE.\"ArmarioCable\") " +
            "ELSE CONCAT (PE.\"Distribuidor\",PE.\"ArmarioCable\",PE.\"CajasAfectadas\") " +
            "END AS \"NetName\", " +
            "CASE " +
            "WHEN PE.\"ArmarioCable\" = '' THEN PE.\"Distribuidor\" " +
            "WHEN PE.\"CajasAfectadas\" = '' THEN CONCAT( PE.\"Distribuidor\",PE.\"ArmarioCable\") " +
            "ELSE CONCAT (PE.\"Distribuidor\",PE.\"ArmarioCable\",PE.\"CajasAfectadas\") " +
            "END AS \"NetAddress\", " +
            "'1' as \"NetState\", " +
            "'PE' as \"Clas\", " +
            "PE.\"TecnicoReporta\" as \"AdditionalInfo\", " +
            "coalesce(PE.\"Departamento\"||''||'/'||''||PE.\"Localidad\") as \"Location\", " +
            "Tipo.\"IdTipologia\" as \"Massiva\", " +
            "PE.\"ClientesAfectados\" as \"AffectedCustomer\", " +
            "PE.\"DesFalla\" as \"Description\", " +
            "PE.\"DesFalla\" as \"Summary\", " +
            "U.\"Nombres\" as \"Agent\" " +
            "FROM \"V2_DM\".\"PLANTA_EXTERNA\" as PE " +
            "INNER JOIN \"V2_DM\".\"USUARIOS\" as U ON PE.\"FK_IdUsuario\" = U.\"IdUsuario\" " +
            "INNER JOIN \"V2_DM\".\"TIPOLOGIA\" as Tipo ON PE.\"FK_IdTipologia\" = Tipo.\"IdTipologia\" "+
            "WHERE PE.\"ViabilidadTicket\" = :ticket  ORDER BY 1 DESC limit 1",
        nativeQuery = true)
    IResponseQueryHada getPE(@Param("ticket") Integer ticket);


    @Query( value =
            "SELECT sh.* FROM ( SELECT DISTINCT " +
                    "ON (\"V2_DM\".\"IVR\".\"Abonado\") \"V2_DM\".\"IVR\".\"Abonado\"," +
                    "\"V2_DM\".\"IVR\".\"ViabilidadTicket\"," +
                    "\"V2_DM\".\"IVR\".\"FK_IdTipologia\"," +
                    "\"V2_DM\".\"IVR\".\"FechaFin\"," +
                    "\"V2_DM\".\"IVR\".\"HoraFin\"," +
                    "\"V2_DM\".\"IVR\".\"TipoDaño\"," +
                    "\"V2_DM\".\"IVR\".\"Flag1\"," +
                    "\"V2_DM\".\"IVR\".\"IdIVR\"," +
                    "\"V2_DM\".\"IVR\".\"FK_IdCargaIVR\"," +
                    "\"V2_DM\".\"IVR\".\"Flag2\" " +
                    "    FROM \"V2_DM\".\"CARGA_IVR\" " +
                    "    INNER JOIN \"V2_DM\".\"IVR\" " +
                    "    ON \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" = \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" " +
                    "    WHERE \"V2_DM\".\"CARGA_IVR\".\"Estado\"='true' " +
                    "    AND \"V2_DM\".\"IVR\".\"FechaFin\" > current_date " +
                    "    OR ( \"V2_DM\".\"IVR\".\"FechaFin\" = current_date AND \"V2_DM\".\"IVR\".\"HoraFin\" >= current_time) " +
                    "    ORDER BY \"V2_DM\".\"IVR\".\"Abonado\", \"V2_DM\".\"IVR\".\"FechaFin\" ) sh WHERE sh.\"Abonado\" = :abonado",
            nativeQuery = true )
    Object[] getDamageDetail(@Param("abonado") String abonado);

}
