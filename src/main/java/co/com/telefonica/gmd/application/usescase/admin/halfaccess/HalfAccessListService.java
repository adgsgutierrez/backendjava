package co.com.telefonica.gmd.application.usescase.admin.halfaccess;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service("HalfAccessList")
@Slf4j
public class HalfAccessListService implements IHalfAccess {

    @Autowired
    private IHalfAccessRepository halfAccessRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        HashMap<String ,String> list = (HashMap<String, String>) data;
        response.setCode(200);
        response.setMessage("Success");

        List<HalfAccessEntity> listHalfAccess = halfAccessRepository.getListHalfAccess();
        response.setData(listHalfAccess);
        return response;
    }
}
