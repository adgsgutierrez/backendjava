package co.com.telefonica.gmd.application.usescase.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.user.IUserProfileRepository;
import co.com.telefonica.gmd.application.repositories.service.login.ILoginLdapService;
import co.com.telefonica.gmd.domain.user.UserDTO;
import co.com.telefonica.gmd.domain.user.UserLdapLoginResponse;
import co.com.telefonica.gmd.domain.user.UserProfileQuery;
import co.com.telefonica.gmd.domain.user.UserResponsePermision;
import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("UserLogin")
@Slf4j
public class UserLoginService implements IUser {

    @Autowired
    private IUserProfileRepository userRepository;

    @Autowired
    private ILoginLdapService ldapLogin;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data){
            UserDTO user = (UserDTO) data;
            // Consulta servicio login
            UserLdapLoginResponse userResponseLdap = ldapLogin.login(user);
            // Consulta Base de Datos
            if (userResponseLdap != null) {
                //userRepository.refreshViewUser();
                String userDecode = FunctionsTransform.decodeStringToBase64WithIteration(user.getUser());
                log.info("UserDecode ["+userDecode+"]" );
                Iterable<UserProfileQuery> userResponse = userRepository.findUserWithProfile(userDecode);
                if (userResponse != null) {
                    UserResponsePermision userResponsePermision = new UserResponsePermision();
                    userResponse.forEach(userProfileQuery -> {
                        userResponsePermision.getPermisions().add(userProfileQuery.getPermisos());
                        userResponsePermision.setProfile(userProfileQuery.getPerfiles());
                        userResponsePermision.setId(userProfileQuery.getId());
                        userResponsePermision.setName(userProfileQuery.getNombres());

                    });
                    if (
                            userResponsePermision.getPermisions().size() > 0 &&
                                    !userResponsePermision.getId().equals("") && userResponsePermision.getId() != null
                    ) {
                        response.setCode(200);
                        response.setMessage("ok");
                        response.setData(userResponsePermision);
                    } else {
                        response.setCode(300);
                        response.setMessage("El usuario no tiene acceso al sistema");
                        response.setData("");
                    }
                } else {
                    response.setCode(300);
                    response.setMessage("El usuario no tiene acceso al sistema");
                    response.setData("");
                }
            } else {
                response.setCode(301);
                response.setMessage("El usuario no se encuentra en el sistema y/o las credenciales son incorrectas");
                response.setData("");
            }

        return response;
    }
}
