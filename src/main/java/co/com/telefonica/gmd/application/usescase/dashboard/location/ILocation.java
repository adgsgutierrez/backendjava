package co.com.telefonica.gmd.application.usescase.dashboard.location;

import co.com.telefonica.gmd.platform.beans.Response;

public interface ILocation {

    public Response get(Integer dpto);
}
