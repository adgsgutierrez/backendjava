package co.com.telefonica.gmd.application.repositories.database.profile;

import co.com.telefonica.gmd.domain.profile.ProfileEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IProfileRepository extends CrudRepository<ProfileEntity, Integer> {

    @Query(value = "SELECT * FROM \"V2_DM\".\"PERFILES\" ", nativeQuery = true)

    ArrayList<ProfileEntity> getProfile();
}
