package co.com.telefonica.gmd.application.usescase.notification;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.application.repositories.database.notification.INotificationRepository;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantCreateDTO;
import co.com.telefonica.gmd.domain.notification.NotificationDTO;
import co.com.telefonica.gmd.domain.notification.NotificationEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

@Service("NotificationList")
@Slf4j
public class NotificationListService implements INotification {

    @Autowired
    private INotificationRepository notificationRepository;

    @Autowired
    private Response response;

    private ArrayList<NotificationEntity> notificationEntity;

    @Autowired
    private IIvrDetailRepository ivrDetailRepository;

    @Override
    public Response get(Object[] data) {
        response.setCode(200);
        response.setMessage("OK");
        notificationEntity = new ArrayList<>();
        Integer user = Integer.parseInt(String.valueOf(data[2]));
        Pageable page = PageRequest.of(
                Integer.parseInt(String.valueOf(data[0])),
                Integer.parseInt(String.valueOf(data[1]))
        );
        try {
            ivrDetailRepository.setTimeZone();
        } catch (JpaSystemException e){
            log.error("Seteo de la consulta de timezone -5");
        }
        Page<NotificationEntity> listNotifications = notificationRepository.getList(user ,page);
        response.setData(listNotifications);
        return response;
    }
}
