package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

@Service("IvrListDetailTicket")
@Slf4j
public class IvrListDetailTicketService implements IIvrService{

    @Autowired
    private IIvrDetailRepository ivrDetailRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] args) {
        Integer ticket = new Integer((String) args[0]);
        Integer pageNumber = Integer.parseInt((String) args[1]);
        Integer limitNumber = Integer.parseInt((String) args[2]);
        Pageable page = PageRequest.of(pageNumber, limitNumber);
        try {
            ivrDetailRepository.setTimeZone();
        } catch (JpaSystemException e){
            log.error("Seteo de la consulta de timezone -5");
        }
        Page<Iterable<IvrDetailEntity>> listIvrTicket = ivrDetailRepository.listDetalleIvrTicket(ticket,page);
        response.setCode(200);
        response.setMessage("Ok");
        response.setData(listIvrTicket);
        return response;
    }
}
