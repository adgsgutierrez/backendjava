package co.com.telefonica.gmd.application.repositories.database.user;

import co.com.telefonica.gmd.domain.user.IUserFilterProfile;
import co.com.telefonica.gmd.domain.user.UserAllQeury;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserDetailRepository extends CrudRepository<UsuariosEntity, Integer> {

    @Query(value = "SELECT U.\"IdUsuario\", U.\"Nombres\", U.\"Estado\", Per.\"Perfiles\", U.\"Email\", U.\"UsuarioRed\" " +
            "FROM \"V2_DM\".\"USUARIOS\" as U " +
            "INNER JOIN \"V2_DM\".\"PERFILES\" AS Per ON U.\"FK_IdPerfiles\" = Per.\"IdPerfiles\" " +
            "WHERE U.\"IdUsuario\"= :IdUsuario ",
    nativeQuery = true)

    List<UserAllQeury> getDetailUser(@Param("IdUsuario") Integer IdUsuario);

    @Query(value = "SELECT U.\"IdUsuario\", U.\"Nombres\", U.\"Estado\", Per.\"Perfiles\", U.\"Email\", U.\"UsuarioRed\" " +
            "FROM \"V2_DM\".\"USUARIOS\" as U " +
            "INNER JOIN \"V2_DM\".\"PERFILES\" AS Per ON U.\"FK_IdPerfiles\" = Per.\"IdPerfiles\" " +
            "WHERE U.\"FK_IdPerfiles\"= :idPerfil ",
            nativeQuery = true)

    Page<Iterable<IUserFilterProfile>> getFilterProfileUser(@Param("idPerfil") Integer idPerfil, Pageable pageable);
}
