package co.com.telefonica.gmd.application.usescase.profile;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IProfile {

    Response get(Object data);
}
