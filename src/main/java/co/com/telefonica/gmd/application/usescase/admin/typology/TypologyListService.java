package co.com.telefonica.gmd.application.usescase.admin.typology;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.admin.typology.ITypologyRepository;
import co.com.telefonica.gmd.domain.admin.typology.TypologyEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service("TypologyList")
@Slf4j
public class TypologyListService implements ITypology {

    @Autowired
    private ITypologyRepository typologyRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        HashMap<String ,String> list = (HashMap<String, String>) data;
        response.setCode(200);
        response.setMessage("Success");
        List<TypologyEntity> listTypology = typologyRepository.getListTypology();
        response.setData(listTypology);
        return response;
    }
}
