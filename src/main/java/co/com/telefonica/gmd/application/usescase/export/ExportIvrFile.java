package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("ExportIvrFile")
@Slf4j
public class ExportIvrFile implements IExportOtherFile {

    @Autowired
    private ObjectToExcel export;

    @Autowired
    private IIvrDetailRepository ivrDetailRepository;

    @Override
    public List<String> getWorkbook(Map<String, Object> paramsFilter) {
        try {

            ArrayList<String> values = (ArrayList<String>) ivrDetailRepository.getTimeZone();
            for(String value : values){
                log.info("Zona horaria capturada " + value);
            }
            ivrDetailRepository.setTimeZone();
        } catch (JpaSystemException e){
            log.error("Seteo de la consulta de timezone -5");
        }
        List<String> result = ivrDetailRepository.getIvrDetailExport();
       return result;
    }
}
