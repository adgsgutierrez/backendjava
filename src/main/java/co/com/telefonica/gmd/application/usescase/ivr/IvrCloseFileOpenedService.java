package co.com.telefonica.gmd.application.usescase.ivr;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.domain.ivr.SearchIdCargaIVR;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("IvrCloseFileOpened")
@Slf4j
public class IvrCloseFileOpenedService implements IIvrService {

    @Autowired
    private IIvrRepository repository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] args) {
        response.setMessage("");
        Integer idFile = (Integer) args[0];
        Optional<SearchIdCargaIVR> data = repository.updateViabilityTicket(idFile , Boolean.FALSE.toString());
        response.setCode( (data.isPresent() ) ? 200 : 300 );
        response.setData( (data.isPresent() ) ? "Se actualizó el registro" : "No se actualizó el registro" );
        return response;
    }
}
