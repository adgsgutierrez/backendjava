package co.com.telefonica.gmd.application.usescase.hada;

import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.hada.HadaRepository;
import co.com.telefonica.gmd.application.repositories.database.internalplant.IInternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.hada.IResponseQueryHada;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.platform.beans.ResponseHada;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;


@Service("HadaServiceReport")
@Slf4j
public class HadaService implements IHadaService{

    @Autowired
    private HadaRepository hadaRepository;

    @Autowired
    private IIvrDetailRepository ivrDetailRepository;

    @Autowired
    private IExternalPlantRepository externalPlantRepository;

    @Autowired
    private IInternalPlantRepository internalPlantRepository;

    private final static String N_A = "N/A";

    @Override
    public ResponseHada get(String number) {
        ResponseHada responseHada = new ResponseHada();
        if ( number != null) {
            log.info("Consultando el numbero " + number);
            if (getEventReport(number) > 0) {
                responseHada.setAlarm("NOK");
                responseHada.setDescriptionMessage("OK");

                try {

                    ArrayList<String> values = (ArrayList<String>) ivrDetailRepository.getTimeZone();
                    for(String value : values){
                        log.info("Zona horaria capturada " + value);
                    }
                    ivrDetailRepository.setTimeZone();
                } catch (JpaSystemException e){
                    log.error("Seteo de la consulta de timezone -5");
                }

                // Set data File
                IvrEntity entityFile = getFileDamage(number);
                if (entityFile != null ) {
                    responseHada.setState((entityFile.getEstado()) ? "1" : "2");
                    // Set detail
                    IvrDetailEntity ivrDetailEntity = getDetail(number);
                    if (ivrDetailEntity != null ) {
                        responseHada.setLastOcurrence(entityFile.getFechaCarga());
                        responseHada.setEventId(String.valueOf(ivrDetailEntity.getViabilidadTicket()));
                        responseHada.setStateChange(ivrDetailEntity.getFechaFin());
                        responseHada.setHourOcurrence(ivrDetailEntity.getHoraFin().toString());
                        responseHada.setFlag1(ivrDetailEntity.getFlag1());
                        responseHada.setFlag2(ivrDetailEntity.getFlag2());
                        responseHada.setType(ivrDetailEntity.getFkIdTipologia().intValue());
                        // validate data report
                        responseHada = evaluateSeverity(ivrDetailEntity.getViabilidadTicket(), responseHada);
                    } else {
                        responseHada.setAlarm("OK");
                        responseHada.setDescriptionMessage("Sin Alarma Detectada");
                    }
                } else {
                    responseHada.setAlarm("OK");
                    responseHada.setDescriptionMessage("Sin Alarma Detectada");
                }

            } else {
                responseHada.setAlarm("OK");
                responseHada.setDescriptionMessage("Sin Alarma Detectada");
            }
            responseHada.setCodeError(0);
        } else {
            responseHada.setCodeError(1);
            responseHada.setDescriptionMessage("NOK");
        }
        return responseHada;
    }


    private long getEventReport(String number){
        return hadaRepository.countValuesFilter(number);
    }

    private IvrEntity getFileDamage(String number){
        return hadaRepository.getFileDamage(number);
    }

    private IvrDetailEntity getDetail(String number){
        Object[] resultMatrix = hadaRepository.getDamageDetail(number);
        if(resultMatrix.length == 1) {
            Object[] result = (Object[]) resultMatrix[0];
            IvrDetailEntity ivr = new IvrDetailEntity();
            ivr.setAbonado(String.valueOf(result[0]));
            ivr.setViabilidadTicket((Integer) result[1]);
            ivr.setFkIdTipologia(BigInteger.valueOf(Long.valueOf(String.valueOf(result[2]))));
            ivr.setFechaFin((Date) result[3]);
            ivr.setHoraFin((Time) result[4]);
            ivr.setTipoDanio((Integer) result[5]);
            ivr.setFlag1((Integer) result[6]);
            ivr.setIdIvr(BigInteger.valueOf(Long.valueOf(String.valueOf(result[7]))));
            ivr.setFkIdCargaIvr(BigInteger.valueOf(Long.valueOf(String.valueOf(result[8]))));
            ivr.setFlag2((Integer) result[9]);
            return ivr;
        }
        return null;
    }

    private ResponseHada evaluateSeverity(Integer ticket , ResponseHada responseHada){
        // Seteando valores genericos
        responseHada.setSeverity(N_A);
        responseHada.setDescription(N_A);
        responseHada.setClas(N_A);
        responseHada.setNetAddress(N_A);
        responseHada.setNetName(N_A);
        responseHada.setNetState(N_A);
        responseHada.setNetType(N_A);
        responseHada.setAdditionalInfo(N_A);
        responseHada.setSite(N_A);
        responseHada.setAgent(N_A);
        responseHada.setTally(0);
        responseHada.setMassiva(N_A);
        responseHada.setAcknowledged(0);
        // Buscando el detalle de desscripcion en PI/PE
        ExternalPlantEntity external = externalPlantRepository.getByViabilityTicket(ticket);
        if (external != null) {
            IResponseQueryHada responseQueryHada = hadaRepository.getPE(ticket);
            if(responseQueryHada != null) {
                responseHada = this.setValues(responseHada, responseQueryHada);
                return responseHada;
            }

        }
        InternalPlantEntity internal = internalPlantRepository.getByViabilityTicket(ticket);
        if (internal != null) {
            IResponseQueryHada responseQueryHada = hadaRepository.getPI(ticket);
            if(responseQueryHada != null) {
                responseHada = this.setValues(responseHada, responseQueryHada);
            }
        }
        return responseHada;
    }

    private ResponseHada setValues(ResponseHada responseHada, IResponseQueryHada responseQueryHada){
        responseHada.setNetAddress(responseQueryHada.getNetAddress());
        responseHada.setSeverity(responseQueryHada.getSeverity());
        responseHada.setAdditionalInfo(responseQueryHada.getAdditionalInfo());
        responseHada.setAgent(responseQueryHada.getAgent());
        responseHada.setNetState(responseQueryHada.getNetState());
        responseHada.setMassiva(responseQueryHada.getMassiva());
        responseHada.setClas(responseQueryHada.getClas());
        return responseHada;
    }

}
