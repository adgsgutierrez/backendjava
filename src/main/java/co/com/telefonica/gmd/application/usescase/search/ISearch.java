package co.com.telefonica.gmd.application.usescase.search;

import co.com.telefonica.gmd.platform.beans.Response;

public interface ISearch {
    Response get(Object[] args);
}
