package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.application.repositories.database.admin.eecc.IEeccRepository;
import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.application.repositories.database.admin.networktype.INetworkTypeRepository;
import co.com.telefonica.gmd.application.repositories.database.admin.typology.ITypologyRepository;
import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.application.repositories.database.internalplant.IInternalPlantRepository;
import co.com.telefonica.gmd.domain.admin.eecc.EeccEntity;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessEntity;
import co.com.telefonica.gmd.domain.admin.networktype.NetworkTypeEntity;
import co.com.telefonica.gmd.domain.admin.typology.TypologyEntity;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;

@Service("ExportDashboard")
@Slf4j
public class ExportDashboard implements IExportOtherFile {

    @Autowired
    private ObjectToExcel export;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private IExternalPlantRepository externalRepository;

    @Autowired
    private IInternalPlantRepository internalRepository;

    @Autowired
    private IHalfAccessRepository halfAccessRepository;

    @Autowired
    private ITypologyRepository typologyRepository;

    @Autowired
    private IEeccRepository eeccRepository;

    @Autowired
    private INetworkTypeRepository networkTypeRepository;

    @Value("${query.view.dashboard}")
    private String queryDashboard;

    @SneakyThrows
    @Override
    public List<String> getWorkbook(Map<String, Object> paramsFilter) {
        Query query = FunctionsTransform.getQueryBuilderString( queryDashboard , entityManager , paramsFilter);
        List<Object[]> results = query.getResultList();
        List<Object[]> resultEnv = new ArrayList<>();
        for(Object[] object : results){
            //Object[] objectEnv = Arrays.copyOf(object, object.length + 3);
            List<Object> objectsEnv = new ArrayList<>();
            for(Object obj : object){
                objectsEnv.add(obj);
            }
            if( String.valueOf(object[11]).equals("PE") ){
                Optional<ExternalPlantEntity> pe = externalRepository.findById( Integer.parseInt( String.valueOf(object[0])) );
                log.info("" + pe.get().toString());
                objectsEnv.add(pe.get().getTecnicoReporta());
                objectsEnv.add(pe.get().getFechaHoraRegistro());
                objectsEnv.add(pe.get().getFechaHoraInicial());
                objectsEnv.add(pe.get().getCelularTecnico() );
                objectsEnv.add(pe.get().getViabilidadTicket());
                objectsEnv.add( pe.get().getBarrio());
                objectsEnv.add( pe.get().getDireccion());
                Optional<EeccEntity> entityEecc = eeccRepository.findById(pe.get().getFkIdEecc());
                objectsEnv.add( entityEecc.get().getProveedorContratista() );
                Optional<HalfAccessEntity> medio = halfAccessRepository.findById(pe.get().getFkIdMa());
                objectsEnv.add( medio.get().getMediosacceso() );
                Optional<TypologyEntity> typologyEntity = typologyRepository.findById(BigInteger.valueOf(pe.get().getFkIdTipologia()));
                objectsEnv.add( typologyEntity.get().getTipologia() );
                Optional<NetworkTypeEntity> networkTypeEntity = networkTypeRepository.findById(BigInteger.valueOf(pe.get().getFkIdTipoRed()));
                objectsEnv.add( networkTypeEntity.get().getTipored() );
                objectsEnv.add("N/E");
                objectsEnv.add(pe.get().getDistribuidor());
                objectsEnv.add(pe.get().getArmarioCable());
                objectsEnv.add(pe.get().getCajasAfectadas());
            }else {
                Optional<InternalPlantEntity> pi = internalRepository.findById( Integer.parseInt( String.valueOf(object[0])) );
                log.info("" + pi.get().toString());
                objectsEnv.add( pi.get().getTecnicoReporta() );
                objectsEnv.add( pi.get().getFechaHoraRegistro() );
                objectsEnv.add( pi.get().getFechaHoraInicial());
                objectsEnv.add( pi.get().getCelularTecnico());
                objectsEnv.add(pi.get().getViabilidadTicket());
                objectsEnv.add("N/E");
                objectsEnv.add("N/E");
                Optional<EeccEntity> entityEecc = eeccRepository.findById(pi.get().getFkIdEecc());
                objectsEnv.add( entityEecc.get().getProveedorContratista() );
                Optional<HalfAccessEntity> medio = halfAccessRepository.findById(pi.get().getIdMa());
                objectsEnv.add( medio.get().getMediosacceso() );
                objectsEnv.add("N/E");
                objectsEnv.add("N/E");
                objectsEnv.add(pi.get().getResponsableAtencionDanoMasivo());
                objectsEnv.add("N/E");
                objectsEnv.add("N/E");
                objectsEnv.add("N/E");
            }
            resultEnv.add(objectsEnv.toArray());
        }
        ArrayList<String> dataExport = FunctionsTransform.getDataResultString(
                new Integer[]{1,2,3,4,5,7,8,9,10,11,12,13,15,16,17,18, 19, 20 , 21, 22 , 23 , 24,25,26,27} , resultEnv
        );
        return dataExport;
    }
}
