package co.com.telefonica.gmd.application.repositories.database.search;

import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.domain.search.ISearchQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ISearchRepository extends CrudRepository<IvrDetailEntity, Integer> {

   @Query( value = "SELECT DISTINCT (CIVR.\"IdCargaIVR\"), CIVR.\"NombreArchivo\" AS \"NombreArchivo\", CIVR.\"FechaCarga\" AS \"FechaCarga\", " +
           "CIVR.\"ClientesAfectados\" AS \"Afectados\", U.\"Nombres\" AS \"Usuario\" " +
           "FROM \"V2_DM\".\"IVR\" AS IVR " +
           "INNER JOIN \"V2_DM\".\"CARGA_IVR\" AS CIVR ON IVR.\"FK_IdCargaIVR\" = CIVR.\"IdCargaIVR\" " +
           "INNER JOIN \"V2_DM\".\"USUARIOS\" AS U ON CIVR.\"FK_IdUsuario\" = U.\"IdUsuario\" " +
           "WHERE CIVR.\"Estado\" = 'true' AND IVR.\"Abonado\" LIKE %:abonado% ORDER BY 3 DESC",
    nativeQuery = true)

    Page<Iterable<ISearchQuery>> getSearchAbonado(@Param("abonado") String abonada, Pageable pageable);
}
