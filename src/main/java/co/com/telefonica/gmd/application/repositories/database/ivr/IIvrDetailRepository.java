package co.com.telefonica.gmd.application.repositories.database.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import lombok.ToString;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Column;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Repository
public interface IIvrDetailRepository extends CrudRepository<IvrDetailEntity, BigInteger> {

    @Query(
            value = "SELECT \"IdIVR\", \"FK_IdTipologia\", \"FK_IdCargaIVR\", \"ViabilidadTicket\", \"Abonado\", \"FechaFin\", \"HoraFin\", \"TipoDaño\", \"Flag1\", \"Flag2\" " +
                    "FROM \"V2_DM\".\"IVR\" WHERE \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" = :file",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"IVR\" WHERE \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" =:file",
            nativeQuery = true
    )
    Page<Iterable<IvrDetailEntity>> listDetailFile(@Param("file") BigInteger file , Pageable pageable);

    /**
     * Consulta si un numero se encuentra en la lista para el IVR
     * */
    @Query( value =
            "SELECT " +
                    "DISTINCT ON (\"V2_DM\".\"IVR\".\"Abonado\") \"V2_DM\".\"IVR\".\"Abonado\", " +
                    "\"V2_DM\".\"IVR\".\"ViabilidadTicket\", " +
                    "\"V2_DM\".\"IVR\".\"FK_IdTipologia\", " +
                    "\"V2_DM\".\"IVR\".\"FechaFin\", " +
                    "\"V2_DM\".\"IVR\".\"HoraFin\", " +
                    "\"V2_DM\".\"IVR\".\"TipoDaño\",  " +
                    "\"V2_DM\".\"IVR\".\"Flag1\", " +
                    "\"V2_DM\".\"IVR\".\"IdIVR\", " +
                    "\"V2_DM\".\"IVR\".\"FK_IdCargaIVR\", " +
                    "\"V2_DM\".\"IVR\".\"Flag2\" " +
                    "FROM \"V2_DM\".\"CARGA_IVR\" " +
                    "INNER JOIN \"V2_DM\".\"IVR\" ON \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" = \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" " +
                    "WHERE \"V2_DM\".\"CARGA_IVR\".\"Estado\"='true' " +
                    "AND \"V2_DM\".\"IVR\".\"FechaFin\" > current_date " +
                    "OR (\"V2_DM\".\"IVR\".\"FechaFin\" = current_date " +
                    "AND \"V2_DM\".\"IVR\".\"HoraFin\" >= current_time) " +
                    "AND \"V2_DM\".\"IVR\".\"Abonado\"=:abonado " +
                    "ORDER BY \"V2_DM\".\"IVR\".\"Abonado\" , \"V2_DM\".\"IVR\".\"FechaFin\" DESC LIMIT 1",
            nativeQuery = true )
    IvrDetailEntity getDamageDetail(@Param("abonado") String abonado);

    /**
     * Consulta de datos para carga ivr
     *
     * @return*/
    @Query( value = "SELECT \"NombreArchivo\", \"Estado\", \"FK_IdTipologia\", \"ViabilidadTicket\", \"Abonado\", \"Tipo de Daño\" as \"TipoDaño\", \"Flag1\", \"Flag2\", \"FechaFin\", \"HoraFin\" FROM \"V2_DM\".\"V_CargaIVR\"",
            nativeQuery = true )
    List<Object[]> getDamageDetail();

    /**
     * Consulta de detalle ivr por viabilidadTicket
     * */
    @Query ( value = "SELECT \"IdIVR\", \"FK_IdTipologia\", \"FK_IdCargaIVR\", \"ViabilidadTicket\", \"Abonado\", \"FechaFin\", \"HoraFin\", \"TipoDaño\", \"Flag1\", \"Flag2\" " +
                    "FROM \"V2_DM\".\"IVR\" WHERE \"V2_DM\".\"IVR\".\"ViabilidadTicket\" = :ticket",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"IVR\" WHERE \"V2_DM\".\"IVR\".\"ViabilidadTicket\" =:ticket",
            nativeQuery = true )

    Page<Iterable<IvrDetailEntity>> listDetalleIvrTicket(@Param("ticket") Integer ticket, Pageable pageable);
    /**
     * Seteo timezone
     * **/
    @Query( value = "SET TIMEZONE=-5",
            nativeQuery = true )
    void setTimeZone();


    /**
     * Seteo timezone
     * **/
    @Query( value = "select CONCAT(current_date, ' ',current_time)",
            nativeQuery = true )
    Iterable<String> getTimeZone();

    /**
     * Consulta de datos para carga ivr
     *
     * @return*/
    @Query( value = "\n" +
            "SELECT CONCAT(\n" +
            " \tq1.\"ViabilidadTicket\" , ';',\n" +
            "\t \tq1.\"Abonado\" , ';',\n" +
            "\t \tq1.\"FechaFin\" , ';',\n" +
            "\t \tq1.\"HoraFin\" , ';',\n" +
            "\t \tq1.\"TipoDaño\" , ';',\n" +
            "\t \tq1.\"FK_IdTipologia\" , ';',\n" +
            "\t \tq1.\"Flag1\" , ';',\n" +
            "\t \tq1.\"Flag2\" , ';'\n" +
            "\t\n" +
            ") FROM (\n" +
            "SELECT \n" +
            "        DISTINCT \n" +
            "            ON (\"V2_DM\".\"IVR\".\"Abonado\") \"V2_DM\".\"IVR\".\"Abonado\",\n" +
            "            \"V2_DM\".\"IVR\".\"IdIVR\",\n" +
            "            \"V2_DM\".\"IVR\".\"FK_IdTipologia\",\n" +
            "            \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\",\n" +
            "            \"V2_DM\".\"IVR\".\"ViabilidadTicket\",\n" +
            "            \"V2_DM\".\"IVR\".\"FechaFin\",\n" +
            "            \"V2_DM\".\"IVR\".\"HoraFin\",\n" +
            "            \"V2_DM\".\"IVR\".\"TipoDaño\",\n" +
            "            \"V2_DM\".\"IVR\".\"Flag1\",\n" +
            "            \"V2_DM\".\"IVR\".\"Flag2\" \n" +
            "    FROM\n" +
            "        \"V2_DM\".\"CARGA_IVR\" \n" +
            "    INNER JOIN\n" +
            "        \"V2_DM\".\"IVR\" \n" +
            "            ON \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" = \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" \n" +
            "    WHERE\n" +
            "        (\n" +
            "            \"V2_DM\".\"IVR\".\"FechaFin\" > current_date  \n" +
            "            OR (\n" +
            "                \"V2_DM\".\"IVR\".\"FechaFin\" = current_date \n" +
            "                AND \"V2_DM\".\"IVR\".\"HoraFin\" >= current_time \n" +
            "            ) \n" +
            "        )  \n" +
            "        AND \"V2_DM\".\"CARGA_IVR\".\"Estado\"='true' \n" +
            "    ORDER BY\n" +
            "        \"V2_DM\".\"IVR\".\"Abonado\" ,\n" +
            "        \"V2_DM\".\"IVR\".\"FechaFin\",\n" +
            "        \"V2_DM\".\"IVR\".\"HoraFin\" DESC\n" +
            "\t\t) q1",
            nativeQuery = true )
    List<String> getIvrDetailExport();
}
