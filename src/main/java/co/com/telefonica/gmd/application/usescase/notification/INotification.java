package co.com.telefonica.gmd.application.usescase.notification;

import co.com.telefonica.gmd.platform.beans.Response;

public interface INotification {

    public Response get(Object[] object);
}
