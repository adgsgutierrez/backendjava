package co.com.telefonica.gmd.application.usescase.profile;

import co.com.telefonica.gmd.application.repositories.database.profile.IProfileRepository;
import co.com.telefonica.gmd.domain.profile.ProfileEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ProfileTable")
@Slf4j
public class ProfileService implements IProfile{

    @Autowired
    private IProfileRepository profileRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        List<ProfileEntity> list = profileRepository.getProfile();
        response.setData(list);
        return response;
    }
}
