package co.com.telefonica.gmd.application.repositories.database.admin.eecc;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.admin.eecc.EeccEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;

@Repository
public interface IEeccRepository extends CrudRepository<EeccEntity, Integer> {

    @Query ( value = "SELECT \"IdEECC\", \"ProveedorContratista\", \"Estado\" " +
            " FROM \"V2_DM\".\"EECC\" ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"EECC\" ",
            nativeQuery = true)
    ArrayList<EeccEntity> getListEecc();

    @Query ( value = "UPDATE \"V2_DM\".\"EECC\" SET \"Estado\" = :estado WHERE \"IdEECC\" = :ideecc RETURNING \"IdEECC\"",
            nativeQuery = true)

    BigInteger getUpdateEecc(@Param("ideecc") BigInteger ideecc, @Param("estado") Boolean estado);


}

