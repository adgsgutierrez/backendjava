package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.application.repositories.database.gravedad.IGravedadRepository;
import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("GravedadLevel")
@Slf4j
public class GravedadLevelService implements IGravedad{

    @Autowired
    private IGravedadRepository gravedadRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Integer value) {
        response.setCode(200);
        response.setMessage("OK");
        GravedadEntity gravedad =  gravedadRepository.getGravedadWithRange(value);
        response.setData(gravedad.getTipoGravedad());
        return response;
    }
}
