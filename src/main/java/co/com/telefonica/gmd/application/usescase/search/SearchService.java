package co.com.telefonica.gmd.application.usescase.search;

import co.com.telefonica.gmd.application.repositories.database.search.ISearchRepository;
import co.com.telefonica.gmd.application.usescase.ivr.IIvrService;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.domain.ivr.SearchFileQuery;
import co.com.telefonica.gmd.domain.ivr.SearchFileQueryDTO;
import co.com.telefonica.gmd.domain.search.ISearchQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service("Search")
@Slf4j
public class SearchService implements ISearch {

    @Autowired
    private ISearchRepository searchRepository;

    @Autowired
    private Response response;


    @Override
    public Response get(Object[] args) {
        String abonado = String.valueOf(args[0]);
        Integer pageNumber = Integer.parseInt((String) args[1]);
        Integer limitNumber = Integer.parseInt((String) args[2]);
        Pageable page = PageRequest.of(pageNumber, limitNumber);
        Page<Iterable<ISearchQuery>> search = searchRepository.getSearchAbonado(abonado,page);

        List<SearchFileQueryDTO> returnList = new ArrayList<>();
        for( Object item :  search.get().toArray() ){
            ISearchQuery itemSearch = (ISearchQuery) item;
            SearchFileQueryDTO itemSave = new SearchFileQueryDTO();
            String dateFormatter = FunctionsTransform.getDate( itemSearch.getFechaCarga() , "dd/MMMM/yyyy" );
            itemSave.setClientesAfectados(itemSearch.getAfectados());
            itemSave.setNombreArchivo( itemSearch.getNombreArchivo() );
            itemSave.setUsuario( itemSearch.getUsuario() );
            itemSave.setFechaCarga(dateFormatter);
            itemSave.setIdCargarIvr( new BigInteger( String.valueOf(itemSearch.getIdCargaIVR())) );
            itemSave.setEstado( Boolean.TRUE );
            returnList.add(itemSave);
        }
        response.setData(
                PageableExecutionUtils.getPage(returnList, search.getPageable(), () -> search.getTotalElements())
        );
        response.setCode(200);
        response.setMessage("Ok");

        return response;
    }
}
