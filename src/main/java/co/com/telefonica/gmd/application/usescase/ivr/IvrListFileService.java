package co.com.telefonica.gmd.application.usescase.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.domain.ivr.SearchFileQuery;
import co.com.telefonica.gmd.domain.ivr.SearchFileQueryDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Service("IvrListFile")
@Slf4j
public class IvrListFileService implements IIvrService {

    @Autowired
    private Response response;

    @Autowired
    private IIvrRepository ivrRepository;

    @Override
    public Response get(Object[] args) {
        response.setCode(200);
        response.setMessage("Success");
        Pageable page = PageRequest.of(
                Integer.parseInt((String) args[0]),
                Integer.parseInt((String) args[1])
        );
        Page<Iterable<SearchFileQuery>> listIvrFile = ivrRepository.findAll(page);
        List<SearchFileQueryDTO> returnList = new ArrayList<>();
        for( Object item :  listIvrFile.get().toArray() ){
            SearchFileQuery itemSearch = (SearchFileQuery) item;
            SearchFileQueryDTO itemSave = new SearchFileQueryDTO();
            String dateFormatter = FunctionsTransform.getDate( itemSearch.getFechaCarga() , "dd/MMMM/yyyy" );
            itemSave.setIdCargarIvr( itemSearch.getIdCargarIvr() );
            itemSave.setClientesAfectados( itemSearch.getClientesAfectados() );
            itemSave.setEstado( itemSearch.getEstado() );
            itemSave.setNombreArchivo( itemSearch.getNombreArchivo() );
            itemSave.setUsuario( itemSearch.getUsuario() );
            itemSave.setFechaCarga(dateFormatter);
            returnList.add(itemSave);
        }
        response.setData(
                PageableExecutionUtils.getPage(returnList, listIvrFile.getPageable(), () -> listIvrFile.getTotalElements())
        );

        return response;
    }
}
