package co.com.telefonica.gmd.application.usescase.dashboard.detailtypeplant;

import co.com.telefonica.gmd.application.repositories.database.dashboard.detailtypeplant.IDetailTypePlantExternalRepository;
import co.com.telefonica.gmd.application.repositories.database.dashboard.detailtypeplant.IDetailTypePlantInternalRepository;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.externalplant.IExternalPlantDetailQuery;
import co.com.telefonica.gmd.domain.internalplant.IInternalPlantaDetailQuery;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("DetailTypePlant")
@Slf4j
public class DetailTypePlantService implements IDetailTypePlant{

    @Autowired
    private Response response;

    @Autowired
    private IDetailTypePlantExternalRepository detailTypePlantExternalRepository;

    @Autowired
    private IDetailTypePlantInternalRepository detailTypePlantInternalRepository;


    @Override
    public Response get(Integer id, String type) {
        response.setCode(200);
        response.setMessage("Ok");
        log.info("Tipo plisss"+type);
        if(type.equals("PE")){
            List<IExternalPlantDetailQuery> detailEx = detailTypePlantExternalRepository.getDetailExternalPlant(id);
            response.setData(detailEx);
        }else if(type.equals("PI"))
        {
            List<IInternalPlantaDetailQuery> detailIn = detailTypePlantInternalRepository.getDetailInternalPlant(id);
            response.setData(detailIn);
            log.info("Tipo plisss"+detailIn);
        }
        return response;
    }
}
