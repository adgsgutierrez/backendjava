package co.com.telefonica.gmd.application.repositories.database.internalplant;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface IInternalPlantRepository extends CrudRepository<InternalPlantEntity, Integer> {

    @Query(
            value = "SELECT * FROM \"V2_DM\".\"PLANTA_INTERNA\" WHERE \"V2_DM\".\"PLANTA_INTERNA\".\"ViabilidadTicket\"=:ticket  ORDER BY 1 DESC limit 1",
            nativeQuery = true
    )
    InternalPlantEntity getByViabilityTicket(@Param("ticket") Integer ticket);

    @Query(
            value = "SELECT * FROM \"V2_DM\".\"PLANTA_INTERNA\" WHERE \"V2_DM\".\"PLANTA_INTERNA\".\"ViabilidadTicket\"=:ticket AND " +
                    "\"V2_DM\".\"PLANTA_INTERNA\".\"FK_IdUsuario\" =:usuario  ",
            nativeQuery = true
    )
    Optional<ArrayList<InternalPlantEntity>>  getByViabilityTicket(@Param("ticket") Integer ticket , @Param("usuario") Integer usuario);

    @Query(value = "UPDATE \"V2_DM\".\"PLANTA_INTERNA\" SET \"Estado\"=:estado " +
            "WHERE \"IdPI\"=:id RETURNING *",
            nativeQuery = true)
    Optional<InternalPlantEntity> updateViabilityTicket(@Param("id") Integer id, @Param("estado") Boolean estado);
}
