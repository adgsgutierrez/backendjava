package co.com.telefonica.gmd.application.usescase.admin.typology;

import co.com.telefonica.gmd.application.repositories.database.admin.networktype.INetworkTypeRepository;
import co.com.telefonica.gmd.application.repositories.database.admin.typology.ITypologyRepository;
import co.com.telefonica.gmd.domain.admin.typology.TypologyDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("TypologyUpdate")
@Slf4j
public class TypologyUpdateService implements ITypology{
    @Autowired
    private ITypologyRepository typologyRepository;

    @Autowired
    private Response response;


    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        TypologyDTO typologyDTO = (TypologyDTO) data;
        response.setData(
                (typologyRepository.getUpdateTypology(typologyDTO.getIdtipologia(), typologyDTO.getEstado()) !=null)
        );
        return response;
    }
}
