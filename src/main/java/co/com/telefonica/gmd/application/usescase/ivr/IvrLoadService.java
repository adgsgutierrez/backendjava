package co.com.telefonica.gmd.application.usescase.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrRepository;
import co.com.telefonica.gmd.application.usescase.notification.INotification;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.domain.notification.NotificationDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import co.com.telefonica.gmd.platform.utils.validation.Validation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service("IvrLoad")
@Slf4j
public class IvrLoadService implements IIvrService {

    @Autowired
    private Response response;

    @Autowired
    private IIvrRepository ivrRepository;

    @Autowired
    private IIvrDetailRepository ivrDetailRepository;

    @Qualifier("NotificationCreate")
    private INotification notification;

    private IvrEntity fileEntity;
    private ArrayList<IvrDetailEntity> detailEntity;

    private ExecutorService executor = Executors.newFixedThreadPool(2);

    @Override
    public Response get(Object[] args) {
        response.setCode(300);
        executor.submit(() -> {runtimeFunction(args);});
        response.setCode(200);
        response.setMessage("Cargando información");
        return response;

    }

    public void runtimeFunction(Object[] args) {
        BigInteger user = new BigInteger((String) args[1]);
        try {
            log.info("Guardando información");
            MultipartFile file = (MultipartFile) args[0];
            String nameFile = file.getOriginalFilename();
            this.saveData(file.getInputStream() , user , nameFile);
            log.info("Informacion cargada con éxito");
        } catch (IOException | ParseException e) {
            log.info("Fallo al momento de cargar el arhivo");
            log.error(e.getMessage());
            NotificationDTO notificationDTO = new NotificationDTO();
            notificationDTO.setEstado(true);
            Date date = new Date();
            notificationDTO.setFecha(new java.sql.Date(date.getTime()));
            notificationDTO.setHora(java.time.LocalTime.now());
            notificationDTO.setTipoNotificacion("ivr");
            notificationDTO.setNotificacion("Se presentó un error al momento de cargar el archivo ivr");
            notificationDTO.setIdUsuario(user.intValue());
            Object[] data = new Object[]{notificationDTO};
            notification.get(data);
        }
    }

    /**
     * Funcion para el guardado de la información del archivo
     * */
    @Async("threadPoolTaskExecutor")
    public void saveData(InputStream file, BigInteger user , String name) throws ParseException {
        fileEntity = new IvrEntity();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Bogota"));
        calendar.setTimeZone( TimeZone.getTimeZone( "ETC/GMT-5" ));
        calendar.add( Calendar.HOUR , -5);
        log.info("Calendar with ETC " + calendar.getTime().toString());
        fileEntity.setFechaCarga( calendar.getTime() );
        fileEntity.setFkIdUsuario(user);
        fileEntity.setNombreArchivo(name);
        try {
            ivrDetailRepository.setTimeZone();
        } catch (JpaSystemException e){
            log.error("Seteo de la consulta de timezone -5");
        }
        // Lectura Fichero
        InputStream inputStream = file;
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        List<String> list = br.lines().collect(Collectors.toList());
        detailEntity = new ArrayList<>();
        detailEntity = FunctionsTransform.getArrayToString(detailEntity , list);
        fileEntity.setEstado( Boolean.TRUE );
        fileEntity.setClientesAfectados( detailEntity.size() );
        IvrEntity resultFileEntity = ivrRepository.save( fileEntity);
        log.info("Informacion de archivo ["+fileEntity.toString()+"]");
        BigInteger idEntity = new BigInteger( String.valueOf(resultFileEntity.getIdCargarIvr()));
        for ( IvrDetailEntity ivrDetailEntity : detailEntity) {
            ivrDetailEntity.setFkIdCargaIvr( idEntity );
        }
        ivrDetailRepository.saveAll(detailEntity);
    }

}
