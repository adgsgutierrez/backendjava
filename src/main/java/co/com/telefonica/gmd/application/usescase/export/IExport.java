package co.com.telefonica.gmd.application.usescase.export;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Map;

public interface IExport {

    XSSFWorkbook getWorkbook(Map<String , Object> paramsFilter);
}
