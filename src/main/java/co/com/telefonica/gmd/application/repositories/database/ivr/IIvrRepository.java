package co.com.telefonica.gmd.application.repositories.database.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.domain.ivr.IvrEntity;
import co.com.telefonica.gmd.domain.ivr.SearchFileQuery;
import co.com.telefonica.gmd.domain.ivr.SearchIdCargaIVR;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface IIvrRepository extends CrudRepository<IvrEntity, BigInteger> {

    @Query(
            value = "SELECT \"IdCargaIVR\" as \"idCargarIvr\", \"Nombres\" as \"Usuario\", \"NombreArchivo\", \"FechaCarga\", \"ClientesAfectados\", \"V2_DM\".\"CARGA_IVR\".\"Estado\" " +
                    "FROM \"V2_DM\".\"CARGA_IVR\" " +
                    "INNER JOIN \"V2_DM\".\"USUARIOS\" ON \"V2_DM\".\"CARGA_IVR\".\"FK_IdUsuario\" = \"V2_DM\".\"USUARIOS\".\"IdUsuario\" " +
                    "WHERE \"V2_DM\".\"CARGA_IVR\".\"Estado\" = 'true' ORDER BY 1 desc",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"CARGA_IVR\" WHERE \"V2_DM\".\"CARGA_IVR\".\"Estado\" = 'true';",
            nativeQuery = true
    )
    Page<Iterable<SearchFileQuery>> findAll(Pageable page);


    @Query(
            value = "SELECT DISTINCT \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" " +
                    "FROM \"V2_DM\".\"CARGA_IVR\" INNER JOIN \"V2_DM\".\"IVR\" " +
                    "ON \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\" = \"V2_DM\".\"IVR\".\"FK_IdCargaIVR\" " +
                    "WHERE \"V2_DM\".\"IVR\".\"ViabilidadTicket\" = :viabilidad",
            nativeQuery = true
    )
    Optional<ArrayList<SearchIdCargaIVR>> getListDamageByViability(@Param("viabilidad") Integer viabilidad );

    @Query(value = "UPDATE \"V2_DM\".\"CARGA_IVR\" SET \"Estado\"=:estado " +
            "WHERE \"V2_DM\".\"CARGA_IVR\".\"IdCargaIVR\"=:id RETURNING *",
            nativeQuery = true)
    Optional<SearchIdCargaIVR> updateViabilityTicket(@Param("id") Integer id, @Param("estado") String estado);
}
