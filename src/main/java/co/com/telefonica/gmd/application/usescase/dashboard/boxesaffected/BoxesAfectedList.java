package co.com.telefonica.gmd.application.usescase.dashboard.boxesaffected;

import co.com.telefonica.gmd.domain.dashboard.boxesaffected.BoxesAffectedDTO;
import co.com.telefonica.gmd.domain.dashboard.boxesaffected.BoxesAffectedEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.OracleCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Service("BoxesAffectedList")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class BoxesAfectedList implements IBoxesAffected {

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${query.view.boxes}")
    private String queryBoxes;

    @Autowired
    private Response response;

    @Override
    public Response getBoxesAffected(Map<String, Object> paramsFilter) {
        String[] keysArray = paramsFilter.keySet().toArray(new String[0]);
        StringBuilder queryString = new StringBuilder(queryBoxes);
        Codec ORACLE_CODEC = new OracleCodec();
        for ( String key: keysArray) {
            queryString.append( "CAST ( \"");
            queryString.append(ESAPI.encoder().encodeForSQL(ORACLE_CODEC , key ));
            queryString.append("\" AS TEXT) IN(:");
            queryString.append(ESAPI.encoder().encodeForSQL(ORACLE_CODEC , key ).toLowerCase() +") ");
        }
        Query query = entityManager
                .createNativeQuery(
                        org.apache.commons.lang.StringEscapeUtils.escapeSql(queryString.toString()
                        ),  BoxesAffectedEntity.class);
        for (String key : keysArray)  {
            List<String> params = Arrays.asList(("" + paramsFilter.get(key)).split(","));
            query.setParameter(key.toLowerCase(), params);
        }
        List<Object> results =  query.getResultList();
        response.setCode(200);
        response.setMessage("Success");
        response.setData(results);
        return response;
    }
}
