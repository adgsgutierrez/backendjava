package co.com.telefonica.gmd.application.usescase.dashboard.table;

import co.com.telefonica.gmd.domain.dashboard.table.TableListQueryDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Julio 2021
 * Author: Everis GDM
 * All rights reserved
 */
@Service("TableList")
@Slf4j
public class TableListService implements ITable {

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${query.view.dashboard}")
    private String queryFilter;
    @Value("${query.count.dashboard}")
    private String queryCountFilter;

    @Autowired
    private Response response;

    @SneakyThrows
    @Override
    public Response get(Object data){
        HashMap<String, Object> list = (HashMap<String, Object>) data;
        response.setCode(200);
        response.setMessage("Success");

        Integer pages = (list.get("page") != null)? Integer.parseInt( String.valueOf(list.get("page"))) : 0 ;
        Integer items = (list.get("items") != null)? Integer.parseInt( String.valueOf(list.get("items"))) : 5 ;
        Pageable page = PageRequest.of(pages , items );
        Map<String , Object> newList = new HashMap<>();
        String[] keys = list.keySet().toArray(new String[0]);
        for(String key : keys){
            if(!key.equals("page") && !key.equals("items")) {
                StringBuilder keyFilter = new StringBuilder("\"V2_DM\".\"V_TablaDashboard\"");
                keyFilter.append(".\"")
                        .append(key)
                        .append("\"");
                newList.put(keyFilter.toString() , list.get(key));
            }
        }
        StringBuilder queryString = new StringBuilder(queryFilter);
        queryString = (newList.size() > 0) ? queryString.append(" WHERE ") :  queryString;
        log.info("Query " + queryString);
        Query query = FunctionsTransform.getQueryBuilderString( queryString.toString(), entityManager , newList , Boolean.FALSE);
        log.info("Query " + query.toString() );
        query.setFirstResult((int) page.getOffset());
        query.setMaxResults( page.getPageSize());
        List tableResult = query.getResultList();

        List<TableListQueryDTO> tableResponse = new ArrayList<>();
        SimpleDateFormat formatHours = new SimpleDateFormat("dd/MMMM/yyyy hh:mm:ss");

        for ( Object row : tableResult ) {
            Object[] celd = (Object[]) row;
            TableListQueryDTO dto = new TableListQueryDTO();
            Calendar calendar = Calendar.getInstance(); // creates a new calendar instance
            calendar.setTime((Date) celd[1]);
            //String dateFormatter = FunctionsTransform.getDate((Date) celd[1], "dd/MMMM/yyyy hh:mm:ss" );
            String dateFormatter = formatHours.format(calendar.getTime());
            dto.setFecha(dateFormatter);
            dto.setEstado((Boolean) celd[12]);
            dto.setDepartamento((String) celd[2]);
            dto.setLocalidad((String) celd[3]);
            dto.setDistribuidor((String) celd[5]);
            dto.setElemento((String) celd[7]);
            dto.setDiagnostico((String) celd[4]);
            dto.setUsuario((String) celd[8]);
            dto.setAfectados((Integer) celd[9]);
            dto.setGravedad((String) celd[10]);
            dto.setNombreElemento((String) celd[6]);
            dto.setTipoPlanta((String) celd[11]);
            dto.setId((Integer) celd[0]);
            tableResponse.add(dto);
        }
        log.info("queryFilter "+ queryCountFilter);
        StringBuilder queryCountString = new StringBuilder(queryCountFilter);
        queryCountString = (newList.size() > 0) ? queryCountString.append(" WHERE ") :  queryCountString;
        Query queryCount = FunctionsTransform.getQueryBuilderString( queryCountString.toString(), entityManager , newList , Boolean.FALSE);
        BigInteger result = (BigInteger) queryCount.getResultList().get(0);
        log.info("Result "+ result);
        response.setData(PageableExecutionUtils.getPage(tableResponse, page, () -> result.intValue()));
        return response;
    }
}
