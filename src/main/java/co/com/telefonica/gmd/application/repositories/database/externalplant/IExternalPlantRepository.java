package co.com.telefonica.gmd.application.repositories.database.externalplant;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface IExternalPlantRepository extends CrudRepository<ExternalPlantEntity, Integer> {

    @Query(
            value = "SELECT * FROM \"V2_DM\".\"PLANTA_EXTERNA\" WHERE \"V2_DM\".\"PLANTA_EXTERNA\".\"ViabilidadTicket\"=:ticket ORDER BY 1 DESC limit 1",
            nativeQuery = true
    )
    ExternalPlantEntity getByViabilityTicket(@Param("ticket") Integer ticket);

    @Query(
            value = "SELECT * FROM \"V2_DM\".\"PLANTA_EXTERNA\" \n" +
                    "WHERE \"V2_DM\".\"PLANTA_EXTERNA\".\"ViabilidadTicket\"=:ticket AND\n" +
                    "\"V2_DM\".\"PLANTA_EXTERNA\".\"FK_IdUsuario\" =:usuario  ",
            nativeQuery = true
    )
    Optional< ArrayList<ExternalPlantEntity> > getByViabilityTicket(@Param("ticket") Integer ticket , @Param("usuario") Integer usuario);

    @Query(value = "UPDATE \"V2_DM\".\"PLANTA_EXTERNA\" SET \"Estado\"=:estado " +
            "WHERE \"IdPE\"=:id RETURNING *",
            nativeQuery = true)
    Optional<ExternalPlantEntity> updateViabilityTicket(@Param("id") Integer id, @Param("estado") Boolean estado);
}
