package co.com.telefonica.gmd.application.repositories.database.dashboard.detailtypeplant;

import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.domain.externalplant.IExternalPlantDetailQuery;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IDetailTypePlantExternalRepository extends CrudRepository<ExternalPlantEntity, Integer> {

    @Query( value = "SELECT PE.\"IdPE\", PE.\"Departamento\", PE.\"Localidad\", PE.\"Distribuidor\", PE.\"ArmarioCable\", PE.\"CajasAfectadas\", " +
            "PC.\"ProveedorContratista\", MA.\"MediosAcceso\", Tipo.\"Tipologia\", TR.\"TipoRed\", " +
            "PE.\"FechaHoraRegistro\", PE.\"TecnicoReporta\", PE.\"CelularTecnico\", PE.\"DesFalla\", PE.\"ClientesAfectados\", " +
            "PE.\"ViabilidadTicket\", PE.\"Estado\", PE.\"Barrio\", PE.\"Direccion\" " +
            "FROM \"V2_DM\".\"PLANTA_EXTERNA\" AS PE " +
            "INNER JOIN \"V2_DM\".\"EECC\" AS PC ON PE.\"FK_IdEECC\" = PC.\"IdEECC\" " +
            "INNER JOIN \"V2_DM\".\"MEDIOS_ACCESO\" AS MA ON PE.\"FK_IdMA\" = MA.\"IdMA\" " +
            "INNER JOIN \"V2_DM\".\"TIPOLOGIA\" AS Tipo ON PE.\"FK_IdTipologia\" = Tipo.\"IdTipologia\" " +
            "INNER JOIN \"V2_DM\".\"TIPO_RED\" AS TR ON PE.\"FK_IdTipoRed\" = TR.\"IdTipoRed\" " +
            "WHERE PE.\"IdPE\" = :idPE",
    nativeQuery = true)

    ArrayList<IExternalPlantDetailQuery> getDetailExternalPlant(@Param("idPE") Integer idPe);

}
