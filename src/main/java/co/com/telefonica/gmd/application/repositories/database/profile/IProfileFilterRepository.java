package co.com.telefonica.gmd.application.repositories.database.profile;

import co.com.telefonica.gmd.domain.profile.ProfileFilterDTO;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IProfileFilterRepository extends CrudRepository<UsuariosEntity, Integer> {

    @Query( value = "SELECT \"IdUsuario\", \"Perfiles\", \"Nombres\", \"Estado\", \"Email\", \"UsuarioRed\"\n" +
            "FROM \"V2_DM\".\"USUARIOS\" as U\n" +
            "Inner Join \"V2_DM\".\"PERFILES\" as P on U.\"FK_IdPerfiles\" = P.\"IdPerfiles\"\n" +
            "where \"FK_IdPerfiles\" = :id ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"USUARIOS\" WHERE \"V2_DM\".\"IVR\".\"FK_IdPerfiles\" =:id",
            nativeQuery = true)
    Page<Iterable<ProfileFilterDTO>> getFilterProfile(@Param("id") Integer id, Pageable pageable);

}
