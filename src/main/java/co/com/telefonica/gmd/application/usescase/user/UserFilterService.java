package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserDetailRepository;
import co.com.telefonica.gmd.application.repositories.database.user.IUserRepository;
import co.com.telefonica.gmd.domain.user.IUserFilterProfile;
import co.com.telefonica.gmd.domain.user.UserAllQeury;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.HashMap;

@Service("UserFilter")
@Slf4j
public class UserFilterService implements IUserFilter{

    @Autowired
    private IUserDetailRepository userDetailRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] data) {
        Integer id = new Integer((Integer) data[0]);
        Integer pageNumber = Integer.parseInt((String) data[1]);
        Integer limitNumber = Integer.parseInt((String) data[2]);
        Pageable page = PageRequest.of(pageNumber, limitNumber);
        response.setCode(200);
        response.setMessage("Ok");
        if(id == 0) {
            Page<Iterable<UserAllQeury>> listUsers = userRepository.getlist(page);
            response.setData(listUsers);
        } else{
            Page<Iterable<IUserFilterProfile>> listPerfil = userDetailRepository.getFilterProfileUser(id, page);
            response.setData(listPerfil);
        }
        return response;
    }
}
