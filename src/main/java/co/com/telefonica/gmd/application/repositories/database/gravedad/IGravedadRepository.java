package co.com.telefonica.gmd.application.repositories.database.gravedad;

import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;

@Repository
public interface IGravedadRepository extends CrudRepository<GravedadEntity, Integer> {

    @Query( value = "SELECT \"IdGravedad\", \"Desde\", \"Hasta\", \"TipoGravedad\" " +
                    " FROM \"V2_DM\".\"GRAVEDAD\" WHERE :value BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\" ",
            nativeQuery = true
    )
    GravedadEntity getGravedadWithRange(@Param("value") Integer value);

    @Query(value = "SELECT * FROM \"V2_DM\".\"GRAVEDAD\" ", nativeQuery = true)

    ArrayList<GravedadEntity> getTableGravity();

    @Query( value = "UPDATE \"V2_DM\".\"GRAVEDAD\"" +
            "    SET \"Desde\" = :desde, \"Hasta\" = :hasta" +
            "    WHERE \"IdGravedad\"= :idGravedad RETURNING \"IdGravedad\" ",
    nativeQuery = true)

    Integer getUpdateGravedad(@Param("idGravedad") Integer idGravedad, @Param("desde") Integer desde, @Param("hasta") Integer hasta);
}
