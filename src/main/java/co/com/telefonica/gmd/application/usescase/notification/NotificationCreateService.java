package co.com.telefonica.gmd.application.usescase.notification;

import co.com.telefonica.gmd.application.repositories.database.notification.INotificationRepository;
import co.com.telefonica.gmd.domain.notification.NotificationDTO;
import co.com.telefonica.gmd.domain.notification.NotificationEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@Service("NotificationCreate")
@Slf4j
public class NotificationCreateService implements INotification {

    @Autowired
    private INotificationRepository notificationRepository;

    @Autowired
    private Response response;

    private ArrayList<NotificationEntity> detailEntityObject;

    @Override
    public Response get(Object[] data) {
        response.setCode(200);
        response.setMessage("OK");
        detailEntityObject = new ArrayList<>();
        NotificationDTO notificationDTO = (NotificationDTO) data[0];
        NotificationEntity detailEntityObject = new NotificationEntity();
        detailEntityObject.setIdUsuario(notificationDTO.getIdUsuario());
        detailEntityObject.setNotification(notificationDTO.getNotificacion());
        detailEntityObject.setTipoNotificacion(notificationDTO.getTipoNotificacion());
        detailEntityObject.setEstado(notificationDTO.getEstado());
        detailEntityObject.setFecha(notificationDTO.getFecha());
        detailEntityObject.setHora(notificationDTO.getHora());
        notificationRepository.save(detailEntityObject);
        return response;
    }
}
