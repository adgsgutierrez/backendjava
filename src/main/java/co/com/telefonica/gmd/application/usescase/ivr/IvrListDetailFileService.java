package co.com.telefonica.gmd.application.usescase.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service("IvrListDetailFile")
@Slf4j
public class IvrListDetailFileService implements IIvrService {

    @Autowired
    private Response response;

    @Autowired
    private IIvrDetailRepository ivrDetailRepository;

    @Override
    public Response get(Object[] args) {
        BigInteger file = new BigInteger((String) args[0]);
        Integer pageNumber = Integer.parseInt((String) args[1]);
        Integer limitNumber = Integer.parseInt((String) args[2]);
        try {
            ivrDetailRepository.setTimeZone();
        } catch (JpaSystemException e){
            log.error("Seteo de la consulta de timezone -5");
        }
        Pageable page = PageRequest.of(pageNumber, limitNumber);
        Page<Iterable<IvrDetailEntity>> listDetailFile = ivrDetailRepository.listDetailFile( file , page);
        response.setCode(200);
        response.setMessage("Success");
        response.setData(listDetailFile);
        return response;
    }
}
