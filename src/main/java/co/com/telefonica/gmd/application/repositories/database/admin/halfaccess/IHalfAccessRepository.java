package co.com.telefonica.gmd.application.repositories.database.admin.halfaccess;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;

@Repository
public interface IHalfAccessRepository extends CrudRepository<HalfAccessEntity, Integer> {

    @Query ( value = "SELECT \"IdMA\" , \"MediosAcceso\", \"Estado\"" +
            "FROM \"V2_DM\".\"MEDIOS_ACCESO\" ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"MEDIOS_ACCESO\" ",
            nativeQuery = true)
    ArrayList<HalfAccessEntity> getListHalfAccess();

    @Query ( value = "UPDATE \"V2_DM\".\"MEDIOS_ACCESO\" SET \"Estado\" = :estado WHERE \"IdMA\" = :idma RETURNING \"IdMA\"",
            nativeQuery = true)

    BigInteger getUpdateHalfAccess(@Param("idma") BigInteger ideecc, @Param("estado") Boolean estado);
}
