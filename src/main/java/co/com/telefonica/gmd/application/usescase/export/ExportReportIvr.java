package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("ExportReportIvr")
@Slf4j
public class ExportReportIvr implements IExportOtherFile {

    @Autowired
    private ObjectToExcel export;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${query.view.ivr}")
    private String queryIvr;

    @SneakyThrows
    @Override
    public List<String> getWorkbook(Map<String, Object> paramsFilter) {
        Query query = FunctionsTransform.getQueryBuilderString(queryIvr , entityManager , paramsFilter);
        query.setParameter("estado" , "true");
        List<Object[]> results = query.getResultList();
        ArrayList<String> dataExport = FunctionsTransform.getDataResultString( new Integer[]{0,1,2,3} , results ) ;

        return dataExport;
    }

}
