package co.com.telefonica.gmd.application.repositories.database.conteogravedad;

import co.com.telefonica.gmd.domain.conteogravedad.IConteoGravedadQuery;
import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface IConteoGravedadRepository extends CrudRepository<GravedadEntity, BigInteger> {

    @Query( value = "SELECT ( (SELECT count (PE.\"ClientesAfectados\") AS \"CGCritica\" " +
            "FROM \"V2_DM\".\"PLANTA_EXTERNA\" as PE " +
            "WHERE (Select \"V2_DM\".\"GRAVEDAD\".\"IdGravedad\" as \"Severity\" " +
            "    FROM \"V2_DM\".\"GRAVEDAD\" " +
            "    WHERE PE.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\") = 3 AND PE.\"Estado\"='true') " +
            "+ " +
            "(SELECT count (PI.\"ClientesAfectados\") AS \"CGCritica\" " +
            "FROM \"V2_DM\".\"PLANTA_INTERNA\" as PI " +
            "WHERE (Select \"V2_DM\".\"GRAVEDAD\".\"IdGravedad\" as \"Severity\" " +
            "    FROM \"V2_DM\".\"GRAVEDAD\" " +
            "    WHERE PI.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\") = 3 AND PI.\"Estado\"='true')) AS \"ConteoGravedad\" ",
            nativeQuery = true)

    IConteoGravedadQuery getConteoCritica();

    @Query(value = "SELECT ( (SELECT count (PE.\"ClientesAfectados\") AS \"CGMa\" " +
            "FROM \"V2_DM\".\"PLANTA_EXTERNA\" as PE " +
            "WHERE (Select \"V2_DM\".\"GRAVEDAD\".\"IdGravedad\" as \"Severity\" " +
            "    FROM \"V2_DM\".\"GRAVEDAD\" " +
            "    WHERE PE.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\") = 2 AND PE.\"Estado\"='true') " +
            "+ " +
            "(SELECT count (PI.\"ClientesAfectados\") AS \"CGMa\" " +
            "FROM \"V2_DM\".\"PLANTA_INTERNA\" as PI " +
            "WHERE (Select \"V2_DM\".\"GRAVEDAD\".\"IdGravedad\" as \"Severity\" " +
            "    FROM \"V2_DM\".\"GRAVEDAD\" " +
            "    WHERE PI.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\") = 2 AND PI.\"Estado\"='true')) AS \"ConteoGravedad\" ",
            nativeQuery = true)

    IConteoGravedadQuery getConteoMayor();

    @Query(value = "SELECT ( (SELECT count (PE.\"ClientesAfectados\") AS \"CGMe\" " +
            "FROM \"V2_DM\".\"PLANTA_EXTERNA\" as PE " +
            "WHERE (Select \"V2_DM\".\"GRAVEDAD\".\"IdGravedad\" as \"Severity\" " +
            "    FROM \"V2_DM\".\"GRAVEDAD\" " +
            "    WHERE PE.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\") = 1 AND PE.\"Estado\"='true') " +
            "+ " +
            "(SELECT count (PI.\"ClientesAfectados\") AS \"CGMe\" " +
            "FROM \"V2_DM\".\"PLANTA_INTERNA\" as PI " +
            "WHERE (Select \"V2_DM\".\"GRAVEDAD\".\"IdGravedad\" as \"Severity\" " +
            "    FROM \"V2_DM\".\"GRAVEDAD\"  " +
            "    WHERE PI.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\") = 1 AND PI.\"Estado\"='true')) AS \"ConteoGravedad\" ",
            nativeQuery = true)

    IConteoGravedadQuery getConteoMenor();

    @Query(value = "SELECT ((SELECT count(PE.\"Estado\") AS \"ConteoActivoPE\" FROM \"V2_DM\".\"PLANTA_EXTERNA\" AS PE " +
            "WHERE PE.\"Estado\"= true) " +
            "+ " +
            "(SELECT count(PI.\"Estado\") AS \"ConteoActivoPI\" FROM \"V2_DM\".\"PLANTA_INTERNA\" AS PI " +
            "WHERE PI.\"Estado\"= true)) AS \"ConteoGravedad\" ",
            nativeQuery = true)
    IConteoGravedadQuery getConteoActivos();

    @Query(value = "SELECT( (select count(PE.\"Estado\") AS \"ConteoInactivoPE\" FROM \"V2_DM\".\"PLANTA_EXTERNA\" AS PE " +
            "WHERE PE.\"Estado\"= false) " +
            "+ " +
            "(SELECT count(PI.\"Estado\") AS \"ConteoInactivoPI\" FROM \"V2_DM\".\"PLANTA_INTERNA\" AS PI " +
            "WHERE PI.\"Estado\"= false)) as \"ConteoGravedad\" ",
            nativeQuery = true)
    IConteoGravedadQuery getConteoInactivos();


    @Query(value = "SELECT count(PE.\"Estado\") AS \"ConteoGravedad\" FROM \"V2_DM\".\"PLANTA_EXTERNA\" AS PE " +
            "WHERE PE.\"Estado\"= true ",
            nativeQuery = true)
    IConteoGravedadQuery getConteoPe();

    @Query(value = "SELECT count(PI.\"Estado\") AS \"ConteoGravedad\" FROM \"V2_DM\".\"PLANTA_INTERNA\" AS PI " +
            "WHERE PI.\"Estado\"= true ",
            nativeQuery = true)
    IConteoGravedadQuery getConteoPi();



}
