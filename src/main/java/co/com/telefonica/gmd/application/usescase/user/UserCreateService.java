package co.com.telefonica.gmd.application.usescase.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.user.IUserRepository;
import co.com.telefonica.gmd.domain.user.UserCreateDTO;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("UserCreate")
@Slf4j
public class UserCreateService implements IUser {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        UserCreateDTO userCreate = (UserCreateDTO) data;
        Integer values = userRepository.getExistUser(userCreate.getUsuarioRed());
        if(values == 0) {
            UsuariosEntity userEntity = new UsuariosEntity();
            userEntity.setUsuarioRed(userCreate.getUsuarioRed());
            userEntity.setEmail(userCreate.getEmail());
            userEntity.setEstado(userCreate.getEstado());
            userEntity.setNombres(userCreate.getNombres());
            userEntity.setFkIdPerfiles(userCreate.getPerfil());
            response.setData((userRepository.save(userEntity) != null));
        } else {
            response.setCode(300);
            response.setMessage("El usuario ya existe");
        }
        return response;
    }
}
