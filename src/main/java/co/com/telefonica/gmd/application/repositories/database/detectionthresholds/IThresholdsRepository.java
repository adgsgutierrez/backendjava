package co.com.telefonica.gmd.application.repositories.database.detectionthresholds;

import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IThresholdsRepository extends CrudRepository<ThresholdsEntity, Integer> {

    @Query(value = "SELECT \"IdUmbrales\", \"FK_IdElemento\", \"PorcConexion\", \"PorcDesconexion\", \"VolDesconexion\", \"VentanaTiempo\" " +
            "\tFROM \"V2_DM\".\"UMBRALES\" where \"FK_IdElemento\" = :idElemento ",
    nativeQuery = true)
    ArrayList<ThresholdsEntity> getThresholds(@Param("idElemento") Integer idElemento);

    @Query( value = "UPDATE \"V2_DM\".\"UMBRALES\" " +
            "SET \"PorcConexion\"= :porcConexion, \"PorcDesconexion\"= :porcDesconexion, \"VolDesconexion\"= :volDesconexion, \"VentanaTiempo\"=:ventanaTiempo " +
            "WHERE \"FK_IdElemento\" =:idElement RETURNING \"FK_IdElemento\"", nativeQuery = true)
    Integer updateElements(
            @Param("idElement") Integer idElement,
            @Param("porcConexion") Integer porcConexion,
            @Param("porcDesconexion") Integer porcDesconexion,
            @Param("volDesconexion") Integer volDesconexion,
            @Param("ventanaTiempo") Integer ventanaTimepo );
}
