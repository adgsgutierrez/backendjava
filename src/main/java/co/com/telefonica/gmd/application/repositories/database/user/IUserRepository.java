package co.com.telefonica.gmd.application.repositories.database.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.user.UserAllQeury;
import co.com.telefonica.gmd.domain.user.UsuariosEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface IUserRepository extends CrudRepository<UsuariosEntity, Integer> {

    @Query( value = "SELECT  \"IdUsuario\", \"Perfiles\", \"Nombres\", \"Estado\" as Estado, \"Email\" as Email, \"UsuarioRed\" " +
                    " FROM \"V2_DM\".\"USUARIOS\" as U " +
                    " INNER JOIN \"V2_DM\".\"PERFILES\" as PE ON U.\"FK_IdPerfiles\" = PE.\"IdPerfiles\" ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"USUARIOS\" ",
            nativeQuery = true )
    Page<Iterable<UserAllQeury>> getlist(Pageable pageable);

    @Query ( value = "UPDATE \"V2_DM\".\"USUARIOS\" " +
            " SET \"FK_IdPerfiles\" = :fkIdPerfiles, \"Nombres\" = :nombres, \"Estado\" = :estado, \"Email\"= :email, \"UsuarioRed\"= :usuarioRed " +
            " WHERE \"IdUsuario\" = :idUsuario RETURNING \"IdUsuario\"",
            nativeQuery = true)

    Integer getUpdateUser(
            @Param("idUsuario") Integer idUsuario,
            @Param("fkIdPerfiles") Integer fkIdPerfiles,
            @Param("nombres") String nombres,
            @Param("estado") Boolean estado,
            @Param("email") String email,
            @Param("usuarioRed") String usuarioRed
            );

    @Query(
            value = "SELECT COUNT(*) FROM \"V2_DM\".\"USUARIOS\" WHERE  \"UsuarioRed\"= :usuarioRed",
            nativeQuery = true
    )
    Integer getExistUser(@Param("usuarioRed") String usuarioRed);

}
