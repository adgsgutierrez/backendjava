package co.com.telefonica.gmd.application.repositories.database.detectionthresholds;

import co.com.telefonica.gmd.domain.detectionthresholds.ElementsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IElementsRepository extends CrudRepository<ElementsEntity, Integer> {

    @Query( value = "SELECT * FROM \"V2_DM\".\"ELEMENTO\" ", nativeQuery = true)
    ArrayList<ElementsEntity> getElements();

}
