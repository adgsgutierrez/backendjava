package co.com.telefonica.gmd.application.usescase.dashboard.department;

import co.com.telefonica.gmd.application.repositories.database.dashboard.department.IDepartmentRepository;
import co.com.telefonica.gmd.domain.dashboard.department.DepartmentDTO;
import co.com.telefonica.gmd.domain.dashboard.department.DepartmentEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service("DepartmentList")
public class DepartmentListService implements IDepartment {

    @Autowired
    private IDepartmentRepository departmentRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        HashMap<String, String> list = (HashMap<String, String>) data;
        response.setCode(200);
        response.setMessage("Success");
        List<DepartmentEntity> departmentEntity = departmentRepository.getListDepartment(list);
        response.setData(departmentEntity);
        return response;
    }
}
