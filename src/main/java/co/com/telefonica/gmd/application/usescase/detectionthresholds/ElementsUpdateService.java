package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IElementsRepository;
import co.com.telefonica.gmd.application.repositories.database.detectionthresholds.IThresholdsRepository;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdDTO;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdsEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("ThresholdUpdate")
@Slf4j
public class ElementsUpdateService implements IThresholds {

    @Autowired
    private IThresholdsRepository thresoldRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object[] data) {
        ThresholdDTO thresholdDTO = (ThresholdDTO) data[0];
        response.setCode(200);
        response.setMessage("OK");
        Optional<ThresholdsEntity> thresholdsEntity = thresoldRepository.findById(thresholdDTO.getIdElemento());
        if(thresholdDTO.getPorcConexion() != null){
            thresholdsEntity.get().setPorcConexion(thresholdDTO.getPorcConexion());
        }
        if(thresholdDTO.getPorcDesconexion() != null){
            thresholdsEntity.get().setPorcDesconexion(thresholdDTO.getPorcDesconexion());
        }
        if(thresholdDTO.getVolDesconexion() != null){
            thresholdsEntity.get().setVolDesconexion(thresholdDTO.getVolDesconexion());
        }
        if(thresholdDTO.getVentanaTiempo() != null){
            thresholdsEntity.get().setVentanaTiempo(thresholdDTO.getVentanaTiempo());
        }
        thresoldRepository.updateElements(
                thresholdsEntity.get().getFK_idElemento(),
                thresholdsEntity.get().getPorcConexion(),
                thresholdsEntity.get().getPorcDesconexion(),
                thresholdsEntity.get().getVolDesconexion(),
                thresholdsEntity.get().getVentanaTiempo()
        );
        return response;
    }
}
