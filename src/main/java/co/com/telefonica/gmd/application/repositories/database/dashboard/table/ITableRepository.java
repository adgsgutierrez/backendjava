package co.com.telefonica.gmd.application.repositories.database.dashboard.table;

import co.com.telefonica.gmd.domain.dashboard.table.TableListQuery;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITableRepository extends CrudRepository<ExternalPlantEntity, String> {

    @Query( value  = "SELECT * FROM (SELECT  PE.\"IdPE\" as \"ID\",PE.\"FechaHoraRegistro\" as \"Fecha\", PE.\"Departamento\", PE.\"Localidad\" , " +
            "PE.\"DesFalla\" as \"Diagnostico\", PE.\"Distribuidor\", " +
            "CASE " +
            " WHEN PE.\"ArmarioCable\" = ' ' THEN 'Distribuidor' " +
            " WHEN PE.\"CajasAfectadas\" = ' ' THEN 'Distribuidor/ArmarioCable' " +
            " ELSE 'Distribuidor/ArmarioCable/CajasAfectadas' " +
            " END AS \"nombreElemento\", " +
            "CASE " +
            " WHEN PE.\"ArmarioCable\" = ' ' THEN PE.\"Distribuidor\" " +
            " WHEN PE.\"CajasAfectadas\" = ' ' THEN CONCAT( PE.\"Distribuidor\",PE.\"ArmarioCable\") " +
            " ELSE coalesce(PE.\"Distribuidor\"||'/'||PE.\"ArmarioCable\"||'/'||PE.\"CajasAfectadas\") " +
            " END AS \"Elemento\", " +
            "U.\"Nombres\" as \"Usuario\", PE.\"ClientesAfectados\" as \"Afectados\", " +
            "(Select \"V2_DM\".\"GRAVEDAD\".\"TipoGravedad\" as \"gravedad\" " +
            "FROM \"V2_DM\".\"GRAVEDAD\" WHERE PE.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\"), " +
            "'PE' as \"tipoPlanta\", " +
            "PE.\"Estado\" " +
            "FROM \"V2_DM\".\"PLANTA_EXTERNA\" as PE " +
            "INNER JOIN \"V2_DM\".\"USUARIOS\" as U ON PE.\"FK_IdUsuario\" = U.\"IdUsuario\" " +
            "UNION ALL " +
            "SELECT  PI.\"IdPI\" as \"ID\",PI.\"FechaHoraRegistro\" as \"Fecha\", PI.\"Departamento\", PI.\"Localidad\", " +
            "PI.\"DesFalla\" as \"Diagnostico\", PI.\"Distribuidor\", 'Distribuidor' as \"nombreElemento\", " +
            "PI.\"Distribuidor\" AS \"Elemento\", U.\"Nombres\" as \"Usuario\", PI.\"ClientesAfectados\" as \"Afectados\", " +
            "(Select \"V2_DM\".\"GRAVEDAD\".\"TipoGravedad\" as \"gravedad\" " +
            "FROM \"V2_DM\".\"GRAVEDAD\" WHERE PI.\"ClientesAfectados\" BETWEEN \"V2_DM\".\"GRAVEDAD\".\"Desde\" AND \"V2_DM\".\"GRAVEDAD\".\"Hasta\"), " +
            "'PI' as \"Tipo de Planta\", " +
            "PI.\"Estado\" " +
            "FROM \"V2_DM\".\"PLANTA_INTERNA\" as PI " +
            "INNER JOIN \"V2_DM\".\"USUARIOS\" as U ON PI.\"FK_IdUsuario\" = U.\"IdUsuario\") AS QTD " +
            "ORDER BY 2 desc ",
            nativeQuery = true,
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"PLANTA_EXTERNA\" AND SELECT COUNT(*) FROM \"V2_DM\".\"PLANTA_INTERNA\" "
    )
    Page<TableListQuery> getTableData(Pageable pageable);
}






