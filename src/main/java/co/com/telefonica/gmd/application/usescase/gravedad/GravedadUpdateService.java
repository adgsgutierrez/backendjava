package co.com.telefonica.gmd.application.usescase.gravedad;

import co.com.telefonica.gmd.application.repositories.database.gravedad.IGravedadRepository;
import co.com.telefonica.gmd.domain.gravedad.GravedadDTO;
import co.com.telefonica.gmd.domain.gravedad.GravedadEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("GravedadUpdate")
@Slf4j
public class GravedadUpdateService implements IGravedaTable{

    @Autowired
    private IGravedadRepository gravedadRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        GravedadDTO gravedadDTO = (GravedadDTO) data;
        Optional<GravedadEntity> gravedadEntity = gravedadRepository.findById(gravedadDTO.getIdGravedad());
       if(gravedadDTO.getDesde() != null){
           gravedadEntity.get().setDesde(gravedadDTO.getDesde());
       }
       if(gravedadDTO.getHasta() != null){
           gravedadEntity.get().setHasta(gravedadDTO.getHasta());
       }
       gravedadRepository.getUpdateGravedad(
               gravedadEntity.get().getIdGravedad(),
               gravedadEntity.get().getDesde(),
               gravedadEntity.get().getHasta()
       );
        return response;
    }
}
