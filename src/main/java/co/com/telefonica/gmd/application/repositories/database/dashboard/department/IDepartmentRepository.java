package co.com.telefonica.gmd.application.repositories.database.dashboard.department;

import co.com.telefonica.gmd.domain.dashboard.department.DepartmentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public interface IDepartmentRepository extends CrudRepository<DepartmentEntity, Integer> {

    @Query(value = "SELECT \"IdDpto\", \"Departamento\" "  +
            " FROM \"V2_DM\".\"DEPARTAMENTO\" ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"DEPARTAMENTO\"  ",
            nativeQuery = true)
    ArrayList<DepartmentEntity> getListDepartment(HashMap<String, String> list);
}
