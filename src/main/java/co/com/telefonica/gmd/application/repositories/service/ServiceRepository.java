package co.com.telefonica.gmd.application.repositories.service;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Mayo 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Qualifier("ServiceHttp")
@Slf4j
public class ServiceRepository implements IServiceRepository{

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public ResponseEntity<?> get(String url, HttpHeaders headers , Class<?> classResult) {
        HttpEntity<?> request = (headers == null) ? new HttpEntity<>(null) : new HttpEntity<>(null , headers);
        return restTemplate.exchange(url, HttpMethod.GET, request, classResult);
    }

    @Override
    public ResponseEntity<?> post(String url, Object body, HttpHeaders headers , Class<?> classResult) {
        log.info("URL ["+url+"]");
        log.info("Body ["+body.toString()+"]");
        log.info("HttpHeaders ["+headers.toString()+"]");
        HttpEntity<?> request = (headers == null) ? new HttpEntity<>(body) : new HttpEntity<>(body , headers);
        log.info("HttpEntity ["+request.toString()+"]");
        return restTemplate.exchange(url, HttpMethod.POST, request, classResult);
    }

}
