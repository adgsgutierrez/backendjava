package co.com.telefonica.gmd.application.usescase.conteogravedad;

import co.com.telefonica.gmd.application.repositories.database.conteogravedad.IConteoGravedadRepository;
import co.com.telefonica.gmd.domain.conteogravedad.IConteoGravedadQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("GravedadMayor")
public class GravedadMayorService implements IGravedad{
    @Autowired
    private IConteoGravedadRepository conteoGravedadRepository;

    @Autowired
    private Response response;

    @Override
    public Response get() {
        response.setCode(200);
        response.setMessage("Ok");
        IConteoGravedadQuery conteoGravedadQuery = conteoGravedadRepository.getConteoMayor();
        response.setData(conteoGravedadQuery);
        return response;
    }
}
