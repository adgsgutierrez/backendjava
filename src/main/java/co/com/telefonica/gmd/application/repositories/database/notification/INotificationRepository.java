package co.com.telefonica.gmd.application.repositories.database.notification;

import co.com.telefonica.gmd.domain.notification.NotificationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface INotificationRepository extends CrudRepository<NotificationEntity, String> {

    @Query( value = "SELECT * FROM \"V2_DM\".\"V_Notificacion\" WHERE \"FK_IdUsuario\" IN (:idUsuario , 1 ) ORDER BY 1 DESC",

        countName = "SELECT COUNT(*) FROM \"V2_DM\".\"V_Notificacion\" WHERE \"FK_IdUsuario\" IN (:idUsuario , 1 ) ORDER BY 1 DESC",
        nativeQuery = true)
    Page<NotificationEntity> getList(@Param("idUsuario") Integer idUsuario, Pageable pageable);
}
