package co.com.telefonica.gmd.application.repositories.database.admin.typology;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.admin.typology.TypologyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;

@Repository
public interface ITypologyRepository extends CrudRepository<TypologyEntity, BigInteger> {

    @Query ( value = "SELECT \"IdTipologia\" , \"Tipologia\", \"Estado\" " +
            " FROM \"V2_DM\".\"TIPOLOGIA\" ",
            countName = "SELECT COUNT(*) FROM \"V2_DM\".\"TIPOLOGIA\" ",
            nativeQuery = true)
    ArrayList<TypologyEntity> getListTypology();

    @Query ( value = "UPDATE \"V2_DM\".\"TIPOLOGIA\" SET \"Estado\" = :estado WHERE \"IdTipologia\" = :idtipologia RETURNING \"IdTipologia\"",
            nativeQuery = true)
    BigInteger getUpdateTypology(@Param("idtipologia") BigInteger idtipologia, @Param("estado") Boolean estado);
}
