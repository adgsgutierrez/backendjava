package co.com.telefonica.gmd.application.usescase.dashboard.boxesaffected;

import co.com.telefonica.gmd.platform.beans.Response;

import java.util.Map;

public interface IBoxesAffected {

    Response getBoxesAffected(Map<String, Object> paramsFilter);
}
