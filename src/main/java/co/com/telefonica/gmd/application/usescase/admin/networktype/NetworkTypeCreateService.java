package co.com.telefonica.gmd.application.usescase.admin.networktype;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.admin.networktype.INetworkTypeRepository;
import co.com.telefonica.gmd.domain.admin.networktype.NetworkTypeEntity;
import co.com.telefonica.gmd.domain.admin.networktype.NetworktTypeDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("NetworkTypeCreate")
@Slf4j
public class NetworkTypeCreateService implements INetworkType{

    @Autowired
    private INetworkTypeRepository networkTypeRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        NetworktTypeDTO networktTypeDTO = (NetworktTypeDTO) data;
        NetworkTypeEntity networkTypeEntity = new NetworkTypeEntity();
        networkTypeEntity.setTipored(networktTypeDTO.getTipored());
        networkTypeEntity.setEstado(networktTypeDTO.getEstado());
        response.setData(
                (networkTypeRepository.save(networkTypeEntity) != null)
        );
        return response;
    }
}
