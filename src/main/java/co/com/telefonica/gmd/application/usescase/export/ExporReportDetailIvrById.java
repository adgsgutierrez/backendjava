package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("ExporReportDetailIvrById")
@Slf4j
public class ExporReportDetailIvrById implements IExportOtherFile  {

    @Autowired
    private ObjectToExcel export;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${query.view.detailIvr.id}")
    private String queryFilterIvrById;

    @Override
    public List<String> getWorkbook(Map<String, Object> paramsFilter) {
        Query query = entityManager.createNativeQuery(queryFilterIvrById);
        Integer ticket = Integer.valueOf( String.valueOf( paramsFilter.get("id")));
        query.setParameter("id" , ticket );
        List<Object[]> results = query.getResultList();
        ArrayList<String> dataExport = FunctionsTransform.getDataResultString(
                new Integer[]{3,4,5,6,7,2,8,9} , results
        );
        return dataExport;
    }

}
