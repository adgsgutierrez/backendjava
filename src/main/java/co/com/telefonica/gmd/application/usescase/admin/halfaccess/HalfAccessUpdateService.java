package co.com.telefonica.gmd.application.usescase.admin.halfaccess;

import co.com.telefonica.gmd.application.repositories.database.admin.halfaccess.IHalfAccessRepository;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("HalfAccessUpdate")
@Slf4j
public class HalfAccessUpdateService implements IHalfAccess{
    @Autowired
    private IHalfAccessRepository halfAccessRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        HalfAccessDTO halfAccessDTO = (HalfAccessDTO) data;
        response.setData(
                (halfAccessRepository.getUpdateHalfAccess(halfAccessDTO.getIdma(),halfAccessDTO.getEstado()) != null)
        );
        return response;
    }
}
