package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.application.repositories.database.ivr.IIvrDetailRepository;

import java.util.List;
import java.util.Map;

public interface IExportOtherFile {

    List<String> getWorkbook(Map<String , Object> paramsFilter);
}
