package co.com.telefonica.gmd.application.usescase.user;

import co.com.telefonica.gmd.application.repositories.database.user.IUserDetailRepository;
import co.com.telefonica.gmd.domain.user.UserAllQeury;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("UserDetail")
@Slf4j
public class UserDetailService implements IUserDetail{

    @Autowired
    private IUserDetailRepository detailRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Integer id) {
        response.setCode(200);
        response.setMessage("Ok");
        List<UserAllQeury> user = detailRepository.getDetailUser(id);
        response.setData(user);
        return response;
    }
}
