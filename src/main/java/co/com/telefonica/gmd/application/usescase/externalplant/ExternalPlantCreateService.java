package co.com.telefonica.gmd.application.usescase.externalplant;

import co.com.telefonica.gmd.application.repositories.database.externalplant.IExternalPlantRepository;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantCreateDTO;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
@Service("ExternalPlantCreate")
@Slf4j
public class ExternalPlantCreateService implements IExternalPlant{

    @Autowired
    private IExternalPlantRepository externalPlantRepository;

    @Autowired
    private Response response;

    private ArrayList<ExternalPlantEntity> detailEntity;

    /**
     *
     * @param data
     * @return respuesta
     */
    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("OK");
        detailEntity = new ArrayList<>();
        ExternalPlantCreateDTO externalPlantCreateDTO = (ExternalPlantCreateDTO) data;
        ExternalPlantEntity detailEntityObject = new ExternalPlantEntity();
        detailEntityObject.setFkIdUsuario(externalPlantCreateDTO.getIdUsuario());
        detailEntityObject.setFkIdTipologia(externalPlantCreateDTO.getIdTipologia());
        detailEntityObject.setFkIdMa(externalPlantCreateDTO.getIdMa());
        detailEntityObject.setFkIdEecc(externalPlantCreateDTO.getIdEecc());
        detailEntityObject.setFkIdTipoRed(externalPlantCreateDTO.getIdTipoRed());
        detailEntityObject.setViabilidadTicket(externalPlantCreateDTO.getViabilidadTicket());

            Timestamp timestamp = Timestamp.valueOf(externalPlantCreateDTO.getFechaHoraRegistro());
            detailEntityObject.setFechaHoraRegistro(timestamp);
            detailEntityObject.setFechaHoraInicial( new Timestamp(externalPlantCreateDTO.getFechaInicialRegistro().getTime() ) );
            detailEntityObject.setDepartamento(externalPlantCreateDTO.getDepartamento());
            detailEntityObject.setLocalidad(externalPlantCreateDTO.getLocalidad());
            detailEntityObject.setFechaHoraInicial(new Timestamp( externalPlantCreateDTO.getFechaInicialRegistro().getTime()) );
            detailEntityObject.setDistribuidor(externalPlantCreateDTO.getDistribuidor());
            detailEntityObject.setArmarioCable(externalPlantCreateDTO.getArmarioCable());
            detailEntityObject.setCajasAfectadas(externalPlantCreateDTO.getCajasAfectadas());
            detailEntityObject.setTecnicoReporta(externalPlantCreateDTO.getTecnicoReporta());
            detailEntityObject.setCelularTecnico(externalPlantCreateDTO.getCelularTecnico());
            detailEntityObject.setDesFalla(externalPlantCreateDTO.getDesFalla());
            detailEntityObject.setClientesAfectados(externalPlantCreateDTO.getClientesAfectados());
            detailEntityObject.setBarrio(externalPlantCreateDTO.getBarrio());
            detailEntityObject.setDireccion(externalPlantCreateDTO.getDireccion());
            externalPlantRepository.save(detailEntityObject);

        return response;
    }
}
