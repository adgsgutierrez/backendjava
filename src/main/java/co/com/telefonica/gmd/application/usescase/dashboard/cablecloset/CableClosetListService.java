package co.com.telefonica.gmd.application.usescase.dashboard.cablecloset;

import co.com.telefonica.gmd.application.repositories.database.dashboard.cablecloset.ICableClosetRepository;
import co.com.telefonica.gmd.domain.dashboard.cablecloset.CableClosetListQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Slf4j
@Service("CableClosetList")
public class CableClosetListService implements ICableCloset {

    @Autowired
    private ICableClosetRepository cableClosetRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Integer distributor) {
        response.setCode(200);
        response.setMessage("Success");
        List<CableClosetListQuery> cableClosetEntity = cableClosetRepository.getCableClosetByDistributor(distributor);
        response.setData(cableClosetEntity);
        return response;
    }
}
