package co.com.telefonica.gmd.application.usescase.conteogravedad;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IGravedad {

    Response get();
}
