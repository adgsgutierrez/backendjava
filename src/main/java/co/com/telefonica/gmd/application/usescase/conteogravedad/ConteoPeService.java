package co.com.telefonica.gmd.application.usescase.conteogravedad;

import co.com.telefonica.gmd.application.repositories.database.conteogravedad.IConteoGravedadRepository;
import co.com.telefonica.gmd.domain.conteogravedad.IConteoGravedadQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ConteoPe")
@Slf4j
public class ConteoPeService implements IGravedad{
    @Autowired
    private IConteoGravedadRepository conteoGravedadRepository;

    @Autowired
    private Response response;

    @Override
    public Response get() {
        response.setCode(200);
        response.setMessage("Ok");
        IConteoGravedadQuery conteoGravedadQuery = conteoGravedadRepository.getConteoPe();
        response.setData(conteoGravedadQuery);
        return response;
    }
}
