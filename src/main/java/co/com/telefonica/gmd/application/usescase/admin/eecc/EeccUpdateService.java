package co.com.telefonica.gmd.application.usescase.admin.eecc;

import co.com.telefonica.gmd.application.repositories.database.admin.eecc.IEeccRepository;
import co.com.telefonica.gmd.domain.admin.eecc.EeccDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("EeccUpdate")
@Slf4j
public class EeccUpdateService implements IEecc{
    @Autowired
    private IEeccRepository eeccRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("Ok");
        EeccDTO eeccDTO = (EeccDTO) data;
        response.setData(
                (eeccRepository.getUpdateEecc(eeccDTO.getIdeecc(), eeccDTO.getEstado())  != null)
        );
        return response;
    }
}
