package co.com.telefonica.gmd.application.usescase.detectionthresholds;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IElements {

    Response get(Object data);
}
