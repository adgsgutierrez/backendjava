package co.com.telefonica.gmd.application.usescase.dashboard.distributor;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IDistributor {

    public Response get(Integer location);
}
