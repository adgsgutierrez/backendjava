package co.com.telefonica.gmd.application.usescase.event;

import co.com.telefonica.gmd.platform.beans.Response;

public interface IEvent {

    public Response getOperation(Object request);

}
