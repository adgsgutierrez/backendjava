package co.com.telefonica.gmd.application.repositories.database.dashboard.detailtypeplant;

import co.com.telefonica.gmd.domain.internalplant.IInternalPlantaDetailQuery;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IDetailTypePlantInternalRepository extends CrudRepository<InternalPlantEntity, Integer> {


    @Query(value = "SELECT PI.\"IdPI\", PI.\"Departamento\", PI.\"Localidad\", PI.\"Distribuidor\", " +
            "PC.\"ProveedorContratista\", MA.\"MediosAcceso\", " +
            "PI.\"FechaHoraRegistro\", PI.\"TecnicoReporta\", PI.\"CelularTecnico\", PI.\"DesFalla\", PI.\"ClientesAfectados\", " +
            "PI.\"ViabilidadTicket\", PI.\"Estado\" , PI.\"RangoElementoAfectados\" " +
            "FROM \"V2_DM\".\"PLANTA_INTERNA\" AS PI " +
            "INNER JOIN \"V2_DM\".\"EECC\" AS PC ON PI.\"FK_IdEECC\" = PC.\"IdEECC\" " +
            "INNER JOIN \"V2_DM\".\"MEDIOS_ACCESO\" AS MA ON PI.\"FK_IdMA\" = MA.\"IdMA\" " +
            "WHERE PI.\"IdPI\" = :idPI",
            nativeQuery = true)
    ArrayList<IInternalPlantaDetailQuery> getDetailInternalPlant(@Param("idPI") Integer idPi);
}
