package co.com.telefonica.gmd.application.usescase.conteogravedad;

import co.com.telefonica.gmd.application.repositories.database.conteogravedad.IConteov2Repository;
import co.com.telefonica.gmd.domain.conteogravedad.ConteoGravedadGeneralDTO;
import co.com.telefonica.gmd.domain.conteogravedad.IConteoGravedadQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Slf4j
@Service("GravedadGeneral")
public class GravedadGeneral implements IGravedad{

    @Autowired
    private IConteov2Repository gravedadMenor;
    @Autowired
    private Response response;

    @Override
    public Response get() {
        response.setCode(200);
        response.setMessage("Ok");
        response.setData(gravedadMenor.getInformation());
        return response;
    }
}
