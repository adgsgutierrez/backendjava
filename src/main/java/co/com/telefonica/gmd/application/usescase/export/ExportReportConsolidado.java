package co.com.telefonica.gmd.application.usescase.export;

import co.com.telefonica.gmd.platform.utils.export.ObjectToExcel;
import co.com.telefonica.gmd.platform.utils.transform.FunctionsTransform;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("ExportReportConsolidado")
@Slf4j
public class ExportReportConsolidado implements IExportOtherFile  {

    @Autowired
    private ObjectToExcel export;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${query.view.report.tfs.0}")
    private String queryFTS0;
    @Value("${query.view.report.tfs.1}")
    private String queryFTS1;
    @Value("${query.view.report.tfs.2}")
    private String queryFTS2;
    @Value("${query.view.report.tfs.3}")
    private String queryFTS3;
    @Value("${query.view.report.tfs.4}")
    private String queryFTS4;

    @Override
    public ArrayList<String> getWorkbook(Map<String, Object> paramsFilter) {
        StringBuilder queryFTS = new StringBuilder();
        queryFTS.append(queryFTS0);
        queryFTS.append(queryFTS1);
        queryFTS.append(queryFTS2);
        queryFTS.append(queryFTS3);
        queryFTS.append(queryFTS4);
        Query query = entityManager.createNativeQuery(queryFTS.toString());
        List<Object[]> results = query.getResultList();
        log.info("Results " + results.size());
        ArrayList< String> dataExport = FunctionsTransform.getDataResultString(
                new Integer[]{0,1,2,3,4,5,6,7,8,9,10,11,12} , results
        );
        log.info("Results " + dataExport.size());
        return dataExport;
    }

    private ArrayList<String> getTitles() {
        ArrayList<String> titles = new ArrayList<>();
        titles.add("TELEFONO");
        titles.add("DEPARTAMENTO_1");
        titles.add("MUNICIPIO");
        titles.add("FECHA_INICIAL_DANO");
        titles.add("FECHA_ESTIMADA_REPARACION DANO");
        titles.add("FECHA_REAL_REPARACION DANO");
        titles.add("TIPOLOGIA");
        titles.add("ID_EUREKA");
        titles.add("ESTADO");
        titles.add("NUMERO OT");
        titles.add("FECHA OT");
        titles.add("ESTADO OT");
        titles.add("SISTEMA ORIGEN");
        return titles;
    }
}
