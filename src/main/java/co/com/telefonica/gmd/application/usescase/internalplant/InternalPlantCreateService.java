package co.com.telefonica.gmd.application.usescase.internalplant;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.repositories.database.internalplant.IInternalPlantRepository;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantCreateDTO;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantEntity;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@Service("InternalPlantCreate")
@Slf4j
public class InternalPlantCreateService implements IInternalPlant{

    @Autowired
    private IInternalPlantRepository internalPlantRepository;

    @Autowired
    private Response response;

    private ArrayList<InternalPlantEntity> detailEntity;

    @Override
    public Response get(Object data) {
        response.setCode(200);
        response.setMessage("OK");
        detailEntity = new ArrayList<>();
        InternalPlantCreateDTO internalPlantCreateDTO = (InternalPlantCreateDTO) data;
        InternalPlantEntity detailEntityObject = new InternalPlantEntity();
        detailEntityObject.setFkIdUsuario(internalPlantCreateDTO.getIdUsuario());
        detailEntityObject.setFkIdEecc(internalPlantCreateDTO.getIdEecc());
        detailEntityObject.setIdMa(internalPlantCreateDTO.getIdMa());

            Timestamp timestamp = Timestamp.valueOf(internalPlantCreateDTO.getFechaHoraRegistro());
            detailEntityObject.setFechaHoraRegistro(timestamp);
            detailEntityObject.setFechaHoraInicial( new Timestamp(internalPlantCreateDTO.getFechaInicialRegistro().getTime()) );
            detailEntityObject.setDepartamento(internalPlantCreateDTO.getDepartamento());
            detailEntityObject.setLocalidad(internalPlantCreateDTO.getLocalidad());
            detailEntityObject.setDistribuidor(internalPlantCreateDTO.getDistribuidor());
            detailEntityObject.setTecnicoReporta(internalPlantCreateDTO.getTecnicoReporta());
            detailEntityObject.setCelularTecnico(internalPlantCreateDTO.getCelularTecnico());
            detailEntityObject.setDesFalla(internalPlantCreateDTO.getDesFalla());
            detailEntityObject.setRangoElementosAfectados(internalPlantCreateDTO.getRangoElementosAfectados());
            detailEntityObject.setClientesAfectados(internalPlantCreateDTO.getClientesAfectados());
            detailEntityObject.setResponsableAtencionDanoMasivo(internalPlantCreateDTO.getResponsableAtencionDanoMasivo());
            detailEntityObject.setViabilidadTicket(internalPlantCreateDTO.getViabilidadTicket());
            internalPlantRepository.save(detailEntityObject);

        return response;
    }
}
