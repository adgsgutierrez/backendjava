package co.com.telefonica.gmd.application.usescase.dashboard.distributor;

import co.com.telefonica.gmd.application.repositories.database.dashboard.distributor.IDistributorRepository;
import co.com.telefonica.gmd.domain.dashboard.distributor.DistributorListQuery;
import co.com.telefonica.gmd.platform.beans.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("DistributorList")
public class DistributorListService implements IDistributor{

    @Autowired
    private IDistributorRepository distributorRepository;

    @Autowired
    private Response response;

    @Override
    public Response get(Integer location) {
        response.setCode(200);
        response.setMessage("Success");
        List<DistributorListQuery> distributorListEntity = distributorRepository.getDistributorByLocation(location);
        response.setData(distributorListEntity);
        return response;
    }
}
