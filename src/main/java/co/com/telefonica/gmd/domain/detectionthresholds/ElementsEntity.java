package co.com.telefonica.gmd.domain.detectionthresholds;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ELEMENTO", schema = "V2_DM")
@Getter
@Setter
public class ElementsEntity implements Serializable {

    @Id
    @Column(name = "IdElemento")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idElemento;

    @Column(name = "Elemento")
    private String elemento;

    @Override
    public String toString(){
        return "ElementsEntity{"+
                "idElemento=" + idElemento +
                ", elemento=" + elemento +
                '}';
    }
}
