package co.com.telefonica.gmd.domain.user;

public interface IUserFilterProfile {

    Integer getIdUsuario();

    String getNombres();

    Boolean getEstado();

    String getPerfiles();

    String getEmail();

    String getUsuarioRed();
}
