package co.com.telefonica.gmd.domain.profile;

public interface IProfileFilterQuery {

    Integer getIdUsuario();

    String getPerfil();

    String getNombre();

    Boolean getEstado();

    String getEmail();

    String getUsuarioRed();
}
