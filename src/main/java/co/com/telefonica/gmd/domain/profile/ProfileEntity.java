package co.com.telefonica.gmd.domain.profile;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PERFILES", schema = "V2_DM")
@Getter
@Setter
public class ProfileEntity implements Serializable {

    @Id
    @Column(name = "IdPerfiles")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPerfiles;

    @Column(name = "Perfiles")
    private String perfiles;

    @Override
    public String toString(){
        return "ProfileEntity{" +
                "idPerfiles=" + idPerfiles +
                ", perfiles=" + perfiles +
                '}';
    }
}
