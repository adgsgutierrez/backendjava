package co.com.telefonica.gmd.domain.dashboard.distributor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DistributorDTO {

    private Integer idDistribuidor;
    private String distribuidor;

    @Override
    public String toString() {
        return "{" +
                "idDistribuidor:" + idDistribuidor +
                ", distribuidor:" + distribuidor +
                "}";
    }
}
