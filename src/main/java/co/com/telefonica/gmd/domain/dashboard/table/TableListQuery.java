package co.com.telefonica.gmd.domain.dashboard.table;

public interface TableListQuery {

    Integer getId();
    String getFecha();
    String getDepartamento();
    String getLocalidad();
    String getDiagnostico();
    String getDistribuidor();
    String getNombreElemento();
    String getElemento();
    String getUsuario();
    Integer getAfectados();
    String getGravedad();
    String getTipoPlanta();
    Boolean getEstado();
}
