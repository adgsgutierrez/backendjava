package co.com.telefonica.gmd.domain.profile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ProfileFilterDTO {

    private Integer getIdUsuario;

    private String getPerfil;

    private String getNombre;

    private Boolean getEstado;

    private String getEmail;

    private String getUsuarioRed;
}
