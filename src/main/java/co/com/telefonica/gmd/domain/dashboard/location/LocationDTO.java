package co.com.telefonica.gmd.domain.dashboard.location;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LocationDTO {

    private Integer idLocalidad;
    private String localidad;
    private Integer codigoDane;

    @Override
    public String toString() {
        return "{" +
                "idLocalidad:" + idLocalidad +
                ", localidad:" + localidad +
                ", codigoDane:" + codigoDane +
                "}";
    }
}
