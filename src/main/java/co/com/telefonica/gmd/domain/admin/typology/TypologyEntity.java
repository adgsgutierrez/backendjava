package co.com.telefonica.gmd.domain.admin.typology;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table( name = "TIPOLOGIA" ,schema = "V2_DM")

@Getter
@Setter
public class TypologyEntity implements Serializable {

    @Id
    @Column( name = "IdTipologia")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger idtipologia;

    @Column( name = "Tipologia")
    private String tipologia;

    @Column( name = "Estado")
    private Boolean estado;

    @Override
    public String toString() {
        return "typologyEntity{" +
                "idTipologia=" + idtipologia +
                ", tipologia=" + tipologia +
                ", estado=" + estado + '\'' +
                '}';
    }
}
