package co.com.telefonica.gmd.domain.internalplant;

import java.sql.Timestamp;

public interface IInternalPlantaDetailQuery {

    Integer getIdPI();

    String getDepartamento();

    String getLocalidad();

    String getDistribuidor();

    String getProveedorContratista();

    String getMediosAcceso();

    Timestamp getFechaHoraRegistro();

    String getTecnicoReporta();

    String getCelularTecnico();

    String getDesFalla();

    String getClientesAfectados();

    Integer getViabilidadTicket();

    Boolean getEstado();

    String getRangoElementoAfectados();
}
