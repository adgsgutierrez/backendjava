package co.com.telefonica.gmd.domain.dashboard.department;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "DEPARTAMENTO",
        schema = "V2_DM")

@Getter
@Setter
public class DepartmentEntity implements Serializable {

    @Id
    @Column(name = "IdDpto")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDpto;

    @Column(name = "Departamento")
    private String departamento;

    @Override
    public String toString() {
        return "DepartmentEntity{" +
                "idDpto=" + idDpto +
                ", departamento=" + departamento +
                "}";
    }
}
