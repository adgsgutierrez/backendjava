package co.com.telefonica.gmd.domain.admin.halfaccess;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table( name = "MEDIOS_ACCESO" ,schema = "V2_DM")

@Getter
@Setter
public class HalfAccessEntity implements Serializable {
    @Id
    @Column( name = "IdMA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idma;

    @Column( name = "MediosAcceso")
    private String mediosacceso;

    @Column( name = "Estado")
    private Boolean estado;

    @Override
    public String toString() {
        return "HalfAccessEntity{" +
                "idMA=" + idma +
                ", mediosacceso=" + mediosacceso +
                ", estado=" + estado + '\'' +
                '}';
    }
}
