package co.com.telefonica.gmd.domain.hada;

public interface IResponseQueryHada {

    String getDescription();

    String getLocation();

    String getNetType();

    String getNetName();

    String getNetAddress();

    Integer getAffectedCustomer();

    String getSeverity();

    String getEventId();

    String getAdditionalInfo();

    String getSummary();

    String getAgent();

    String getNetState();

    String getMassiva();

    String getClas();

}
