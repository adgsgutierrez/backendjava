package co.com.telefonica.gmd.domain.detectionthresholds;

import lombok.Getter;
import lombok.Setter;
import org.checkerframework.checker.units.qual.C;

import javax.persistence.*;

@Entity
@Table(name = "UMBRALES", schema = "V2_DM")
@Getter
@Setter
public class ThresholdsEntity {

    @Id
    @Column(name = "IdUmbrales")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUmbrales;

    @Column(name = "FK_IdElemento")
    private Integer FK_idElemento;

    @Column(name = "PorcConexion")
    private Integer porcConexion;

    @Column(name = "PorcDesconexion")
    private Integer porcDesconexion;

    @Column(name = "VolDesconexion")
    private Integer volDesconexion;

    @Column(name = "VentanaTiempo")
    private Integer ventanaTiempo;

    @Override
    public String toString(){
        return "ThresholdsEntity{" +
                "idUmbrales=" +idUmbrales+
                ", FK_idElemento=" +FK_idElemento+
                ", porcConexion="+porcConexion+
                ", volDesconexion="+volDesconexion+
                ", ventanaTiempo="+ventanaTiempo+
                '}';
    }
}
