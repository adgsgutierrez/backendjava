package co.com.telefonica.gmd.domain.permits;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="RelacionPerfilesPermisos", catalog = "gdm", schema = "V2_DM")
@Getter
@Setter
public class PermitsEntity {

    @Id
    @Column(name = "IdRelacionPerfilesPermisos")
    private Integer idRelacionPerfilesPermisos;

    @Column(name = "FK_IdPerfiles")
    private Integer idperfiles;

    @Column(name = "FK_IdPermisos")
    private Integer idpermisos;

    @Column (name = "Estado")
    private Boolean estado;

    @Override
    public  String toString(){
        return  "PermitsEntity{" +
                "idRelacionPerfilesPermisos="+ idRelacionPerfilesPermisos +
                ", idperfiles=" + idperfiles +
                ", idpermisos=" + idpermisos +
                ", estado=" + estado +
                '}';
    }
}
