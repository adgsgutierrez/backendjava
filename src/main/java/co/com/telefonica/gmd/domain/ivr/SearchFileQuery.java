package co.com.telefonica.gmd.domain.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import java.math.BigInteger;
import java.util.Date;

public interface SearchFileQuery {

    BigInteger getIdCargarIvr();
    String getUsuario();
    String getNombreArchivo();
    Date getFechaCarga();
    Integer getClientesAfectados();
    Boolean getEstado();

}
