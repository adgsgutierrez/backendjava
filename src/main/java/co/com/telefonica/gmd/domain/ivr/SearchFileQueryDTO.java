package co.com.telefonica.gmd.domain.ivr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@ToString
public class SearchFileQueryDTO {

    private BigInteger idCargarIvr;
    private String usuario;
    private String nombreArchivo;
    private String fechaCarga;
    private Integer clientesAfectados;
    private Boolean estado;

}

