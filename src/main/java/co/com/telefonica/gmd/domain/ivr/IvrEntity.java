package co.com.telefonica.gmd.domain.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table( name = "CARGA_IVR")
@Getter
@Setter
@ToString
public class IvrEntity {

    @Id
    @Column(name = "IdCargaIVR")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private BigInteger idCargarIvr;

    @Column(name = "FK_IdUsuario")
    private BigInteger fkIdUsuario;

    @Column(name="NombreArchivo")
    private String nombreArchivo;

    @Column(name="FechaCarga")
    private Date fechaCarga;

    @Column(name="ClientesAfectados")
    private Integer clientesAfectados;

    @Column(name="Estado")
    private Boolean estado;
}
