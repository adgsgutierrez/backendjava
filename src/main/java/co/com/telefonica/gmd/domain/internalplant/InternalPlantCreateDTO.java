package co.com.telefonica.gmd.domain.internalplant;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class InternalPlantCreateDTO {

    private Integer idPI;
    private Integer idUsuario;
    private String departamento;
    private String localidad;
    private String distribuidor;
    private Integer idEecc;
    private Integer idMa;
    private Date fechaInicialRegistro;
    private String fechaHoraRegistro;
    private String tecnicoReporta;
    private String celularTecnico;
    private String desFalla;
    private String rangoElementosAfectados;
    private Integer clientesAfectados;
    private String responsableAtencionDanoMasivo;
    private Integer viabilidadTicket;
}
