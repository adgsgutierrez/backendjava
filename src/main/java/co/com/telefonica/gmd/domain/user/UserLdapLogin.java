package co.com.telefonica.gmd.domain.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLdapLogin {

    private String username;

    private String password;

    @Override
    public String toString() {
        return "{" +
                " \"username\" : \"" + username + "\", " +
                " \"password\" : \"" + password + "\"" +
                "}";
    }
}
