package co.com.telefonica.gmd.domain.admin.typology;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;

@Getter
@Setter
@ToString
public class TypologyDTO {

    private BigInteger idtipologia;

    private String tipologia;

    private Boolean estado;
}
