package co.com.telefonica.gmd.domain.dashboard.table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
public class TableListQueryDTO {

    Integer id;
    String fecha;
    String departamento;
    String localidad;
    String diagnostico;
    String distribuidor;
    String nombreElemento;
    String elemento;
    String usuario;
    Integer afectados;
    String gravedad;
    String tipoPlanta;
    Boolean estado;

}
