package co.com.telefonica.gmd.domain.dashboard.location;

public interface LocationListQuery {

    Integer getIdLocalidad();
    String getLocalidad();
    Integer getCodigoDane();

}
