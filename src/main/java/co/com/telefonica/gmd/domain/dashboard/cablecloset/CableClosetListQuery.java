package co.com.telefonica.gmd.domain.dashboard.cablecloset;

public interface CableClosetListQuery {

    Integer getIdAC();
    String getArmarioCable();
}
