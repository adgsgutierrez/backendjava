package co.com.telefonica.gmd.domain.externalplant;

import java.sql.Timestamp;

public interface IExternalPlantDetailQuery {

    Integer getIdPE();

    String getDepartamento();

    String getLocalidad();

    String getDistribuidor();

    String getArmarioCable();

    String getCajasAfectadas();

    String getProveedorContratista();

    String getMediosAcceso();

    String getTipologia();

    String getTipoRed();

    Timestamp getFechaHoraRegistro();

    String getTecnicoReporta();

    String getCelularTecnico();

    String getDesFalla();

    String getClientesAfectados();

    Integer getViabilidadTicket();

    Boolean getEstado();

    String getBarrio();

    String getDireccion();
}
