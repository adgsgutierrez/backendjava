package co.com.telefonica.gmd.domain.dashboard.cablecloset;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CableClosetDTO {

    private Integer idAC;
    private String armarioCable;

    @Override
    public String toString() {
        return "{" +
                "idAC:" + idAC +
                ", armarioCable:" + armarioCable +
                "}";
    }
}
