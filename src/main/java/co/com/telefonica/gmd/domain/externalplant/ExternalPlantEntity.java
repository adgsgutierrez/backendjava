package co.com.telefonica.gmd.domain.externalplant;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;


@Entity
@Table( name = "PLANTA_EXTERNA",
        schema = "V2_DM")

@Getter
@Setter
public class ExternalPlantEntity implements Serializable {

    @Id
    @Column(name = "IdPE")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPE;

    @Column(name = "FK_IdUsuario")
    private Integer fkIdUsuario;

    @Column(name = "Departamento")
    private String departamento;

    @Column(name = "Localidad")
    private String localidad;

    @Column(name = "Distribuidor")
    private String distribuidor;

    @Column(name = "ArmarioCable")
    private String armarioCable;

    @Column(name = "CajasAfectadas")
    private String cajasAfectadas;

    @Column(name = "FK_IdEECC")
    private Integer fkIdEecc;

    @Column(name = "FK_IdMA")
    private Integer fkIdMa;

    @Column(name = "FK_IdTipologia")
    private Integer fkIdTipologia;

    @Column(name = "FK_IdTipoRed")
    private Integer fkIdTipoRed;

    @Column(name = "FechaHoraInicial")
    private Timestamp fechaHoraInicial;

    @Column(name = "FechaHoraRegistro")
    private Timestamp fechaHoraRegistro;

    @Column( name = "TecnicoReporta")
    private String tecnicoReporta;

    @Column( name = "CelularTecnico")
    private String celularTecnico;

    @Column( name = "DesFalla")
    private String desFalla;

    @Column( name = "ClientesAfectados")
    private Integer clientesAfectados;

    @Column( name = "ViabilidadTicket")
    private Integer viabilidadTicket;

    @Column( name = "Barrio" )
    private String barrio;

    @Column ( name = "Direccion")
    private String direccion;

    @Override
    public String toString() {
        return "PeEntity{" +
                "idPE=" + idPE +
                ", fkIdUusuario=" + fkIdUsuario +
                ", departamento=" + departamento +
                ", localidad=" + localidad +
                ", distribuidor=" + distribuidor +
                ", armarioCable=" + armarioCable +
                ", cajasAfectadas=" + cajasAfectadas +
                ", fkIdEecc=" + fkIdEecc +
                ", fkIdMa=" + fkIdMa +
                ", fkIdTipologia=" + fkIdTipologia +
                ", fkIdTipoRed=" + fkIdTipoRed +
                ", fechaHoraRegistro=" + fechaHoraRegistro +
                ", tecnicoReporta=" + tecnicoReporta +
                ", celularTecnico=" + celularTecnico +
                ", desFalla=" + desFalla +
                ", clientesAfectados=" + clientesAfectados +
                ", viabilidadTicket=" + viabilidadTicket +
                ", barrio=" + barrio +
                ", direccion=" + direccion +
                "}";
    }

}

