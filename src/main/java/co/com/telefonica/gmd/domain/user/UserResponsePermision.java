package co.com.telefonica.gmd.domain.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;

@Getter
@Setter
@ToString
public class UserResponsePermision {

    public UserResponsePermision() {
        permisions = new ArrayList<>();
        profile = "";
    }

    private String id;

    private ArrayList<String> permisions;

    private String profile;

    private String name;


}
