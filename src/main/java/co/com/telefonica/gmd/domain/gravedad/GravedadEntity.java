package co.com.telefonica.gmd.domain.gravedad;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table( name = "GRAVEDAD",
        schema = "V2_DM")
@Getter
@Setter
public class GravedadEntity {

    @Id
    @Column(name = "IdGravedad")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idGravedad;

    @Column(name = "Desde")
    private Integer desde;

    @Column(name = "Hasta")
    private Integer hasta;

    @Column(name = "TipoGravedad")
    private String tipoGravedad;
}
