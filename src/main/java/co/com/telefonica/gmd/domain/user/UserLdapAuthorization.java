package co.com.telefonica.gmd.domain.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class UserLdapAuthorization {

    private String user;
    private String password;
    private String hostDestiny;

    @Override
    public String toString() {
        return "{" +
                "\"user\" :\"" + user +"\", " +
                "\"password\":\"" + password + "\", " +
                "\"hostDestiny\":\"" + hostDestiny + "\"" +
                '}';
    }
}
