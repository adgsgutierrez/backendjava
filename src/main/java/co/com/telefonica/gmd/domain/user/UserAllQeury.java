package co.com.telefonica.gmd.domain.user;

public interface UserAllQeury {

    Integer getIdUsuario();

    String getPerfiles();

    String getNombres();

    Boolean getEstado();

    String getEmail();

    String getUsuarioRed();
}
