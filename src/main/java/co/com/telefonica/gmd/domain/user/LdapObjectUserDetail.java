package co.com.telefonica.gmd.domain.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LdapObjectUserDetail {

    private String name;

    private Object memberOf;

    private String company;

    private String department;
}
