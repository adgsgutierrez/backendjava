package co.com.telefonica.gmd.domain.externalplant;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@ToString
public class ExternalPlantCreateDTO {

    private Integer idUsuario;
    private String departamento;
    private String localidad;
    private String distribuidor;
    private String armarioCable;
    private String cajasAfectadas;
    private Integer idTipologia;
    private Integer idMa;
    private Integer idEecc;
    private Integer idTipoRed;
    private String fechaHoraRegistro;
    private Date fechaInicialRegistro;
    private String tecnicoReporta;
    private String celularTecnico;
    private String desFalla;
    private Integer clientesAfectados;
    private Integer viabilidadTicket;
    private String barrio;
    private String direccion;
}
