package co.com.telefonica.gmd.domain.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
public interface UserProfileQuery {

    String getNombres();

    String getPerfiles();

    String getPermisos();

    String getUsuarioRed();

    String getId();
}