package co.com.telefonica.gmd.domain.admin.eecc;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table( name = "EECC",
        schema = "V2_DM")

@Getter
@Setter
public class EeccEntity implements Serializable {

    @Id
    @Column( name = "IdEECC")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ideecc;

    @Column(name = "ProveedorContratista")
    private  String proveedorContratista;

    @Column( name = "Estado")
    private Boolean estado;

    @Override
    public  String toString() {
        return "EeccEntity{" +
                "idEecc=" + ideecc +
                ", proveedorContratista=" + proveedorContratista +
                ", estado=" + estado + '\'' +
                '}';
    }
}
