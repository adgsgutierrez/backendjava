package co.com.telefonica.gmd.domain.notification;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalTime;

@Entity
@Table( name = "NOTIFICACION",
        schema = "V2_DM")

@Getter
@Setter
public class NotificationEntity implements Serializable {

    @Id
    @Column(name = "IdNotificacion")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idNotificacion;

    @Column(name = "FK_IdUsuario")
    private Integer idUsuario;

    @Column(name = "Notificacion")
    private String notification;

    @Column(name = "TipoNotificacion")
    private String tipoNotificacion;

    @Column(name = "Estado")
    private Boolean estado;

    @Column(name = "Fecha")
    private Date fecha;

    @Column(name = "Hora")
    private LocalTime hora;

    @Override
    public String toString() {
        return "NotificationEntity{" +
                "idNotificacion=" + idNotificacion  +
                ", idUsuario=" + idUsuario +
                ", notificacion=" + notification +
                ", tipoNotificacion=" + tipoNotificacion +
                ", estado=" + estado +
                ", fecha=" + fecha +
                ", hora=" + hora +
                "}";
    }
}
