package co.com.telefonica.gmd.domain.notification;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Date;
import java.time.LocalTime;

@Getter
@Setter
@ToString
public class NotificationDTO {

    private Integer idUsuario;
    private String notificacion;
    private String tipoNotificacion;
    private Boolean estado;
    private Date fecha;
    private LocalTime hora;
}
