package co.com.telefonica.gmd.domain.search;

import java.sql.Date;

public interface ISearchQuery {

    Integer getIdCargaIVR();

    String getNombreArchivo();

    Date getFechaCarga();

    Integer getAfectados();

    String getUsuario();

    Boolean getEstado();
}
