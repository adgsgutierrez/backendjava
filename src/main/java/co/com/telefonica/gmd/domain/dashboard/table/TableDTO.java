package co.com.telefonica.gmd.domain.dashboard.table;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@Getter
@Setter
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TableDTO {

    private String fecha;
    private String departamento;
    private String localidad;
    private String diagnostico;
    private String distribuidor;
    private String nombreElemento;
    private String elemento;
    private String usuario;
    private Integer afectados;
    private String tipoPlanta;
    private Boolean estado;

    @Override
    public String toString() {
        return "{" +
                "\"fecha\":\"" + fecha + "\"," +
                "\"departamento\":\"" + departamento + "\"" +
                "\"localidad\":\"" + localidad + "\"" +
                "\"diagnostico\":\"" + diagnostico + "\"" +
                "\"distribuidor\":\"" + distribuidor + "\"" +
                "\"nombreElemento\":\"" + nombreElemento + "\"" +
                "\"elemento\":\"" + elemento + "\"" +
                "\"usuario\":\"" + usuario + "\"" +
                "\"afectados\":\"" + afectados + "\"" +
                "\"tipoPlanta\":\"" + tipoPlanta + "\"" +
                "\"estado\":\"" + estado + "\"" +
                "}";

    }

}
