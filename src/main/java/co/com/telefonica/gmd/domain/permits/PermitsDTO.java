package co.com.telefonica.gmd.domain.permits;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PermitsDTO {

    private Integer idPerfiles;
    private Integer idPermisos;
    private Boolean estado;
}
