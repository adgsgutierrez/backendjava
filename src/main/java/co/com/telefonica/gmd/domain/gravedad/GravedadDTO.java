package co.com.telefonica.gmd.domain.gravedad;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class GravedadDTO {

    private Integer idGravedad;

    private Integer desde;

    private Integer hasta;

    private String tipoGravedad;
}
