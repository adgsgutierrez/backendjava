package co.com.telefonica.gmd.domain.conteogravedad;

import java.math.BigInteger;

public interface IConteoGravedadQuery {

    BigInteger getConteoGravedad();
}
