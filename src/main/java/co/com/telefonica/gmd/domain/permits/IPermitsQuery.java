package co.com.telefonica.gmd.domain.permits;

public interface IPermitsQuery {

    Integer getIdPerfil();
    Integer getIdPermiso();
    String getPermisos();
    Boolean getEstado();
}
