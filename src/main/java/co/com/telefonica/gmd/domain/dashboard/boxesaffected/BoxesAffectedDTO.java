package co.com.telefonica.gmd.domain.dashboard.boxesaffected;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BoxesAffectedDTO {

    private Integer idCA;
    private Integer fkIdAC;
    private String cajasAfectadas;

    @Override
    public String toString() {
        return "{" +
                "idCA:" + idCA +
                ", fkIdAC:" + fkIdAC +
                ", cajasAfectadas:" + cajasAfectadas +
                "}";
    }
}
