package co.com.telefonica.gmd.domain.ivr;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;
import java.util.Objects;

@Entity
@Table( name = "IVR", catalog="gdm" , schema = "V2_DM")
@Getter
@Setter
@ToString
public class IvrDetailEntity {

    @Id
    @Column( name = "IdIVR")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private BigInteger idIvr;

    @Column( name = "FK_IdTipologia")
    private BigInteger fkIdTipologia;

    @Column( name = "FK_IdCargaIVR")
    private BigInteger fkIdCargaIvr;

    @Column( name = "ViabilidadTicket")
    private Integer viabilidadTicket;

    @Column( name = "Abonado")
    private String abonado;

    @Column( name = "FechaFin")
    private Date fechaFin;

    @Column( name = "HoraFin")
    private Time horaFin;

    @Column( name = "TipoDaño")
    private Integer tipoDanio;

    @Column( name = "Flag1")
    private Integer flag1;

    @Column( name = "Flag2")
    private Integer flag2;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IvrDetailEntity that = (IvrDetailEntity) o;
        return viabilidadTicket.equals(that.viabilidadTicket) &&
                abonado.equals(that.abonado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fkIdTipologia, viabilidadTicket, abonado, fechaFin, horaFin, tipoDanio, flag1, flag2);
    }
}
