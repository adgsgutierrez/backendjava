package co.com.telefonica.gmd.domain.internalplant;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table( name = "PLANTA_INTERNA",
        schema = "V2_DM")
@Getter
@Setter
public class InternalPlantEntity implements Serializable {

    @Id
    @Column(name = "IdPI")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPI;

    @Column(name = "FK_IdUsuario")
    private Integer fkIdUsuario;

    @Column(name = "Departamento")
    private String departamento;

    @Column(name = "Localidad")
    private String localidad;

    @Column(name = "Distribuidor")
    private String distribuidor;

    @Column(name = "FK_IdEECC")
    private Integer fkIdEecc;

    @Column(name = "FK_IdMA")
    private Integer idMa;

    @Column(name = "FechaHoraInicial")
    private Timestamp fechaHoraInicial;

    @Column(name = "FechaHoraRegistro")
    private Timestamp fechaHoraRegistro;

    @Column(name = "TecnicoReporta")
    private String tecnicoReporta;

    @Column(name = "CelularTecnico")
    private String celularTecnico;

    @Column(name = "DesFalla")
    private String desFalla;

    @Column(name = "RangoElementoAfectados")
    private String rangoElementosAfectados;

    @Column(name = "ClientesAfectados")
    private Integer clientesAfectados;

    @Column(name = "ResponsableAtencionDañoMasivo")
    private String responsableAtencionDanoMasivo;

    @Column(name = "ViabilidadTicket")
    private Integer viabilidadTicket;

}
