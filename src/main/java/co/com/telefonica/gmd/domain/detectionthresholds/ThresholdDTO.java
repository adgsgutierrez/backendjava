package co.com.telefonica.gmd.domain.detectionthresholds;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ThresholdDTO {

    private Integer idElemento;
    private Integer porcConexion;
    private Integer porcDesconexion;
    private Integer volDesconexion;
    private Integer ventanaTiempo;
}
