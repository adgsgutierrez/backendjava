package co.com.telefonica.gmd.domain.event;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EventUpdateRequest {

    private String idUsuario;
    private Boolean estadoTicket;
    private String viabilidadTicket;

}
