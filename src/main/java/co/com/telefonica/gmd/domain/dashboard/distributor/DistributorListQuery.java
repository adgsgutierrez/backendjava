package co.com.telefonica.gmd.domain.dashboard.distributor;

public interface DistributorListQuery {

    Integer getIdDistribuidor();
    String  getDistribuidor();
}
