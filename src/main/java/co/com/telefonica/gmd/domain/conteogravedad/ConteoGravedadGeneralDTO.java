package co.com.telefonica.gmd.domain.conteogravedad;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ConteoGravedadGeneralDTO {

    private Integer critica;

    private Integer mayor;

    private Integer menor;

    private Integer activos;

    private Integer inactivos;

    private Integer conteoPi;

    private Integer conteoPe;
}
