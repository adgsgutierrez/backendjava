package co.com.telefonica.gmd.domain.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;


@Entity
@Table( name = "USUARIOS",
        schema = "V2_DM" )
@Getter
@Setter
public class UsuariosEntity {

    @Id
    @Column(name = "IdUsuario")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idUsuario;

    @Column(name = "FK_IdPerfiles")
    private Integer fkIdPerfiles;

    @Column(name="Nombres")
    private String nombres;

    @Column(name="Estado")
    private Boolean estado;

    @Column(name="Email")
    private String email;

    @Column(name="UsuarioRed")
    private String usuarioRed;

    @Override
    public String toString() {
        return "UserEntity{" +
                "idUsuario=" + idUsuario +
                ", fkIdPerfiles=" + fkIdPerfiles +
                ", nombres='" + nombres + '\'' +
                ", estado=" + estado +
                ", email='" + email + '\'' +
                ", usuarioRed='" + usuarioRed + '\'' +
                '}';
    }
}
