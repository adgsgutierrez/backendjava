package co.com.telefonica.gmd.domain.dashboard.department;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DepartmentDTO {

    private Integer idDpto;
    private String departamento;
}
