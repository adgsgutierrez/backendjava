package co.com.telefonica.gmd.domain.dashboard.boxesaffected;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "CAJAS_AFECTADAS",
        schema = "V2.DM")

@Getter
@Setter
public class BoxesAffectedEntity implements Serializable {

    @Id
    @Column(name = "IdCA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCa;

    @Column(name = "FK_IdAC")
    private Integer fkIdAc;

    @Column(name = "CajasAfectadas")
    private String cajasAfectadas;

    @Override
    public String toString() {
        return "BoxesAffectedEntity {" +
                "idCa : " + idCa +
                ", fkIdAc : " + fkIdAc +
                ", cajasAfectadas : " + cajasAfectadas +
                "}";
    }
}
