package co.com.telefonica.gmd.domain.admin.networktype;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table( name = "TIPO_RED", schema = "V2_DM")

@Getter
@Setter
public class NetworkTypeEntity implements Serializable {

    @Id
    @Column( name = "IdTipoRed")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger idtipored;

    @Column( name = "TipoRed")
    private String tipored;

    @Column(name = "Estado")
    private Boolean estado;

    @Override
    public  String toString(){
        return "NetworkTypeEntity {" +
                "idTipoRed=" + idtipored +
                ", tipored=" + tipored +
                ", estado=" + estado + '\'' +
                '}';
    }
}
