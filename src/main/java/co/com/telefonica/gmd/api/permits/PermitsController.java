package co.com.telefonica.gmd.api.permits;

import co.com.telefonica.gmd.application.usescase.permits.IPermitsService;
import co.com.telefonica.gmd.domain.permits.IPermitsQuery;
import co.com.telefonica.gmd.domain.permits.PermitsDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;

@RestController
@RequestMapping("/api/permits")
public class PermitsController {

    @Autowired
    @Qualifier("Permits")
    private IPermitsService permitsService;

    @Autowired
    @Qualifier("PermitsUpdate")
    private IPermitsService permitsUpdateService;

    @GetMapping("/all/{perfil}")
    public Response getAllPermits(
            @PathVariable("perfil") Integer perfil,
            @RequestParam(name = "limit" ,required = false , defaultValue = "25") String limit,
            @RequestParam(name = "page" ,required = false , defaultValue = "0")  String page
            ){
        Object[] data = new Object[]{perfil,page,limit};
        return this.permitsService.get(data);
    }

    @PostMapping("/update")
    public Response updatePermits(@RequestBody() PermitsDTO permitsDTO){
        Object[] data = new Object[]{permitsDTO};
        return this.permitsUpdateService.get(data);

    }
}
