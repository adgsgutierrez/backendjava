package co.com.telefonica.gmd.api.internalplant;

import co.com.telefonica.gmd.application.usescase.internalplant.IInternalPlant;
import co.com.telefonica.gmd.domain.internalplant.InternalPlantCreateDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
@RestController
@RequestMapping("/api/internalplant")
public class InternalPlantController {

    @Autowired
    @Qualifier("InternalPlantCreate")
    private IInternalPlant internalPlantService;

    /**
     * @param internalPlant
     * @return resultado insercion de datos planta interna
     */
    @PostMapping("/create")
    public Response createInternalPlant(@RequestBody()InternalPlantCreateDTO internalPlant) {return this.internalPlantService.get(internalPlant);}
}
