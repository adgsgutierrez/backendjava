package co.com.telefonica.gmd.api.dashboard.boxesaffected;

import co.com.telefonica.gmd.application.usescase.dashboard.boxesaffected.IBoxesAffected;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

@RestController
@RequestMapping("/api/dashboard/boxesaffected")
public class BoxesAffectedController {

    @Autowired
    @Qualifier("BoxesAffectedList")
    private IBoxesAffected boxesAffected;

    @GetMapping("/all")
    public Response getBoxesAffected(@RequestParam Map<String, Object> params ){
        return  this.boxesAffected.getBoxesAffected(params);
    }

}
