package co.com.telefonica.gmd.api.externalplant;

import co.com.telefonica.gmd.application.usescase.externalplant.IExternalPlant;
import co.com.telefonica.gmd.domain.externalplant.ExternalPlantCreateDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */

@RestController
@RequestMapping("/api/externalplant")
public class ExternalPlantController {

    @Autowired
    @Qualifier("ExternalPlantCreate")
    private IExternalPlant externalPlantService;

    /**
     * @param externalPlant
     * @return resultado insercion de datos planta externa
     */
    @PostMapping("/create")
    public Response createExternalPlant(@RequestBody()ExternalPlantCreateDTO externalPlant) {return this.externalPlantService.get(externalPlant);}
}
