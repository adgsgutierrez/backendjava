package co.com.telefonica.gmd.api.dashboard.department;

import co.com.telefonica.gmd.application.usescase.dashboard.department.IDepartment;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/api/dashboard/department")
public class DepartmentController {

    @Autowired
    @Qualifier("DepartmentList")
    private IDepartment departmentListService;

    @GetMapping("/all")
    public Response getAllDepartment(){
        HashMap<String , String> list = new HashMap<>();
        return this.departmentListService.get(list);}
}
