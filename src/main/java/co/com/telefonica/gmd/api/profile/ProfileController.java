package co.com.telefonica.gmd.api.profile;

import co.com.telefonica.gmd.application.usescase.profile.IProfielFilter;
import co.com.telefonica.gmd.application.usescase.profile.IProfile;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/profile")
public class ProfileController {

    @Autowired
    @Qualifier("ProfileFilter")
    private IProfielFilter profielFilter;

    @Autowired
    @Qualifier("ProfileTable")
    private IProfile profile;

    @GetMapping("/all/{id}")
    public Response filterProfile(
            @PathVariable("id") Integer id,
            @RequestParam(name = "limit" ,required = false , defaultValue = "25") String limit,
            @RequestParam(name = "page" ,required = false , defaultValue = "0")  String page){
        Object[] data = new Object[]{id , limit, page};

        return this.profielFilter.get(data);
    }

    @GetMapping("/select")
    public Response getProfile(){
        HashMap<String, String> list = new HashMap<>();
        return this.profile.get(list);
    }
}
