package co.com.telefonica.gmd.api.dashboard.table;

import co.com.telefonica.gmd.application.usescase.dashboard.table.ITable;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/dashboard/table")
public class TableController {

    @Autowired
    @Qualifier("TableList")
    private ITable tableListService;

    @GetMapping("/all")
    public Response getAllTable(@RequestParam Map<String, Object> params){
        return this.tableListService.get(params);
    }

}
