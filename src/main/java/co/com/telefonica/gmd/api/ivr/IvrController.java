package co.com.telefonica.gmd.api.ivr;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.usescase.ivr.IIvrService;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/ivr")
public class IvrController {

    @Autowired
    @Qualifier("IvrLoad")
    private IIvrService ivrService;

    @Autowired
    @Qualifier("IvrListFile")
    private IIvrService ivrListService;

    @Autowired
    @Qualifier("IvrListDetailFile")
    private IIvrService ivrListDetailService;

    @Autowired
    @Qualifier("IvrListDetailTicket")
    private IIvrService ivrListDetailTicket;

    @Autowired
    @Qualifier("IvrCloseFileOpened")
    private IIvrService ivrCloseFileOpened;

    @PostMapping("/load/{user}")
    public Response load(
            @RequestParam("file") MultipartFile file,
            @PathVariable("user") String user ){

        Object[] data = new Object[]{ file, user };
        return this.ivrService.get(data);
    }

    @GetMapping("/all")
    public Response readAllIvr(
            @RequestParam(name="page" , required = false , defaultValue = "0") String page,
            @RequestParam(name="limit" , required = false , defaultValue = "25") String limit,
            @RequestParam(name="field" , required = false) String field,
            @RequestParam(name="value" , required = false) String value ) {

        Object[] data = new Object[]{ page,limit,field,value };
        return this.ivrListService.get(data);
    }

    @GetMapping("/detail/{file}")
    public Response detail(
            @PathVariable("file") String file,
            @RequestParam(name = "limit" ,required = false , defaultValue = "25") String limit,
            @RequestParam(name = "page" ,required = false , defaultValue = "0")  String page,
            @RequestParam(name = "nameColumn" ,required = false , defaultValue = "0")  String nameColumn,
            @RequestParam(name = "valueColumn" ,required = false , defaultValue = "0")  String valueColumn){

        Object[] data = new Object[]{ file , page,limit,nameColumn , valueColumn };

        return this.ivrListDetailService.get(data);
    }

    @GetMapping("/detailTicket")
    public Response detailTicket(
            @RequestParam(name="ticket") String ticket,
            @RequestParam(name = "limit" ,required = false , defaultValue = "25") String limit,
            @RequestParam(name = "page" ,required = false , defaultValue = "0")  String page
    ){
        Object[] data = new Object[]{ticket,page,limit};
        return this.ivrListDetailTicket.get(data);
    }

    @PostMapping("/close/file")
    public Response closeFileOpened(@RequestParam("file") Integer file){
        return this.ivrCloseFileOpened.get( new Object[]{file});
    }

}
