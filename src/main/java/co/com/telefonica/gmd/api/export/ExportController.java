package co.com.telefonica.gmd.api.export;

import co.com.telefonica.gmd.application.usescase.export.IExportOtherFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.web.header.Header;
import org.springframework.web.bind.annotation.*;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/export")
@Slf4j
public class ExportController {

    @Autowired
    @Qualifier("ExportDashboard")
    private IExportOtherFile exportDashboard;

    @Autowired
    @Qualifier("ExportReportDetailIvr")
    private IExportOtherFile exportReportDetailIvr;

    @Autowired
    @Qualifier("ExportIvrFile")
    private IExportOtherFile exportReportIvr;

    @Autowired
    @Qualifier("ExportReportIvr")
    private IExportOtherFile exportFtpIvr;

    @Autowired
    @Qualifier("ExporReportDetailIvrById")
    private IExportOtherFile exportReportById;

    @Autowired
    @Qualifier("ExportReportConsolidado")
    private IExportOtherFile exportReportConsolidado;


    @GetMapping("/dashboard")
    public void getDashBoard(@RequestParam Map<String, Object> params , HttpServletResponse response){
            List<String> workbook = exportDashboard.getWorkbook(params);
            StringBuilder titles = new StringBuilder();
            titles.append("Fecha/Hora Registro;Departamento;Localidad;Diagnóstico;Distribuidor;Elemento;Usuario;Afectados;");
            titles.append("Criticidad;Tipo;Estado;Tecnico Reporta;Fecha/Hora Inicio Daño;Cel tecnico;Viabilidad;Barrio;");
            titles.append("Dirección;Contratista;Medio Acceso;Tipología;Tipo red;Responsable;Distribuidor;ArmarioCable;CajasAfectadas\n");
            saveExport(response , "dashboard" , titles.toString() , workbook);

    }

    @GetMapping("/ivr/detail")
    public void getDetailIvr(@RequestParam Map<String, Object> params , HttpServletResponse response){
            String titles = "TRS;Abonados;Fecha Fin;Hora Fin;Tipo daño;Tipología;Flag 1;Flag 2\n";
            List<String> workbook = exportReportDetailIvr.getWorkbook(params);
            saveExport(response , "detail_ivr" , titles , workbook);
    }


    @GetMapping("/ftp/ivr")
    public void getIvr(@RequestParam Map<String, Object> params , HttpServletResponse response){
            List<String> workbook = exportReportIvr.getWorkbook(params);
            String titles = "TRS;Abonados;Fecha Fin;Hora Fin;Tipo daño;Tipología;Flag 1;Flag 2\n";
            saveExport(response , "ftp" , titles , workbook);
    }

    @GetMapping("/ivr/{id}")
    public void getIvr(@PathVariable("id") String id, HttpServletResponse response){
            HashMap<String, Object> params = new HashMap<>();
            params.put("id" , id);
            List<String> workbook = exportReportById.getWorkbook(params);
            String titles = "TRS;Abonados;Fecha Fin;Hora Fin;Tipo daño;Tipología;Flag 1;Flag 2\n";
            saveExport(response , "ivr" , titles , workbook);
    }

    @GetMapping("/ivr")
    public void getFtpIvr(@RequestParam Map<String, Object> params , HttpServletResponse response){
            String titles = "Nombre del Archivo;Fecha de carga;Clientes Afectados;Usuario\n";
            List<String> workbook = exportFtpIvr.getWorkbook(params);
            saveExport(response , "ivr" , titles , workbook);
    }

    @GetMapping("/ivr/consolidated")
    public void getExport(@RequestParam Map<String, Object> params , HttpServletResponse response){
            List<String> workbook = exportReportConsolidado.getWorkbook(params);
            log.info("Resultados a concatenar "+workbook.size());
            String titles = "TELEFONO;DEPARTAMENTO_1;MUNICIPIO;FECHA_INICIAL_DANO;FECHA_ESTIMADA_REPARACION DANO;FECHA_REAL_REPARACION DANO;TIPOLOGIA;ID_EUREKA;ESTADO;NUMERO OT;FECHA OT;ESTADO OT;SISTEMA ORIGEN\n";
            saveExport(response , "consolidated" , titles , workbook );
    }

    private static String getCurrentDateTime(){
        DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd_HH:mm:ss");
        return dateFormatter.format(new Date());
    }


    private static void saveExport(HttpServletResponse response , String name , String titles , List<String> list){
        try{
            PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.LINKS);
            response.setContentType("text/csv");
            StringBuilder headerKey = new StringBuilder("Content-Disposition");
            StringBuilder headerValue = new StringBuilder("attachment; filename=");
            headerValue.append(name);
            headerValue.append("_");
            headerValue.append(getCurrentDateTime());
            headerValue.append(".csv");

            response.setHeader(
                    policy.sanitize(headerKey.toString()),
                    policy.sanitize(headerValue.toString())
            );
            StringBuilder csvContainer = new StringBuilder(titles);
            csvContainer.append(String.join("\n", list));
            ServletOutputStream out = response.getOutputStream();
            String policyString = policy.sanitize( csvContainer.toString() );
            out.println( policyString );
            out.flush();
            out.close();
        } catch (IOException e) {
            log.error("Error into export detail " + e.getMessage());
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

}
