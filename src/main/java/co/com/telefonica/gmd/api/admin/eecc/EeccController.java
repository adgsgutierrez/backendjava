package co.com.telefonica.gmd.api.admin.eecc;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */

import co.com.telefonica.gmd.application.usescase.admin.eecc.IEecc;
import co.com.telefonica.gmd.domain.admin.eecc.EeccDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/admin/eecc")
public class EeccController {

    @Autowired
    @Qualifier("EeccCreate")
    private IEecc eeccCreate;

    @Autowired
    @Qualifier("EeccList")
    private  IEecc eeccList;

    @Autowired
    @Qualifier("EeccUpdate")
    private IEecc eeccUpdate;

    /**
     * @param eecc
     * @return resultado insercion Eecc
     */

    @PostMapping("/create")
    public Response createEecc(@RequestBody() EeccDTO eecc) { return this.eeccCreate.get(eecc); }

    /**
     *
     * @return resultado listado de tabla Eecc
     */
    @GetMapping("/all")
    public Response getAllEecc(){
        HashMap<String, String> list = new HashMap<>();
        return this.eeccList.get(list);

    }

    @PostMapping("/update")
    public Response updateEecc(@RequestBody() EeccDTO eeccDTO) {return  this.eeccUpdate.get(eeccDTO); }

}
