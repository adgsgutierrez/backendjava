package co.com.telefonica.gmd.api.gravedad;

import co.com.telefonica.gmd.application.usescase.gravedad.IGravedaTable;
import co.com.telefonica.gmd.application.usescase.gravedad.IGravedad;
import co.com.telefonica.gmd.domain.gravedad.GravedadDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/gravedad")
public class GravedadController {

    @Autowired
    @Qualifier("GravedadLevel")
    private IGravedad gravedad;

    @Autowired
    @Qualifier("GravedadTable")
    private IGravedaTable gravedaTable;

    @Autowired
    @Qualifier("GravedadUpdate")
    private IGravedaTable gravedaUpdate;

    @GetMapping("/all")
    public Response gravedadAll(@Param("value")Integer value ){return this.gravedad.get(value); }

    @GetMapping("/select")
    public Response getSelectGravity(){
        HashMap<String, String> list = new HashMap<>();
        return this.gravedaTable.get(list);
    }

    @PostMapping("/update")
    public Response updateGravedades(@RequestBody() GravedadDTO gravedadDTO){
        return gravedaUpdate.get(gravedadDTO);
    }
}
