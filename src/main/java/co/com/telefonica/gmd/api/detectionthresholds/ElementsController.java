package co.com.telefonica.gmd.api.detectionthresholds;

import co.com.telefonica.gmd.application.usescase.detectionthresholds.IElements;
import co.com.telefonica.gmd.application.usescase.detectionthresholds.IThresholds;
import co.com.telefonica.gmd.domain.detectionthresholds.ThresholdDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/detection")
public class ElementsController {

    @Autowired
    @Qualifier("Elements")
    private IElements elements;

    @Autowired
    @Qualifier("Thresholds")
    private IThresholds thresholds;

    @Autowired
    @Qualifier("ThresholdUpdate")
    private IThresholds thresholdsUpdate;

    @GetMapping("/all")
    public Response getElements(){
        HashMap<String, String> list = new HashMap<>();
        return this.elements.get(list);
    }

    @GetMapping("/elementId")
    public Response getElementID(@RequestParam(name = "idElement") Integer idElement){
        Object[] data = new Object[]{idElement};

        return this.thresholds.get(data);
    }

    @PostMapping("/update")
    public Response updateElement(@RequestBody() ThresholdDTO thresholdDTO){
        Object[] data = new Object[]{thresholdDTO};
        return this.thresholdsUpdate.get(data);
    }
}
