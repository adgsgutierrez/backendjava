package co.com.telefonica.gmd.api.search;

import co.com.telefonica.gmd.application.usescase.search.ISearch;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/search")
public class SearchController {

    @Autowired
    @Qualifier("Search")
    private ISearch search;

    @GetMapping("/all")
    public Response getSearch(
            @RequestParam(name="abonado") String abonado,
            @RequestParam(name = "limit" ,required = false , defaultValue = "25") String limit,
            @RequestParam(name = "page" ,required = false , defaultValue = "0")  String page
    ){
        Object[] data = new Object[]{abonado,page, limit};

        return this.search.get(data);
    }
}
