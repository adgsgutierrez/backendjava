package co.com.telefonica.gmd.api.user;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.usescase.user.IUser;
import co.com.telefonica.gmd.application.usescase.user.IUserDetail;
import co.com.telefonica.gmd.application.usescase.user.IUserFilter;
import co.com.telefonica.gmd.domain.user.UserCreateDTO;
import co.com.telefonica.gmd.domain.user.UserDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    @Qualifier("UserLogin")
    private IUser userService;

    @Autowired
    @Qualifier("UserCreate")
    private IUser useCreateService;

    @Autowired
    @Qualifier("UserUpdate")
    private IUser userUpdateService;

    @Autowired
    @Qualifier("UserDetail")
    private IUserDetail userDetail;

    @Autowired
    @Qualifier("UserFilter")
    private IUserFilter userFilter;

    @PostMapping("/login")
    public Response login(@RequestBody UserDTO user){
        return this.userService.get(user);
    }

    @PostMapping("/create")
    public Response createUser(@RequestBody() UserCreateDTO user){
        return this.useCreateService.get(user);
    }

    @PostMapping("/update")
    public Response updateUser(@RequestBody() UserCreateDTO userCreateDTO) { return this.userUpdateService.get(userCreateDTO); }

    @GetMapping("/detail")
    public Response getDetailUser(@Param("id") Integer id){return this.userDetail.get(id); }

    @GetMapping("/all")
    public Response getFilterProfileUser(
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "page" , defaultValue = "0") String page ,
            @RequestParam(name = "items" , defaultValue = "25") String items){

        Object[] data = new Object[]{id,page, items};

        return this.userFilter.get(data);
    }

}
