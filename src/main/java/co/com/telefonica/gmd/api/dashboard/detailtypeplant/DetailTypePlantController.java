package co.com.telefonica.gmd.api.dashboard.detailtypeplant;

import co.com.telefonica.gmd.application.usescase.dashboard.detailtypeplant.IDetailTypePlant;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/dashboard/detail")
public class DetailTypePlantController {

    @Autowired
    @Qualifier("DetailTypePlant")
    private IDetailTypePlant detailTypePlant;

    @GetMapping("all")
    public Response getDetailNewDamage(
            @Param("id") Integer id,
            @Param("type")String type){
        return this.detailTypePlant.get(id, type);
    }
}
