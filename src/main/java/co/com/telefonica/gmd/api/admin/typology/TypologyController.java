package co.com.telefonica.gmd.api.admin.typology;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.usescase.admin.typology.ITypology;
import co.com.telefonica.gmd.domain.admin.typology.TypologyDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/admin/typology")
public class TypologyController {

    @Autowired
    @Qualifier("TypologyCreate")
    private ITypology typologyCreate;

    @Autowired
    @Qualifier("TypologyList")
    private ITypology typologyList;

    @Autowired
    @Qualifier("TypologyUpdate")
    private ITypology typologyUpdate;

    @PostMapping("/create")
    public Response createTypology(@RequestBody() TypologyDTO typology) { return this.typologyCreate.get(typology); }

    @GetMapping("/all")
    public Response getAllTypology(){
        HashMap<String , String> list = new HashMap<>();
        return  this.typologyList.get(list);
    }

    @PostMapping("/update")
    public Response updateTypology(@RequestBody() TypologyDTO typologyDTO) { return this.typologyUpdate.get(typologyDTO); }

}
