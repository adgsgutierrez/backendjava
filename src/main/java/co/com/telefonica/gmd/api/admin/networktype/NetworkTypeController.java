package co.com.telefonica.gmd.api.admin.networktype;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.usescase.admin.networktype.INetworkType;
import co.com.telefonica.gmd.domain.admin.networktype.NetworktTypeDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/admin/networktype")
public class NetworkTypeController {

    @Autowired
    @Qualifier("NetworkTypeCreate")
    private INetworkType networkTypeCreate;

    @Autowired
    @Qualifier("NetworkTypeList")
    private INetworkType networkTypeList;

    @Autowired
    @Qualifier("NetworkTypeUpdate")
    private INetworkType networkTypeUpdate;

    @PostMapping("/create")
    public Response createNetworkType(@RequestBody() NetworktTypeDTO networktTypeDTO) { return this.networkTypeCreate.get(networktTypeDTO); }

    @GetMapping("/all")
    public Response getAllNetworkType(){
        HashMap<String, String> list = new HashMap<>();
        return this.networkTypeList.get(list);
    }

    @PostMapping("/update")
    public Response updateNetworkType(@RequestBody() NetworktTypeDTO networktTypeDTO) { return this.networkTypeUpdate.get(networktTypeDTO); }
}

