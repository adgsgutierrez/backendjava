package co.com.telefonica.gmd.api.event;

import co.com.telefonica.gmd.application.usescase.event.IEvent;
import co.com.telefonica.gmd.domain.event.EventUpdateRequest;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class EventController {

    @Autowired
    @Qualifier("EventUpdate")
    private IEvent serviceEvent;

    @Autowired
    @Qualifier("EventUpdatePI")
    private IEvent serviceEventPI;

    @Autowired
    @Qualifier("EventUpdatePE")
    private IEvent serviceEventPE;

    @PutMapping("/updategdm")
    public Response updateEventByViability(@RequestBody EventUpdateRequest request){
        return serviceEvent.getOperation(request);
    }

    @GetMapping("/update/pe/{id}")
    public Response updateEventPE(@PathVariable("id") Integer id) {
        return serviceEventPE.getOperation(id);
    }

    @GetMapping("/update/pi/{id}")
    public Response updateEventPI(@PathVariable("id") Integer id) {
        return serviceEventPI.getOperation(id);
    }
}
