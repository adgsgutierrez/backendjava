package co.com.telefonica.gmd.api.conteogravedad;

import co.com.telefonica.gmd.application.usescase.conteogravedad.IGravedad;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/conteo")
public class ConteoGravedadController {

    @Autowired
    @Qualifier("GravedadGeneral")
    private IGravedad gravedadGeneral;

   @GetMapping("/all")
    public Response getAll(){ return this.gravedadGeneral.get(); }
}
