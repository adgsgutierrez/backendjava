package co.com.telefonica.gmd.api.admin.halfaccess;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.application.usescase.admin.halfaccess.IHalfAccess;
import co.com.telefonica.gmd.domain.admin.halfaccess.HalfAccessDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/admin/halfaccess")
public class HalfAccessController {

    @Autowired
    @Qualifier("HalfAccessCreate")
    private IHalfAccess halfAccessCreate;

    @Autowired
    @Qualifier("HalfAccessList")
    private IHalfAccess halfAccessList;

    @Autowired
    @Qualifier("HalfAccessUpdate")
    private IHalfAccess halfAccessUpdate;

    @PostMapping("/create")
    public Response createHalfAccess(@RequestBody() HalfAccessDTO halfAccessDTO) { return this.halfAccessCreate.get(halfAccessDTO); }

    @GetMapping("/all")
    public Response getAllHalfAccess(){
        HashMap<String , String> list = new HashMap<>();
        return  this.halfAccessList.get(list);
    }

    @PostMapping("/update")
    public  Response updateHalfAccess(@RequestBody() HalfAccessDTO halfAccessDTO) { return  this.halfAccessUpdate.get(halfAccessDTO); }

}
