package co.com.telefonica.gmd.api.hada;

import co.com.telefonica.gmd.application.usescase.hada.IHadaService;
import co.com.telefonica.gmd.platform.beans.ResponseHada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */

@RestController
@RequestMapping("/api/hada")
public class HadaController {

    @Autowired
    @Qualifier("HadaServiceReport")
    private IHadaService iHadaService;

    @GetMapping("/phone/{phone}")
    public ResponseHada getReportHada(@PathVariable("phone") String phone){
        return iHadaService.get(phone);
    }

}
