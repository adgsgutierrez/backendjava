package co.com.telefonica.gmd.api.dashboard.cablecloset;

import co.com.telefonica.gmd.application.usescase.dashboard.cablecloset.ICableCloset;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/dashboard/cablecloset")
public class CableClosetController {

    @Autowired
    @Qualifier("CableClosetList")
    private ICableCloset cableClosetService;

    @GetMapping("/all")
    public Response getCableClosetByDistributor(@Param("distributor")Integer distributor){
        return this.cableClosetService.get(distributor);
    }
}
