package co.com.telefonica.gmd.api.dashboard.location;

import co.com.telefonica.gmd.application.usescase.dashboard.location.ILocation;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/dashboard/location")
public class LocationController {

    @Autowired
    @Qualifier("LocationList")
    private ILocation locationListService;

    @GetMapping("/all")
    public Response getLocationByDpto(@Param("dpto")Integer dpto){
        return this.locationListService.get(dpto);
    }
}
