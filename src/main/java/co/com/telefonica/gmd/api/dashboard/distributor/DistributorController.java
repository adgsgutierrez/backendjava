package co.com.telefonica.gmd.api.dashboard.distributor;

import co.com.telefonica.gmd.application.usescase.dashboard.distributor.IDistributor;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/dashboard/distributor")
public class DistributorController {

    @Autowired
    @Qualifier("DistributorList")
    private IDistributor distributorListServices;

    @GetMapping("/all")
    public Response getDistributorByLocation(@Param("location")Integer location){
        return this.distributorListServices.get(location);
    }
}
