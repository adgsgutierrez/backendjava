package co.com.telefonica.gmd.api.notification;

import co.com.telefonica.gmd.application.usescase.notification.INotification;
import co.com.telefonica.gmd.domain.notification.NotificationDTO;
import co.com.telefonica.gmd.platform.beans.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/notification")
public class NotificationController {

    @Autowired
    @Qualifier("NotificationList")
    private INotification notificationListService;

    @Autowired
    @Qualifier("NotificationCreate")
    private INotification notificationCreateService;

    @GetMapping("/all")
    public Response getNotifications(
            @RequestParam(name = "page" , required = false ,defaultValue = "0") String page ,
            @RequestParam(name = "limit" , required = false ,defaultValue = "25") String limit,
            @RequestParam(name = "user")Integer user){

        Object[] data = new Object[]{ page, limit, user };
        return this.notificationListService.get(data);
    }

}
