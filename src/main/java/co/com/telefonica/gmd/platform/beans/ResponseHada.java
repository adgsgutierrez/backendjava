package co.com.telefonica.gmd.platform.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.util.Date;

@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
@Getter
@Setter
@ToString
public class ResponseHada {

    private Integer codeError;

    private String descriptionMessage;

    private String alarm;

    private String eventId;

    private String state;

    private String severity;

    private Date stateChange;

    private Date firstOcurrence;

    private Date lastOcurrence;

    private String hourOcurrence;

    private Integer flag1;

    private Integer flag2;

    private Integer type;

    private String description;

    private Integer tally;

    @JsonProperty(value = "class")
    private String clas;

    private String netType;

    private String netName;

    private String netAddress;

    private String netState;

    private String additionalInfo;

    private String site;

    private String location;

    private String massiva;

    private Integer affectedCustomer;

    private String summary;

    private Integer acknowledged;

    private String agent;
}
