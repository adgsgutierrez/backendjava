package co.com.telefonica.gmd.platform.config;

import co.com.telefonica.gmd.application.usescase.notification.INotification;
import co.com.telefonica.gmd.domain.notification.NotificationDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Slf4j
public class FilterNotificationInterceptor implements HandlerInterceptor {

    private INotification notification;
    private ApplicationContext applicationContext;
    private String typeNotification;
    private String descriptionNotification;
    private ExecutorService executor = Executors.newFixedThreadPool(2);

    public void setApplicationContext(ApplicationContext applicationContext){
        this.applicationContext = applicationContext;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        executor.submit(() -> {saveNotification(request);});
    }

    @Async("threadPoolTaskExecutor")
    private void saveNotification(HttpServletRequest request){
        log.info("URI " + request.getRequestURL().toString());
        int agent = request.getIntHeader("agent");
        log.info("agent " + agent);
        if (isSave(request.getRequestURI()) && request.getIntHeader("agent") != -1) {
            NotificationDTO notificationDTO = new NotificationDTO();
            notificationDTO.setEstado(true);
            Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC-5"));
            LocalTime localtime = LocalDateTime.ofInstant( calendar.getTime().toInstant() , ZoneId.of("America/Bogota")).toLocalTime();
            notificationDTO.setFecha(new java.sql.Date(calendar.getTime().getTime()));
            notificationDTO.setHora(localtime);
            notificationDTO.setTipoNotificacion(typeNotification);
            notificationDTO.setNotificacion(descriptionNotification);
            notificationDTO.setIdUsuario(agent);
            Object[] data = new Object[]{notificationDTO};
            notification = applicationContext.getBean("NotificationCreate" , INotification.class);
            notification.get(data);
            log.info("data guardada ");
        }
    }

    private Boolean isSave(String pathArray){
        if( pathArray.indexOf("ivr") > -1 && pathArray.indexOf( "load" ) > -1  ) {
            log.info("Guardando evento de Ivr");
            typeNotification = "ivr";
            descriptionNotification = "Se cargo un archivo ivr";
            return true;
        }
        if( pathArray.indexOf("export" ) > -1 &&  ( pathArray.indexOf("dashboard" ) > -1 || pathArray.indexOf("ivr" ) > -1 ) ) {
            log.info("Guardando evento de Export");
            typeNotification = "export";
            descriptionNotification = "Se descargo un archivo";
            return true;
        }
        if( pathArray.indexOf("typology" ) > -1 &&  pathArray.indexOf("update" ) > -1 ) {
            log.info("Guardando evento de Tipologia");
            typeNotification = "tipology";
            descriptionNotification = "Se actualizó una tipología";
            return true;
        }
        if( pathArray.indexOf("halfaccess" ) > -1 && pathArray.indexOf("update" ) > -1 ) {
            log.info("Guardando evento de Medio Acceso");
            typeNotification = "halfaccess";
            descriptionNotification = "Se actualizó un medio de acceso";
            return true;
        }
        if( pathArray.indexOf("networktype" ) > -1 &&  pathArray.indexOf("update" ) > -1 ) {
            log.info("Guardando evento de Network");
            typeNotification = "networktype";
            descriptionNotification = "Se actualizó un tipo de red";
            return true;
        }
        if( pathArray.indexOf("gravedad" ) > -1 &&  pathArray.indexOf( "update" ) > -1 ) {
            log.info("Guardando evento de Gravedad");
            typeNotification = "gravedad";
            descriptionNotification = "Se actualizó la gravedad";
            return true;
        }

        return false;
    }

}
