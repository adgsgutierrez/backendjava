package co.com.telefonica.gmd.platform.utils.export;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;

@Controller
@Getter
@Slf4j
public class ObjectToExcel {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public void newExcelBook(String titleSheet){
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet(titleSheet);
    }

    public void newExcelBook(){
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Information");
    }

    public void setTitleDocumentExport(ArrayList<String> titles){
        Row row = sheet.createRow(0);
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
        for (int index = 0 ; index < titles.size() ; index++) {
            createCell(row, index, titles.get(index), style);
        }
    }

    public void setDataDocumentExport(ArrayList<ArrayList<Object>> matrixValuesSheet){
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);
        for ( int indexRow = 0 ; indexRow < matrixValuesSheet.size() ; indexRow ++ ) {
            Row row = sheet.createRow((indexRow + 1));
            for ( int indexColumn = 0 ; indexColumn < matrixValuesSheet.get(indexRow).size() ; indexColumn ++ ) {
                createCell(row, indexColumn, matrixValuesSheet.get(indexRow).get(indexColumn), style);
            }
        }
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

}
