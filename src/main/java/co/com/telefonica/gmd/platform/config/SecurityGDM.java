package co.com.telefonica.gmd.platform.config;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

@EnableWebSecurity
public class SecurityGDM extends WebSecurityConfigurerAdapter {

    private static final String ContentSecurity = "script-src 'self'";
    private static final String FeaturePolicyValue = "accelerometerExperimental 'none' ; ambient-light-sensorExperimental 'none' ; autoplayExperimental 'none' ; cameraExperimental 'none' ; display-captureExperimental 'none' ; document-domainExperimental 'none' ; encrypted-mediaExperimental 'none' ; fullscreenExperimental 'none' ; geolocationExperimental 'none' ; gyroscopeExperimental 'none' ; layout-animationsExperimental 'none' ; legacy-image-formatsExperimental 'none' ; magnetometerExperimental 'none' ; microphoneExperimental 'none' ; midiExperimental 'none' ; oversized-imagesExperimental 'none' ; paymentExperimental 'none' ; picture-in-pictureExperimental 'none' ; speakerExperimental 'none' ; sync-xhrExperimental 'none' ; unoptimized-imagesExperimental 'none' ; unsized-mediaExperimental 'none' ; usbExperimental 'none' ; vibrateExperimental 'none' ; vrExperimental 'none' ; webauthn 'none' ;";
    private static final String FeaturePolicyHeader = "Feature-Policy";
    private static final String StrictTransportSecurityHeader = "Strict-Transport-Security";
    private static final String StrictTransportSecurityValue = "max-age=15000; includeSubDomains";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .csrfTokenRepository(
                        CookieCsrfTokenRepository.withHttpOnlyFalse()
                );
        http.csrf()
                .disable();
        http.requiresChannel()
                .anyRequest();
        http.headers()
                .contentSecurityPolicy(ContentSecurity)
                .and()
                .httpStrictTransportSecurity()
                .includeSubDomains(true)
                .maxAgeInSeconds(15000)
                .and()
                .referrerPolicy(ReferrerPolicy.SAME_ORIGIN)
                .and()
                .addHeaderWriter(
                        new StaticHeadersWriter(
                                FeaturePolicyHeader,
                                FeaturePolicyValue
                        )
                )
                .and()
                .headers()
                .addHeaderWriter(
                        new StaticHeadersWriter(
                                StrictTransportSecurityHeader,
                                StrictTransportSecurityValue
                        )
                )
                .and()
                .headers()
                .frameOptions()
                .sameOrigin()
                .httpStrictTransportSecurity()
                .disable()
                .and()
                .headers()
                .xssProtection()
                .block(false);
    }
}
