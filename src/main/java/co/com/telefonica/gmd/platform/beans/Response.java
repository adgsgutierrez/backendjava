package co.com.telefonica.gmd.platform.beans;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Mayo 2021
 * Author: Everis GDM
 * All rights reserved
 */

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import java.io.Serializable;

@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
@Getter
@Setter
public class Response implements Serializable {

    /**
     * Code to response funcionality
     */
    private Integer code;
/**
     * Code to response funcionality
     */
    private String message;

    private Object data;

}
