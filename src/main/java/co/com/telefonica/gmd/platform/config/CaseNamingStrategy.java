package co.com.telefonica.gmd.platform.config;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import java.io.Serializable;

public class CaseNamingStrategy extends PhysicalNamingStrategyStandardImpl implements Serializable {

    public static final CaseNamingStrategy INSTANCE = new CaseNamingStrategy();

    @Override
    public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment context) {
        return super.toPhysicalCatalogName(name, context);
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment context) {
        return convertToSnakeCase(name , context);
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        return convertToSnakeCase(name , context);
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment context) {
        return convertToSnakeCase(name , context);
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        return convertToSnakeCase(name, context);
    }

    private Identifier convertToSnakeCase(final Identifier identifier , JdbcEnvironment context) {
        if (identifier != null) {
            final String newName = ("\"").concat(identifier.getText()).concat("\"");
            return Identifier.toIdentifier(newName);
        }
        return super.toPhysicalCatalogName(identifier, context);
    }
}