package co.com.telefonica.gmd.platform.utils.transform;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Junio 2021
 * Author: Everis GDM
 * All rights reserved
 */
import co.com.telefonica.gmd.domain.ivr.IvrDetailEntity;
import co.com.telefonica.gmd.platform.utils.validation.Validation;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.OracleCodec;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Pattern;

public class FunctionsTransform {

    /** The Constant random. */
    private static final SecureRandom random = new SecureRandom(); // Compliant for security-sensitive use cases
    /** The Constant bytes. */
    private static final byte[] bytes = new byte[20];

    /** The Constant INTERATION_ENCRIPTION. */
    public static final Integer INTERATION_ENCRIPTION = 2;

    public static String generateKeysDynamic(String cadena1 , String cadena2 , String cadena3) {
        String result = ( cadena1.concat(cadena2) ).concat(cadena3);
        return result;
    }

    public static String encodeStringToBase64WithIteration(String value) {
        byte[] decodedBytes = new byte[] {};
        for (int index = 0; index < INTERATION_ENCRIPTION; index++) {
            decodedBytes = Base64.encodeBase64(value.getBytes());
            value = getTokenText(1).concat(new String(decodedBytes));
        }
        decodedBytes = Base64.encodeBase64(value.getBytes());
        value = new String(decodedBytes);
        return value;
    }

    public static String decodeStringToBase64WithIteration(String value) {
        byte[] decodedBytes = new byte[] {};
        for (int index = 0; index < INTERATION_ENCRIPTION; index++) {
            decodedBytes = Base64.decodeBase64(value.getBytes());
            value = new String(decodedBytes).substring(1);
        }
        decodedBytes = Base64.decodeBase64(value.getBytes());
        value = new String(decodedBytes);
        return value;
    }

    /**
     * Gets the token text.
     *
     * @param number the number
     * @return the token text
     */
    public static String getTokenText(Integer number) {
        String text = "";
        if (number != null) {
            random.nextBytes(bytes);
            String possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            StringBuilder bld = new StringBuilder("");
            for (int i = 0; i < number; i++) {
                int index = (int) (Math.floor(random.nextDouble() * possible.length()));
                bld.append(possible.charAt(index));
            }
            text = bld.toString();
        }
        return text;
    }

    /**
     * Funcion encargada de recibir un String y convertirlo a JSONObject
     *
     */
    public static JSONObject stringToJsonObject(String objectString) {
        JSONObject jsonObject = new JSONObject(StringEscapeUtils.escapeHtml4(objectString));
        return jsonObject;
    }

    public static String getStringNowWithFormat( String format ){
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        formatter.setTimeZone(TimeZone.getTimeZone(ZoneId.of("America/Bogota")));
        return formatter.format(date);
    }

    public static ArrayList<IvrDetailEntity> getArrayToString(ArrayList<IvrDetailEntity> detailEntity , List<String> list) throws ParseException {
        for (String line : list) {
            String separator = Validation.getColumnSeparator(line);
            if ( !separator.equals("")) {
                String[] values = line.split(separator);
                if (values.length == 8) {
                    IvrDetailEntity detailEntityObject = new IvrDetailEntity();
                    detailEntityObject.setViabilidadTicket( new Integer(values[0]));
                    detailEntityObject.setAbonado( values[1] );
                    DateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    detailEntityObject.setFechaFin( formatDate.parse(values[2]) );
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                    Time time = new Time( format.parse(values[3]).getTime() );
                    detailEntityObject.setHoraFin(time);
                    detailEntityObject.setFkIdTipologia( new BigInteger(values[5]) );
                    detailEntityObject.setTipoDanio( Integer.parseInt(values[4]) );
                    detailEntityObject.setFlag1( Integer.parseInt(values[6])) ;
                    detailEntityObject.setFlag2( Integer.parseInt(values[7]) );
                    detailEntity.add(detailEntityObject);
                }
            }
        }
        return detailEntity;
    }

    public static String getTime(Time time , String format){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-5")); // creates a new calendar instance
        calendar.setTime(time);   // assigns calendar to given date
        SimpleDateFormat formatHours = new SimpleDateFormat(format);
        return formatHours.format(calendar.getTime());
    }

    public static String getDate(Date time , String format){
        //TimeZone.getTimeZone("GMT-5")
        Calendar calendar = Calendar.getInstance(); // creates a new calendar instance
        calendar.setTime(time);   // assigns calendar to given date
        SimpleDateFormat formatHours = new SimpleDateFormat(format);
//        formatHours.setTimeZone(TimeZone.getTimeZone(ZoneId.of("America/Bogota")));
        return formatHours.format(calendar.getTime());
    }

    public static Query getQueryBuilderString(String baseQuery, EntityManager entityManager , Map<String, Object> params) throws ParseException {
        return FunctionsTransform.getQueryBuilderString(baseQuery,entityManager,params , Boolean.TRUE);
    }

    public static Query getQueryBuilderString(String baseQuery, EntityManager entityManager , Map<String, Object> params , Boolean isString) throws ParseException {
        StringBuilder builder = new StringBuilder(baseQuery);
        Codec ORACLE_CODEC = new OracleCodec();
        String[] keysArray = params.keySet().toArray(new String[0]);
        int index = 1;
        for ( String key : keysArray ) {
            System.out.println("Key " + key);
            if( index > 1) { builder.append(" AND ").append(" "); };
            if (key.equals("\"V2_DM\".\"V_TablaDashboard\".\"FechaDesde\"")){
                builder.append( ESAPI.encoder().encodeForSQL(ORACLE_CODEC , "\"V2_DM\".\"V_TablaDashboard\".\"Fecha\"" ) ).append(" > ");
            } else if (key.equals("\"V2_DM\".\"V_TablaDashboard\".\"FechaHasta\"")){
                builder.append( ESAPI.encoder().encodeForSQL(ORACLE_CODEC , "\"V2_DM\".\"V_TablaDashboard\".\"Fecha\"" ) ).append(" < ");
            } else if (key.equals("\"V2_DM\".\"V_TablaDashboard\".\"Estado\"")){
                builder.append( ESAPI.encoder().encodeForSQL(ORACLE_CODEC , key ) ).append(" = ");
            } else {
                builder.append( ESAPI.encoder().encodeForSQL(ORACLE_CODEC , key ) ).append(" LIKE ");
            }
            String append = " ? " ;
            builder.append( append );
            index = index + 1;
        }
        String queryStr = org.apache.commons.lang.StringEscapeUtils.escapeSql(builder.toString());
        System.out.println("Query "+ queryStr);
        Query query = entityManager.createNativeQuery( queryStr );
        String regex = "\\d+";
        String regexDate = "^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
        Pattern datePattern =  Pattern.compile(regexDate);
        index = 1;
        for ( String key : keysArray ) {
            Object value =  params.get(key);
            if ( ("" + value).matches(regex) && isString) {
                query.setParameter(index, Integer.valueOf( "" + value));
            } else if (datePattern.matcher(("" + value)).matches()) {
                query.setParameter(index, new SimpleDateFormat("yyyy-MM-dd").parse(("" + value)) );
            } else if ( String.valueOf(value).toLowerCase().equals("true") || String.valueOf(value).toLowerCase().equals("false")){
                query.setParameter(index, Boolean.parseBoolean( String.valueOf(value)) );
            } else {
                query.setParameter(index, ("%").concat(String.valueOf(value)).concat("%") );
            }
            index = index + 1;
        }
        return query;
    }

    public static ArrayList< ArrayList <Object> > getDataResult(Integer[] rowsValues ,  List<Object[]> results){
        ArrayList< ArrayList <Object> > dataExport = new ArrayList<>();
        for (Object[] tableListQuery : results) {
            ArrayList<Object> row = new ArrayList();
            for(Integer index : rowsValues)
                if(tableListQuery[index] != null) {
                    row.add(tableListQuery[index].toString());
                } else {
                    row.add("");
                }
            dataExport.add(row);
        }
        return  dataExport;
    }

    public static ArrayList<String> getDataResultString(Integer[] rowsValues ,  List<Object[]> results){
        ArrayList<String> dataExport = new ArrayList<>();
        for (Object[] tableListQuery : results) {
            ArrayList<String> row = new ArrayList();
            for(Integer index : rowsValues){
                if(tableListQuery[index] != null) {
                    row.add(tableListQuery[index].toString());
                } else {
                    row.add("");
                }
            }
            dataExport.add(String.join(";" , row));
        }
        return  dataExport;
    }
}
