package co.com.telefonica.gmd.platform.config;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Mayo 2021
 * Author: Everis GDM
 * All rights reserved
 */
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource("classpath:configuration/properties/servers.properties"),
        @PropertySource("classpath:configuration/configuration.properties"),
        @PropertySource("classpath:configuration/security/security.properties"),
        @PropertySource("classpath:configuration/properties/querys.properties")
})
public class PropertiesGDM { }
