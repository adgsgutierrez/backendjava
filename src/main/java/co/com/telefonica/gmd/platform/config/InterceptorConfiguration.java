package co.com.telefonica.gmd.platform.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class InterceptorConfiguration extends WebMvcConfigurationSupport {

    private FilterNotificationInterceptor interceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        FilterNotificationInterceptor interceptor = new FilterNotificationInterceptor();
        interceptor.setApplicationContext(this.getApplicationContext());
        registry.addInterceptor(interceptor).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

}
