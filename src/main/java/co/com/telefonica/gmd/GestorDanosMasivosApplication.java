package co.com.telefonica.gmd;
/**
 * Licensed under the MIT License.
 * API de servicios para la plataforma Gestor de Daños Masivos
 * Version: 1.0.0
 * Date: Mayo 2021
 * Author: Everis GDM
 * All rights reserved
 */
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableAsync
@EnableScheduling
@Slf4j
public class GestorDanosMasivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorDanosMasivosApplication.class, new String[]{});
	}

	@PostConstruct
	public void init(){
		// Setting Spring Boot SetTimeZone
		log.info("Cargando zona horaria a Colombia");
		TimeZone.setDefault(TimeZone.getTimeZone("UTC-5"));
	}

}
